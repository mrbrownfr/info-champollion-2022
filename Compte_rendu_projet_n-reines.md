# Projet n-reines MAZOYER AMAURY

## Explications globales

Le programme permet la résolution du problème des n-reines de plusieurs manières différentes. Ci dessous une liste exhaustive de ses capacités :
- résolution pour **une** solution à l'aide de *backtracking*
- résolution pour **toutes** les solutions
- résolution pour **toutes** les solutions **uniques** à isomophisme près (symétries, rotations etc.)
- résolution pour **une** solution à l'aide d'une *résolution de conflits*
- affichage du nombre de solutions, uniques ou non

De plus d'autres options permettent une meilleure expérience utilisateur, comme :
- la redirection de la sortie du programme vers un fichier
- la désactivation de tout affichage pour effectuer des tests de performance

Pour le tester, exécutez le programme `n_reines` dans un terminal sous Linux.
Ci-dessous le résultat de l'exécution de la commande `./n_reines --help`, expliquant comment exécuter le programme.

```bash
n_reines [-all] [-unique] [-conflit] [-nb] [-perf] [-o <nom du fichier>] [-n <nombre de reines>]
  -all         Affiche toutes les possibilités. Si l\'option -unique est utilisée, alors elle prend le dessus
  -unique      Affiche toutes les possibilités uniques à isomorphisme près
  -conflit     Résolution à l\'aide de la méthode de résolution des conflits (plus lent). Incompatible avec les options -all et -unique
  -nb          Renvoie uniquement le nombre de solutions et non les solutions elles-même. Compatible avec -unique
  -perf        Désactive l\'affichage de tout résultat, pratique pour tester les performances du programme
  -o <fichier> Redirige la sortie du programme dans le fichier précisé en paramètre. Si le fichier n\'existe pas, il sera créé. ATTENTION : l\'écriture écrasera le contenu du fichier
  -n <int>     Fixe le nombre de reines (défault=8)
  -help        Display this list of options
  --help       Display this list of options
```

*Limitations :* 
- en mode normal, ne pas lancer le programme avec un $n$ trop élevé ($\sim 30-35$) auquel cas le temps d'exécution du programme risque de s'avérer très grand (maximum **testé** : $n=33$ avec un temps d'exécution $\approx 7$ minutes)
- il est recommandé de désactiver l'affichage pour la résolution de toutes les solutions au delà de $n=13$, présentant déjà $\approx 73000$ solutions (un fichier contenant toutes les solutions pour $n=14$ pèse environ 160Mo)
- la suppression des isomorphismes est coûteuse, il est déconseiller de l'appliquer pour $n \geqslant 14$ 
- la résolution avec la méthode de résolution de conflits est peu efficace car mal implémentée (faute de temps), ainsi il est recommandé de ne pas l'utiliser pour $n > 20$

## Explications techniques

### Méthode classique pour une solution

#### Représentation du problème

Les explications qui suivent sont relative à la première approche du problème qui j'ai facée, celle du backtracking.

En partant de l'observation que le problème revient à placer exactement une reine sur chaque ligne et chaque colonne, en se souciant uniquement des diagonales, on peut réduire le problème à $n$ sous-problèmes : où placer la i-ème reine sur la i-ème ligne ?

Déroulons un exemple pour $n=4$ 
Pour la première ligne, on a le choix entre $4$ possibilités, posons notre reine sur la 1ère case. On a alors ce plateau (où `R` représente la reine placée):
```plain
|R| | | |
```

Maintenant regardons sur quelles cases on peut poser une reine sur la 2ème ligne : les cases libres sont les cases ne se trouvant ni directement sous une reine, ni dans une de ses deux diagonale. On a donc les choix suivants (où `x` représente les cases où l'on ne peut pas poser de reines) :
```plain
|R| | | |
|x|x| | |
```

On place donc notre nouvelle reine sur le 1ère case disponible, et on recommence pour la prochaine ligne :
```plain
|R| | | |
|x|x|R| |
|x|x|x|x|
```

Ici, on ne peut plus placer de reine, nos placement sont donc mauvais. On va alors remonter dans l'aglorithme et essayer les autres positions sur lesquelles on pouvait poser une reine :
```plain
|R| | | |
|x|x|x|R|
|x|x|x|x|
```

Encore une fois, même problème. Il faut donc remonter plus haut dans l'algorithme :
```plain
|x|R| | |
|x|x|x| |
```

En déroulant le reste de l'algorithme, on obtient le plateau final suivant :
```plain
|x|R| | |
|x|x|x|R|
|R|x|x|x|
|x|x|R|x|
```

On a réussi à placer une reine sur la 4ième ligne, on a donc trouver une solution qui est :
```plain
| |R| | |
| | | |R|
|R| | | |
| | |R| |
```

#### Algorithme

Ce type d'algorithme s'appelle algorithme de "backtracking", ou de "retour sur trace".
Il est recommandé de lire le code des fonctions en même temps que cet algorithme pour mieux le comprendre.

Pour chaque ligne i :
- On calcul la liste des indices des cases sur lesquelles on peut poser une reine sur le i-ème ligne en fonction des positions des reines déjà posées. Pour cela :
	- on créé une liste contenant les indices de 0 à n-1
	- on supprime tous les indices dont une dame se trouve dans la même colonne
	- on supprime tous les indices se trouvant sur la diagonale droite d'une reine
	- on supprime tous les indices se trouvant sur la diagonale gauche d'une reine
- Si la liste des indices valides n'est pas vide :
	- si on est sur la n-ème ligne, alors on place la reine au premier indice de la liste et on sort de l'algorithme : on a trouvé une solution
	- si on n'est pas sur la n-ème ligne, alors on place la reine au premier indice de la liste et continue l'algorithme sur la (i+1)-ème ligne
- Si la liste des indices valides est vide, alors on remonte à la (i-1)-ème ligne, en enlevant la reine placée sur la (i-1)-ème ligne, et on reprend l'algorithme à la (i-1)-ème ligne en enlevant l'indice de la reine précédemment placée sur la (i-1)-ème ligne des indices valides. (cf. déroulement de l'algorithme pour n=4)

En pratique, pour simuler l'effet du retour sur trace lorsqu'une solution n'est pas trouvée, on peut tester tous les indices sur lesquels on peut poser une reine sur la ligne jusqu'à ce que l'un de ces indices donne un résultat. Si aucun résultat n'est donné, c'est la ligne du dessus qui continue de tester ses indices valides etc...

La différence avec une recherche exhaustive vient du fait que seules les positions **valides** sont testées, et que si une solution est trouvée, alors l'algorithme entier s'arrête et renvoie cette solution.

#### Complexité temporelle

La recherche des indices valides pour une ligne s'effectue en $O(n)$. 
En effet on parcours une fois la liste des reines contenant au plus $n$ reines pour déterminer quelles cases sont sous attaque ($O(n)$), puis on crée une liste des entiers de $0$ à $n-1$ ($O(n)$) avant d'effectuer un filtrage dessus où la condition testée pour chaque case est effectuée en $O(1)$ (accès d'un élément d'un array + comparaison), donnant une complexité de $O(n)$. 
Ces trois opérations en $O(n)$ s'effectuants l'une après l'autre, la complexité finale est de $O(n)$.

Cette array d'indices valides, pour la i-ème ligne, contient **au plus** $n-i$ éléments, et dans le pire des cas on test tous les $n-i$ indices (boucle while jusqu'au traitement de l'exception) pour toutes les $n$ lignes, soit $n!$ tests ($n$ à la 1ère ligne, $n-1$ à la 2ème etc...)

Comme $n \underset{n\rightarrow+\infty}{=} o(n!)$, alors la complexité finale de l'algorithme est de $O(n!)$ *dans la pire des cas*.

#### Complexité spatiale

On a vu que *dans le pire des cas* le nombre d'appels récursifs est de l'ordre de $n!$.
A chaque appel récursif, un array d'indices valides est calculé, contenant au plus $n$ éléments. De plus chaque appel récursif prend en paramètre la liste des reines, contenant au plus $n-1$ reines.
Dans le pire des cas, la complexité spatiale de l'algorithme est donc de $O(n^{2} \times n!) = O(n!)$.

### Résolutions à l'aide des autres méthodes

Les explications données seront rapides car cette partie du projet dépasse les attentes de la grille d'évaluation. Elles sont présentes pour aider à la compréhension des autres méthodes.

#### Résolution pour toutes les solutions

La recherche de toutes les solutions utilise les mêmes fonctions auxiliaires que la résolution à l'aide du backtracking, à savoir la fonction `indices_valides`.

Les solutions sont stockées dans une référence de liste afin de pouvoir en ajouter au passage.

Pour chaque ligne, on appel une fonction récursive auxiliaire permettant de résoudre le problème en connaissant les coordonnées des $i$ première reines en rajoutant une reine à chaque indice valide sur la ligne (une par appel récursif).
Lorsque la fonction résursive est appelée pour la $n-1$ ème ligne, alors on ajoute à la liste des solutions toutes les solutions possibles à notre disposition, à savoir une par indice valide.

Il suffit ensuite juste d'appeler la fonction récursive pour la ligne $0$ avec une liste de reines vides, et ensuite de renvoyer le contenu de la référence contenant les solutions.

La complexité temporelle de cette fonction est de la même manière que la résolution à l'aide de backtracking en $O(n!)$.

#### Résolution pour toutes les solutions uniques

La "résolution" pour toutes les solutions uniques n'est pas réellement une résolution à proprement parler.
Elle s'appuie d'abord sur la résolution de toutes les solutions avant de filtrer les isomorphismes.

La fonction `isomorphisme` renvoie la liste de tous les isomorphismes pour une certaine solution, à l'exception de l'identité.
Il s'agit ensuite seulement d'appeler une fonction résursive sur les solutions en filtrant les isomorphismes de l'élément en tête à chaque appel.

*Remarque* : le filtrage successif des isomorphismes est couteux, et cela ajouté au fait qu'il faille d'abord trouver toutes les solutions fait de cette résolution une résolution lente.

#### Détermination du nombre de solutions

Il est possible d'adapter l'algorithme décrit pour trouver toutes les solutions à la détermination du nombre de solutions, mais ici l'approche prise est plus simple.
Il s'agit uniquement de renvoyer la taille de la liste des solutions, donnant effectivement le nombre de solutions.
Pour le nombre de solutions uniques, il suffit de donner la taille de la liste après filtrage des isomorphismes.

La complexité temporelle de cette fonction est de $O(n \times n!) = O(n!)$.

#### Résolution à l'aide de la résolution de conflits

Cette méthode est plus complexe et mériterait des explications plus approfondies. Voici donc un extrait de la page wikipédia du problème des 8 reines, donnant des explications sur cette méthode :

"Un algorithme de « réparation itérative » commence typiquement à partir d'un placement de toutes les dames sur l'échiquier, par exemple avec une seule dame par colonne. Il compte alors le nombre de conflits entre dames, et utilise une méthode heuristique pour déterminer comment améliorer le placement des dames. La méthode heuristique de moindre conflit, qui consiste à déplacer la pièce ayant le plus grand nombre de conflits, dans la même colonne à une place où le nombre de conflits est le plus petit, est particulièrement efficace. Elle résout le problème des millions de dames (sur un échiquier de 1 000 000 × 1 000 000 cases) en moins de 50 étapes en moyenne.

L'obtention de cette moyenne de 50 étapes suppose que la configuration initiale soit raisonnablement bonne. Si au début, un million de dames sont placées dans la même rangée, l'algorithme prendra évidemment plus de 50 étapes pour résoudre le problème. Un point de départ « raisonnablement bon » consiste à placer chaque dame dans une colonne telle qu'elle soit en conflit avec le plus petit nombre de dames se trouvant déjà sur l'échiquier.

La méthode de réparation itérative, à la différence de la recherche en profondeur décrite ci-dessus, ne garantit pas une solution. Comme toutes les méthodes de plus profonde descente, elle peut se bloquer sur un [extremum](https://fr.wikipedia.org/wiki/Extremum "Extremum") local (dans ce cas l'algorithme peut être remis en marche avec une configuration initiale différente.) D'un autre côté, elle peut résoudre des problèmes de grandes tailles qui sont largement au-delà de la portée d'une recherche en profondeur."

L'algorithme que j'ai utilisé pour cette résolution se rapproche de ce qui est expliqué ci-dessus. Cela permet aussi de se rendre compte de du manque d'efficacité de mon algorithme par rapport à la complexité qu'on pourrait attendre d'un tel programme.

La complexité temporelle de cette fonction est difficile à déterminer car il repose sur de l'aléatoire (extremums locaux) mais on peut l'évaluer aux alentours de $O(n^n)$.

*Remaques* : des commentaires sont présent dans le code des fonctions constituants cet algorithme et permettent une meilleur compréhension de l'approche prise.

## Documentation du code

La documentation du code est disponible [ici](./documentation.html).