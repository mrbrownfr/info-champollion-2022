(* fonctions permettant l'affichage de la grille de sudoku *)

let pp_ligne outchan arr =
  let n = Array.length arr in
  Printf.fprintf outchan "| ";
  for i = 0 to n - 1 do
    if arr.(i) <> 0 then
      if i mod 3 = 2 then Printf.fprintf outchan "%d | " arr.(i)
      else Printf.fprintf outchan "%d " arr.(i)
    else if i mod 3 = 2 then Printf.fprintf outchan "  | "
    else Printf.fprintf outchan "  "
  done

let pp_grille outchan mat =
  let n = Array.length mat in
  Printf.fprintf outchan "%s\n" (String.make 25 '-');
  for i = 0 to n - 1 do
    Printf.fprintf outchan "%a\n" pp_ligne mat.(i);
    if i mod 3 = 2 then Printf.fprintf outchan "%s\n" (String.make 25 '-')
  done

(* I.A *)
let rec appartient x l =
  match l with [] -> false | t :: q -> t = x || appartient x q

(* I.B *)
let rec supprime x l =
  match l with
  | [] -> []
  | t :: q -> if t = x then supprime x q else t :: supprime x q

(* I.C *)
let ajoute x l = if appartient x l then l else x :: l

(* I.D *)
let indice (b, r) =
  let ib, jb = (b / 3, b mod 3) in
  let ic, jc = (r / 3, r mod 3) in
  ((ib * 3) + ic, (jb * 3) + jc)

type litteral = X of int * int * int | NonX of int * int * int

(* II.A.1 *)
let clauses_i_j i j =
  let res = ref [] in
  for k = 9 downto 1 do
    res := X (i, j, k) :: !res
  done;
  !res

let case1 () =
  let f = ref [] in
  for i = 0 to 8 do
    for j = 0 to 8 do
      f := clauses_i_j i j :: !f
    done
  done;
  !f

(* II.A.2 *)
let clauses_i_k i k =
  let res = ref [] in
  for j = 0 to 8 do
    res := X (i, j, k) :: !res
  done;
  !res

let lig1 () =
  let f = ref [] in
  for i = 0 to 8 do
    for k = 1 to 9 do
      f := clauses_i_k i k :: !f
    done
  done;
  !f

(* II.A.3 *)
let clauses_j_k j k =
  let res = ref [] in
  for i = 0 to 8 do
    res := X (i, j, k) :: !res
  done;
  !res

let col1 () =
  let f = ref [] in
  for j = 0 to 8 do
    for k = 1 to 9 do
      f := clauses_j_k j k :: !f
    done
  done;
  !f

let clauses_b_k b k =
  let res = ref [] in
  for r = 0 to 8 do
    let i, j = indice (b, r) in
    res := X (i, j, k) :: !res
  done;
  !res

let bloc1 () =
  let f = ref [] in
  for b = 0 to 8 do
    for k = 1 to 9 do
      f := clauses_b_k b k :: !f
    done
  done;
  !f

(* II.A.4 *)
let lig2 () =
  let res = ref [] in
  for i = 0 to 8 do
    for k = 1 to 9 do
      for j1 = 0 to 7 do
        for j2 = j1 + 1 to 8 do
          res := [ NonX (i, j1, k); NonX (i, j2, k) ] :: !res
        done
      done
    done
  done;
  !res

(* II.A.5 *)
let col2 () =
  let res = ref [] in
  for j = 0 to 8 do
    for k = 1 to 9 do
      for i1 = 0 to 7 do
        for i2 = i1 + 1 to 8 do
          res := [ NonX (i1, j, k); NonX (i2, j, k) ] :: !res
        done
      done
    done
  done;
  !res

let bloc2 () =
  let res = ref [] in
  for b = 0 to 8 do
    for k = 1 to 9 do
      for r1 = 0 to 7 do
        for r2 = r1 + 1 to 8 do
          let i1, j1 = indice (b, r1) and i2, j2 = indice (b, r2) in
          res := [ NonX (i1, j1, k); NonX (i2, j2, k) ] :: !res
        done
      done
    done
  done;
  !res

let case2 () =
  let res = ref [] in
  for i = 0 to 8 do
    for j = 0 to 8 do
      for k1 = 1 to 8 do
        for k2 = k1 + 1 to 9 do
          res := [ NonX (i, j, k1); NonX (i, j, k2) ] :: !res
        done
      done
    done
  done;
  !res

(* II.B.1 *)
let donnees grille =
  let f = ref [] in
  for i = 0 to 8 do
    for j = 0 to 8 do
      let k = grille.(i).(j) in
      if k <> 0 then
        for u = 1 to 9 do
          if u <> k then f := [ NonX (i, j, u) ] :: !f
          else f := [ X (i, j, k) ] :: !f
        done
    done
  done;
  !f

(* II.B.2 *)
let numbloc i j = (i / 3 * 3) + (j / 3)

let interdites_ij grille i j =
  let f = ref [] in
  let b = numbloc i j in
  for l = 0 to 8 do
    let k = grille.(l).(j) in
    (* parcours de la colonne *)
    if k <> 0 then f := [ NonX (i, j, k) ] :: !f;
    let k = grille.(i).(l) in
    (* parcours de la ligne *)
    if k <> 0 then f := [ NonX (i, j, k) ] :: !f;
    let i1, j1 = indice (b, l) in
    (* parcours du bloc *)
    let k = grille.(i1).(j1) in
    if k <> 0 then f := [ NonX (i, j, k) ] :: !f
  done;
  !f

let interdites grille =
  let f = ref [] in
  for i = 0 to 8 do
    for j = 0 to 8 do
      if grille.(i).(j) = 0 then
        (* f := List.rev_append (!f) (interdites_ij grille i j) *)
        f := interdites_ij grille i j @ !f
    done
  done;
  !f

(* grille du sujet *)
let grille1 = Array.make_matrix 9 9 0;;

grille1.(0).(1) <- 9;
grille1.(1).(0) <- 3;
grille1.(1).(1) <- 2;
grille1.(2).(1) <- 7;
grille1.(0).(3) <- 2;
grille1.(0).(6) <- 6;
grille1.(0).(8) <- 5;
grille1.(1).(5) <- 7;
grille1.(2).(3) <- 9;
grille1.(2).(5) <- 5;
grille1.(2).(8) <- 8;
grille1.(3).(1) <- 1;
grille1.(4).(2) <- 7;
grille1.(4).(7) <- 9;
grille1.(4).(8) <- 4;
grille1.(5).(0) <- 6;
grille1.(6).(2) <- 8;
grille1.(6).(8) <- 7;
grille1.(7).(1) <- 3;
grille1.(7).(3) <- 4;
grille1.(7).(4) <- 9;
grille1.(7).(5) <- 1;
grille1.(7).(6) <- 5;
grille1.(8).(5) <- 3

(* grille d'un sudoku "niveau 5" *)
let grille2 = Array.make_matrix 9 9 0;;

grille2.(0).(0) <- 5;
grille2.(0).(1) <- 4;
grille2.(0).(3) <- 9;
grille2.(0).(4) <- 3;
grille2.(0).(7) <- 1;
grille2.(1).(0) <- 2;
grille2.(1).(4) <- 6;
grille2.(3).(0) <- 7;
grille2.(3).(1) <- 3;
grille2.(3).(3) <- 5;
grille2.(3).(7) <- 6;
grille2.(3).(8) <- 1;
grille2.(4).(1) <- 7;
grille2.(4).(7) <- 8;
grille2.(5).(0) <- 8;
grille2.(5).(1) <- 2;
grille2.(5).(5) <- 1;
grille2.(5).(7) <- 7;
grille2.(5).(8) <- 3;
grille2.(7).(4) <- 2;
grille2.(7).(8) <- 6;
grille2.(8).(1) <- 1;
grille2.(8).(4) <- 8;
grille2.(8).(5) <- 3;
grille2.(8).(7) <- 7;
grille2.(8).(8) <- 9

(* grille 1 avec des cases supplémentaires *)
let grille3 = Array.make_matrix 9 9 0;;

grille3.(0).(1) <- 9;
grille3.(1).(0) <- 3;
grille3.(1).(1) <- 2;
grille3.(2).(1) <- 7;
grille3.(0).(3) <- 2;
grille3.(0).(6) <- 6;
grille3.(0).(8) <- 5;
grille3.(1).(5) <- 7;
grille3.(2).(3) <- 9;
grille3.(2).(5) <- 5;
grille3.(2).(8) <- 8;
grille3.(3).(1) <- 1;
grille3.(4).(2) <- 7;
grille3.(4).(7) <- 9;
grille3.(4).(8) <- 4;
grille3.(5).(0) <- 6;
grille3.(6).(2) <- 8;
grille3.(6).(8) <- 7;
grille3.(7).(1) <- 3;
grille3.(7).(3) <- 4;
grille3.(7).(4) <- 9;
grille3.(7).(5) <- 1;
grille3.(7).(6) <- 5;
grille3.(4).(4) <- 6;
grille3.(4).(5) <- 8;
grille3.(8).(5) <- 3;
grille3.(8).(8) <- 1;
grille3.(8).(0) <- 5

let fgrille g = donnees g @ interdites g

let fregle () =
  case1 () @ lig1 () @ col1 () @ bloc1 () @ case2 () @ lig2 () @ col2 ()
  @ bloc2 ()

let formule_initiale g = fgrille g @ fregle ()

(** III_A_6 **)
let rec nouveau_lit_isole f =
  match f with
  | [] -> X (-1, -1, -1)
  | [ lit ] :: q -> lit
  | _ :: q -> nouveau_lit_isole q

(** III_A_7 **)
let negation l =
  match l with X (i, j, k) -> NonX (i, j, k) | NonX (i, j, k) -> X (i, j, k)

let rec simplification lit f =
  match f with
  | [] -> []
  | clause :: q when appartient lit clause -> simplification lit q
  | clause :: q ->
      let neglit = negation lit in
      let c = supprime neglit clause in
      c :: simplification lit q

(** III_A_8 **)
let rec propagation t f =
  match nouveau_lit_isole f with
  | X (-1, -1, -1) -> f
  | X (i, j, k) ->
      t.(i).(j) <- k;
      propagation t (simplification (X (i, j, k)) f)
  | lit -> propagation t (simplification lit f)

let sudoku_v1 t =
  let f = formule_initiale t in
  let _ = propagation t f in
  t

let rec flatten (f : litteral list list) =
  match f with [] -> [] | clause :: q -> clause @ flatten q

(** III_B_2 **)
let variables f =
  let rec aux f = match f with [] -> [] | lit :: q -> ajoute lit (aux q) in
  aux (flatten f)

let copier_matrice mat =
  Array.init (Array.length mat) (fun i -> Array.copy mat.(i))

(** III_B_3 **)
let deduction x t f =
  if List.mem [] @@ propagation (copier_matrice t) ([ x ] :: f) then -1
  else if List.mem [] @@ propagation (copier_matrice t) ([ negation x ] :: f)
  then 1
  else 0

(** III_B_4 **)
let propagation2 t f =
  let devoiler lit t =
    match lit with X (i, j, k) -> t.(i).(j) <- k | _ -> ()
  in
  let rec aux f =
    let f = propagation t f in
    let var = variables f in
    let rec lit_infructeux var =
      match var with
      | [] -> ()
      | v :: q -> (
          match deduction v t f with
          | -1 ->
              devoiler (negation v) t;
              aux ([ negation v ] :: f)
          | 1 ->
              devoiler v t;
              aux ([ v ] :: f)
          | _ -> lit_infructeux q)
    in
    lit_infructeux var
  in
  aux f

(** III_B_5 **)
let sudoku t =
  let f = formule_initiale t in
  let _ = propagation2 t f in
  t

let print_grille grille = Printf.printf "%a" pp_grille grille

let print_solution t =
  let sol = sudoku t in
  print_grille sol
