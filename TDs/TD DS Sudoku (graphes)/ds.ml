let rec appartient e l =
  match l with [] -> false | h :: q -> h = e || appartient e q

let rec supprime e l =
  match l with
  | [] -> []
  | h :: q -> if h = e then supprime e q else h :: supprime e q

let rec ajoute e l =
  match l with [] -> [ e ] | h :: q -> if h = e then l else h :: ajoute e q

let indice (b, r) = ((b / 3 * 3) + (r / 3), (b mod 3 * 3) + (r mod 3))

type litteral = X of int * int * int | NonX of int * int * int

(* II.A.1 *)
let case1 () =
  List.init 9 (fun i ->
      List.init 9 (fun j -> List.init 9 (fun k -> X (i, j, k + 1))))
  |> List.concat

let pp_litteral_list outchan l =
  let rec print_list = function
    | [] -> ()
    | [ X (i, j, k) ] -> Printf.fprintf outchan "x_(%d,%d)^%d" i j k
    | X (i, j, k) :: q ->
        Printf.fprintf outchan "x_(%d,%d)^%d; " i j k;
        print_list q
    | [ NonX (i, j, k) ] -> Printf.fprintf outchan "non x_(%d,%d)^%d" i j k
    | NonX (i, j, k) :: q ->
        Printf.fprintf outchan "non x_(%d,%d)^%d; " i j k;
        print_list q
  in
  Printf.fprintf outchan "[";
  print_list l;
  Printf.fprintf outchan "]"

let pp_clause_list outchan l =
  let rec print_list = function
    | [] -> ()
    | [ e ] -> Printf.fprintf outchan "%a" pp_litteral_list e
    | e :: q ->
        Printf.fprintf outchan "%a; " pp_litteral_list e;
        print_list q
  in
  Printf.fprintf outchan "[";
  print_list l;
  Printf.fprintf outchan "]"

let print_literal_list = Printf.printf "%a" pp_litteral_list
let print_clause = Printf.printf "%a" pp_clause_list

let bloc1 () =
  List.init 9 (fun b ->
      List.init 9 (fun k ->
          List.init 9 (fun r ->
              let i, j = indice (b, r) in
              X (i, j, k + 1))))
  |> List.concat

(* II.B.1 *)
let donnees (grille : int array array) =
  let rec aux i j acc =
    if i = 9 then acc
    else if grille.(i).(j) <> 0 then
      if j = 8 then
        aux (i + 1) 0
          (List.init 9 (fun k ->
               if k = grille.(i).(j) then [ X (i, j, k) ]
               else [ NonX (i, j, k) ])
          @ acc)
      else
        aux i (j + 1)
          (List.init 9 (fun k ->
               if k = grille.(i).(j) then [ X (i, j, k) ]
               else [ NonX (i, j, k) ])
          @ acc)
    else if j = 8 then aux (i + 1) 0 acc
    else aux i (j + 1) acc
  in
  aux 0 0 []

let numbloc i j = (i / 3 * 3) + (j / 3)

(* II.B.2.b *)
(* let interdites_ij (grille : int array array) (i : int) (j : int) =
   let rec interdit_col ?(acc : litteral list = []) k =
     if k = 9 then acc
     else if k <> i then
       interdit_col ~acc:(NonX (k, j, grille.(i).(j)) :: acc) (k + 1)
     else interdit_col ~acc (k + 1)
   in
   let rec interdit_ligne ?(acc : litteral list = []) k =
     if k = 9 then acc
     else if k <> j then
       interdit_col ~acc:(NonX (i, k, grille.(i).(j)) :: acc) (k + 1)
     else interdit_col ~acc (k + 1)
   in
   let rec interdit_bloc ?(acc : litteral list = []) k =
     if k = 9 then acc
     else
       let b = (i / 3 * 3) + (j / 3) in
       let new_i, new_j = indice (b, k) in
       if (new_i, new_j) <> (i, j) then
         interdit_col ~acc:(NonX (new_i, new_j, grille.(i).(j)) :: acc) (k + 1)
       else interdit_col ~acc (k + 1)
   in
   interdit_col 0 @ interdit_ligne 0 @ interdit_bloc 0 *)

(* II.B.2.c *)
let interdites (grille : int array array) =
  let rec supprime_doublons l =
    match l with [] -> [] | e :: q -> e :: supprime e q
  in
  List.init 81 (fun k -> interdites_ij grille (k / 3) (k mod 3))
  |> List.concat |> supprime_doublons
