# Chapitre 16 - Logique - Introduction

```toc
```

$\displaystyle\bigwedge_{i=1}^{10}$  
$\displaystyle\bigvee_{i=1}^{10}$  
$x_{(1, 1)}^5$

## II -
### A -
#### 1)
##### a)

Soit $(i, j) \in [[0, 8]]^2$, une clause serait :
$\displaystyle{\bigvee_{k=1}^{9} x_{(i, j)}^k}$ 

##### b)

$\forall\ (i, j) \in [[0, 8]]^{2},\ \exists\ k \in [[1, 9]], x_{(i, j)}^k$

##### c)

$\displaystyle\bigwedge_{i=0}^{8} \left(\displaystyle\bigwedge_{j=0}^{8}\left( \displaystyle{\bigvee_{k=1}^{9} x_{(i, j)}^k} \right) \right)$ 

##### d)

$K1$ contient $9 \times 9 = 81$ clauses.

##### e)


#### 2)

$\forall\ i \in [[0, 8]], \forall\ k \in [[1, 9]], \exists j \in [[0, 8]],\ x_{(i, j)}^{k}$
ce qui donne :
$\displaystyle\bigwedge_{i=0}^{8} \left(\displaystyle\bigwedge_{k=1}^{9}\left( \displaystyle{\bigvee_{j=0}^{8} x_{(i, j)}^k} \right) \right)$ 

$\forall\ i \in [[0, 8]],\exists\ j_{1} \in [[0, 8]],\ \exists\ j_{2} \in [[0, 8]] \backslash \lbrace j_1 \rbrace,\ \exists j_{3} \in [[0, 8]] \backslash \lbrace j_{1}, j_{2} \rbrace,\ \cdots,\ \exists j_{9}\in [[0, 8]] \backslash \lbrace j_{1}, j_{2}, \cdots, j_{8} \rbrace ,\ x_{(i, j_{1})}^{1}, x_{(i, j_{2})}^{2}, \cdots, x_{(i, j_{9})}^{9}$
#### 3)

##### a)

$C1 : \displaystyle\bigwedge_{j=0}^{8} \left(\displaystyle\bigwedge_{k=1}^{9}\left( \displaystyle{\bigvee_{i=0}^{8} x_{(i, j)}^k} \right) \right)$

$B1 : \displaystyle\bigwedge_{b=0}^{8} \left(\displaystyle\bigwedge_{k=1}^{9}\left( \displaystyle{\bigvee_{r=0}^{8} x_{\text{indice}(b, r)}^k} \right) \right)$

##### b)

#### 4)

##### a)

Soient $(i, k) \in [[0, 8]] \times [[1, 9]]$. La ligne de numéro $i$ de la grille contient au plus  
une fois la valeur $k$ $\iff$ $\forall\ j_{1}, j_{2} \in [[0, 8]],\ x_{(i, j_1)}^{k}\wedge x_{(i, j_{2})}^{k}\Rightarrow j_{1}=j_{2} \iff \forall\ j_{1}, j_{2} \in [[0, 8]],\ \overline{x_{(i, j_{1})}^{k}}\vee \overline{x_{(i, j_{2})}^{k}} \vee j_{1} \neq j_{2}$

donc 

$\displaystyle\bigwedge_{i=0}^{8} \displaystyle\bigwedge_{k=1}^{9} \displaystyle\bigwedge_{j=0}^{7} \displaystyle\bigwedge_{j'=j+1}^{8} \left( \overline{x_{(i, j)}^{k}}\vee \overline{x_{(i, j')}^{k}} \right)$

### B - 

#### 1)

#### 2)

##### a)

Soient $(i, j) \in [[0, 8]]^{2}$, la case $(i, j)$ appartient au bloc $b = (i \div 3) * 3 + (j \div 3)$

## III -

### A -
#### 1)

Une seule valuation satisfait $F_{\text{initiale}}$.

#### 4)

$F = x_{(0, 0)}^{1} \wedge \left( x_{(2, 2)}^{4} \vee x_{(3, 6)}^{6} \vee x_{(7, 7)}^{7} \right) \wedge \left( \lnot x_{(0, 0)}^{1} \vee \lnot x_{(3, 6)}^{6} \right)$
$F = \left( x_{(2, 2)}^{4} \vee x_{(3, 6)}^{6} \vee x_{(7, 7)}^{7} \right) \wedge  \lnot x_{(3, 6)}^{6}$
$F = x_{(2, 2)}^{4} \vee x_{(7, 7)}^{7}$

#### 5)

C'est un $7$

### B -

#### 1)

Si on déduit la clause vide de $F \wedge \lnot x$ alors $F \wedge \lnot x$ n'est pas réalisable donc nécessairement $F$ et $\lnot x$ sont incompatibles et donc il est nécessairement $x$ est nécessaire à $F$ donc $F \equiv F \wedge x$ 

