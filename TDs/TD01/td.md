# TD 01 - OCaml

## 1 Révisions

### 1.1 Mise en exercice

#### **Exercice 1**

Ecrire une fonction qui calcule la somme de 2 entiers /n/ et /p/. Donner son type. Ecrire une fonction qui prend 3 entiers /n/, /p/ et /q/ et renvoie le produit de q avec la somme de /n/ et /p/. Donner son type.

``` ocaml
let somme n p = n + p;;
(* val somme : int -> int -> int = <fun> *)

let qprod n p q = q * (somme n p);;
(* val qprod : int -> int -> int -> int = <fun> *)
```

#### **Exercice 2**

Ecrire une fonction `egal_2_ou_neg` qui renvoie `true` seulement si le nombre passé en paramètre vaut 2 ou s'il est négatif. Donner son type.
Cette fonction sera écrite en 3 versions :

* en utilisant `if then else`
* en utilisant le filtrage
* en utilisant seulement les opérateurs logiques

``` ocaml
let egal_2_ou_neg x = if (x < 0) || (x = 2) then true else false;;
let egal_2_ou_neg x = match (x < 0) with
    | true  -> true
    | false -> (x = 2);;
let egal_2_ou_neg x = (x = 2) || (x < 0);;
(* val egal_2_ou_neg : int -> bool = <fun> *)
```

### 1.2 Opérateurs logiques

#### **Exercice 3**

Simplifier l'expression suivante en utilisant les opérateurs `not`, `&&` et `||`.

``` ocaml
(* Original *)
let verify_og test1 test2 test3 = if test1 = false then
    if test2 = false then test3 else false
else
    if test3 = true then true else true = false;;

(* Simplifié *)
let verify_new test1 test2 test3 = (not test1 && not test2 && test3) || (test1 && test3)

(* Vérification *)
let checktrue test1 test2 test3 = assert ((verify_og test1 test2 test3) = (verify_new test1 test2 test3)); Printf.printf ("OK\n");;
```

### 1.3 Opérateur logique XOR

#### **Ecercice 4**

Ecrire une fonction `xor` qui effectue un ou exclusif entre 2 booléens. Donner son type.

``` ocaml
(* val xor : bool -> bool -> bool = <fun> *)
let xor bool1 bool2 = (not bool1 && bool2) || (not bool2 && bool1)
```

### 1.4 Curryfication

#### *Q1*

Déterminer le type des 2 fonctions suivantes :

```ocaml
let add1 (x, y) = x + y;; (* var add1 : int * int -> int) = <fun> *)
let add2 x y = x + y;; (* var add2 : int -> int -> int = <fun> *)
```

#### *Q2*

Ecrire une fonction `uncurry` qui prend un argument une fonction curryfiée à 2 arguments et renvoie la fontion non curryfiée. Donner son type.
Donner le type de `uncurry add2`.

```ocaml
let uncurry fonction (x, y) = fonction x y;; (* val uncurry : ('a -> 'b -> 'c) -> 'a * 'b -> 'c = <fun> *)
(* uncurry add2 : int * int -> int = <fun> *)
```

#### *Q3*

Ecrire la fonction réciproque `curry`. Donner son type. Donner le type de `curry add1`.

```ocaml
let curry fonction x y = fonction (x, y);; (* val curry : ('a * 'b -> 'c) -> 'a -> 'b -> 'c *)
(* curry add1 : int -> int -> int *)
```
