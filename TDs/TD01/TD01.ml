(* EX1 *)
let somme n p = n + p

(* val somme : int -> int -> int = <fun> *)
let qprod n p q = q * somme n p

(* val qprod : int -> int -> int -> int = <fun> *)

(* EX2 *)
let egal_2_ou_neg1 x = if x < 0 || x = 2 then true else false
let egal_2_ou_neg2 x = match x < 0 with true -> true | false -> x = 2
let egal_2_ou_neg3 x = x = 2 || x < 0

(* val egal_2_ou_neg : int -> bool = <fun> *)

(* EX3 *)
(* Original *)
let verify_og test1 test2 test3 =
  if test1 = false then if test2 = false then test3 else false
  else if test3 = true then true
  else true = false

(* Simplifié *)
let verify_new test1 test2 test3 =
  ((not (test1 && test2)) && test3) || (test1 && test3)

(* Vérification:
   let checktrue test1 test2 test3 = assert ((verify_og test1 test2 test3) = (verify_new test1 test2 test3)); Printf.printf ("OK\n");; *)

(* EX4 *)
(* val xor : bool -> bool -> bool = <fun> *)
let xor bool1 bool2 = ((not bool1) && bool2) || ((not bool2) && bool1)

(* Q1 *)
let add1 (x, y) = x + y

(* var add1 : int * int -> int) = <fun> *)
let add2 x y = x + y

(* var add2 : int -> int -> int = <fun> *)

(* Q2 *)
let uncurry fonction (x, y) = fonction x y

(* val uncurry : ('a -> 'b -> 'c) -> 'a * 'b -> 'c = <fun> *)
(* uncurry add2 : int * int -> int = <fun> *)

(* Q3 *)
let curry fonction x y = fonction (x, y)

(* val curry : ('a * 'b -> 'c) -> 'a -> 'b -> 'c *)
(* curry add1 : int -> int -> int *)

(* EX5 *)
(* val mystere1 : 'a list -> 'a list = <fun>;
   Arbre d'appel: mystere1 [3, 2, 1] => mystere1 [2, 1] => mystere1 [1] => mystere1 [] => [] => [1] => [1; 2] => [1; 2; 3];
   Cette fonction permet d'inverser une liste. *)

(* EX6 *)
(* TODO vérifier que l'exercice est faisable avec la prof *)

(* EX7 *)
let rec somme_n n =
  match n < 0 with
  | true -> failwith "Cette fonction ne prend que des entiers non signés !"
  | false -> n + somme_n (n - 1)

(* val somme_n : int -> int = <fun> *)
let somme_n n =
  if n < 0 then failwith "Cette fonction ne prend que des entiers non signés !"
  else n * (n + 1) / 2

(* La version 2 me semble plus efficace car elle ne prend pas de récursivité *)

(* EX8 *)
let rec longueur liste =
  match liste with [] -> 0 | head :: tail -> 1 + longueur tail

(* EX9 *)
let rec appartient element liste =
  match liste with
  | [] -> false
  | head :: tail -> if element = head then true else appartient element tail

(* EX10 *)
let indice element liste =
  let rec aux element liste' =
    match liste' with
    | [] -> None
    | head :: tail ->
        if element = head then Some (longueur liste - longueur tail)
        else aux element tail
  in
  aux element liste

(* val indice : 'a -> 'a list -> int option = <fun> *)
(* * Utiliser des fonctions auxiliaires pour garder l'état original d'une variable pendant la récursivité *)
(* ? Refactoriser pour utiliser la fonction appartient *)

(* EX11 *)
let rec somme liste =
  match liste with [] -> 0 | head :: tail -> head + somme tail
(* val somme : int list -> int = <fun> *)

(* EX12 *)
let rec maximum liste =
  Printf.printf "ATTENTION A BIEN ENTRER DES TYPES COMPARABLES!!!\n";
  match liste with
  | [] -> failwith "Aucun entier dans la liste !"
  | x :: [] -> x
  | head :: tail ->
      if head >= List.hd tail then maximum (head :: List.tl tail)
      else maximum tail

(* val maximum : 'a list -> 'a *)
let max3 a b c = maximum [ a; b; c ]

let max3_v2 a b c =
  if a >= b && a >= c then a
  else if b >= a && b >= c then b
  else if c >= a && c >= b then c
  else failwith "Les paramètres ne sont pas compatibles."

let max3_v3 a b c = if max a b = a then max a c else max b c

(* EX13 *)
let rec nieme idx liste =
  if idx < 0 then failwith "Index négatif impossible !"
  else
    match idx with 0 -> List.hd liste | _ -> nieme (idx - 1) (List.tl liste)

(* EX14 *)
let rec retourner liste =
  match liste with [] -> [] | head :: tail -> retourner tail @ [ head ]

(* EX15 *)
let rec sont_egales liste1 liste2 =
  match longueur liste1 = longueur liste2 with
  | false -> false
  | true -> (
      match liste1 with
      | [] -> true
      | head :: tail ->
          if head = List.hd liste2 then sont_egales tail (List.tl liste2)
          else false)

(* EX16 *)
let est_palindrome liste = liste = retourner liste
