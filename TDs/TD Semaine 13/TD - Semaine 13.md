## 1 - Analyse d'une fonction récursive

```c
#include <stdio.h>

inf f(int a, int b, int* pr) {
	printf("a = %d (début)\n", a);
	int q;
	if (a < b) {
		*pr = a;
		q = 0;
	} else {
		q = 1 + f(a - b, b, pr);
	}
	printf("a = %d (fin)\n", a);
	return q;
}

int main(void) {
	int r;
	int q = f(17, 5, &r);
	printf("q = %d, r = %d\n", q, r);
	return 0;
}
```

### Question 1

L'affichage produit par ce programme est "q = 3, r = 2"

### Question 2

La taille de la pile d'appel est de 4 blocs d'activations (si on prend en compte la fonction `main`).

### Question 3

### Question 4

Si on appelle `f` avec une valeur négative de `a` ou une valeur nulle de `b`, la fonction affiche 0 pour `q` et la valeur de `a` pour `r`.

### Question 5