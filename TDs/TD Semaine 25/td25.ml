type tas_arbre = Vide | Noeud of tas_arbre * int * tas_arbre
type tas_tableau = { taille : int; elements : int array }

(* Question 1 *)

let est_tas tas =
  let rec hauteur tas =
    match tas with
    | Vide -> 0
    | Noeud (g, _, d) -> 1 + max (hauteur g) (hauteur d)
  in
  let rec aux tas v =
    match tas with
    | Vide -> true
    | Noeud (Vide, x, Vide) -> x >= v
    | Noeud (Vide, _, _) -> false
    | Noeud (g, x, d) -> hauteur g >= hauteur d && aux g x && x >= v && aux d x
  in
  match tas with Vide -> true | Noeud (_, x, _) -> aux tas x && aux tas x

let t = Noeud (Noeud (Vide, 3, Vide), 1, Noeud (Vide, 2, Vide))

(* Question 2 *)
(* Les noeuds sont numérotés selon leur ordre dans le parcours en largeur de l'arbre *)

(* Question 3 *)
(* formule pour le père :
   - si on est le fils gauche : (i-1) / 2
   - si on est le fils droit : (i/2) - 1
   formule pour le fils :
   - fils gauche : i*2 + 1
   - fils droit : (i+1) * 2
*)

(* Question 4 *)
let pere ind = (ind - 1) / 2
let fils_g ind = (ind * 2) + 1
let fils_d ind = (ind + 1) * 2

(* Question 5 *)
let est_tas_tableau tab =
  let v = ref true and n = tab.taille in
  for i = 0 to n - 1 do
    if !v <> false then
      if pere i >= 0 then v := tab.elements.(pere i) <= tab.elements.(i)
  done;
  !v

(* Question 6 *)
