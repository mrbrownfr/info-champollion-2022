# TD du chapitre 4

## Compréhension de code

### Exercice 1

Après l'exécution des lignes de code 
```c
int a = 32;
int b = 10;
int *p = &a;
*p = b;
```
la valeur de `a` est 10 et la valeur de `b` est 10.

```c
void mystere(int *p, int *q)
{
	int val = *p;
	int i = 0;
	while (val >= *q)
	{
		val = val - *q;
		i = i + 1;
	}
	*q = i;
}
```

### Exercice 2

Lorsque `*p` vaut `21` et `*q` vaut `5`,

| `*p` | `*q` | `val` | `i` |
| :-: | :-: | :-: | :-: |
| 21 | 5 | 21 | 0 |
| 21 | 5 | 16 | 1 |
| 21 | 5 | 11 | 2 |
| 21 | 5 | 6 | 3 |
| 21 | 4 | 1 | 4 |

### Exercice 3

La fonction `mystere` effectue la division euclidienne de `*p` par `*q`.

## Écriture de code

### Exercice 4

```c
int somme_extremites(int tab[], int n)
{
	return tab[0] + tab[n-1];
}
```

### Exercice 5

```c
void echange(int *p, int *q)
{
	int temp = *p;
	*p = *q;
	*q = temp;
}
```

### Exercice 6

```c
void initialise_a_un(int tab[], int n)
{
    for (int i = 0; i < n; i++)
    {
        tab[i] = 1;
    }
}
```

### Exercice 7

```c
void echange_elements(int *tab, int i1, int i2)
{
	echange(&tab[i1], &tab[i2]);
}
```

### Exercice 8

```c
int appartient(int elem, int *tab, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (tab[i] == elem)
        {
            return i;
        }
    }
    return -1;
}
```

### Exercice 9

```c
int longueur_chaine(char *chaine)
{
	int i = 0;
	while (chaine[i] != "\0")
	{
		i += 1;
	}
	return i;
}
```

### Exercice 10

```c
int sont_egaux(int *tab1, int *tab2, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (tab1[i] != tab2[i])
        {
            return 0;
        }
    }
    return 1;
}
```
