# TD - Analyse de programmes - Situations

Pour chaque situation proposée :
- spécifier le problème et la fonction que vous allez écrire
- proposer une écriture **itérative** de l'algorithme
- prouver rigouresement la terminaison
- prouver rigouresement la correction
- éventuellement indiquer la complexité

## Situation 1
Calcul de la factorielle d'un nombre entier

```c
int factorielle(int n)
{
	assert (n >= 0);
	int resultat = 1;
	for (int i = 0; i <= n; i++)
	{
		resultat *= i;
	}
	return resultat;
}
```

*Terminaison* : 
Boucle `for` donc termine.

*Correction* :
tkt

## Situation 2



## Situation 3

Calcul de la partie entière du logarithme en base 2 d'un nombre entier naturel.
**Entrée** :
**Sortie** : Entier égal à -1 si $n \le 0$ ou égal à $\ent{\log_2(n)}$ si $n \ge 0$ 

```c
int log_2(int n)
{
	int compteur = 0;
	int variant = 1;
	while (n >= variant)
	{
		variant *= 2;
		compteur--;
	}
	return compteur-1;
}
```

*Terminaison* : variant croissant et condition majorée donc termine
*Correction* : 
$\forall n \in \mathbb{N}^{*,}P(n) :$ "A la ième itération, $2^{compteur} = variant$"


## Situation 4


```c
int rep_motif(char * mot, char* motif)
{
	int i=0; int p=0; int n=0;
	while(mot[i] != '\0')
	{
		if (mot[i] == motif[p])
		{
			p++; i++;
			if (mot[p] == '\0')
			{
				n++; i=i-p+1; p=0;
			}
		}
		else
		{
			i = i-p+1;
			p=0;
		}
	}
	return n;
}
```

*Terminaison* : `i` incrémenté 
*Correction* :
VARIANT : 
pour la ième lettre de mot, on a n+1 motif
sinon n motif

## Situation 5

```c

```

Invariant :
`ind_minimum` contient le minimum des ièmes premiers éléments

## Situation 6



## Situation 7
Tri bulles

*Spécifications* :
Entrées :
	- un pointeur vers une tableau d'entiers
	- sa taille
La fonction trie la tableau en appliquant le principe du tri bulles.

```c
void tri_bulles(int* tableau, int taille)
{
	for (int i = 0; i < taille; i++)
	{
		for (int j = 0; j < taille-i; j++)
		{
			if (tableau[j] > tableau[j+1])
			{
				int temp = tableau[j];
				tableau[j] = tableau[j+1];
				tableau[j+1] = temp;
			}
		}
	}
}
```

*Terminaison* : Des boucles `for`
*Correction* : 

*Complexité* : $O(n^2)$ (deux boucles `for` imbriquées)

## Situation 8

**Invariant** : $i \in [[0, n]]$, `row` contient la ième ligne du triangle de Pascal 

## Situation 9
Calcul de $a^n$ en utilisant (encore!) le principe d'exponentiation rapide.

Exponentiation rapide :
- 

```c
double puissance(double x, int n)
{

}
```


## Situation 10



## Situation 11



## Situation 12
Fusion de deux 


## Situation 13
