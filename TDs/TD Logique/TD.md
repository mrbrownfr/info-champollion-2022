## I - Logique propositionnelle

### Exercice 1

#### 1.

## II - Logique du premier ordre

### Exercice 1

$\varphi = \forall x.\forall y.\exists z.(\lnot(x < y) \lor ((x < z) \land (z < y)))$

#### 1. Donner l'arbre associé à cette formule

```mermaid
flowchart TD
1[FORALL x] --> 2[FORALL y] --> 3[EXISTS Z]
3 --> 4[OR]
4 --> 5[NOT] --> 6[x < y]
4 --> 9[AND] --> 11[x < z] & 14[z < y]
```

#### 2. Quelles sont ses formules atomiques ?

Les formules atomiques sont $(x < y), (x < z)$ et $(z < y)$.

#### 3. Quelles sont les variables libres ? liées ?

Aucune variable n'est libre, les variables liées sont $x, y, z$.

### Exercice 2

$E(x)$, $x$ est une école
$I(x)$, $x$ est un ordinateur connecté à internet
$L(x)$, $x$ est un ordinateur connecté au réseau local
$O(x, y)$, $x$ est un ordinateur de l'école $y$

Traduire en formule logiques du premier ordre les phrases suivantes :

#### 1. Dans une école, il existe des ordinateurs non connectés au réseau local.

$\exists x.O(x, y) \land \lnot L(x)$

#### 2. Dans les écoles, tous les ordinateurs sont connectés à un réseau local.

$\forall y.E(y) \Rightarrow (\forall x.O(x, y) \Rightarrow L(x))$

#### 3. Dans chaque école, au moins un ordinateur est connecté à la fois au réseau local et à internet.

$\forall y.E(y) \Rightarrow (\exists x.O(x, y) \land L(x) \land I(x))$

### Exercice 3

Pour chaque formule ci-dessous, donner une traduction en langage naturel et un exemple de tableau validant la formule. Les formules supposent l'existence d'un tableau $t$ de taille $n$.

#### 1.
$\forall i \in [[0, n]].\forall j \in [[0, n]].i\neq j \Rightarrow t[i] = t[j]$
Deux éléments d'indice distincts tu tableau sont forcément égaux $\Rightarrow$ tous les éléments du tableau sont égaux
$[1, 1, 1, 1, 1]$

#### 2.
$\forall i \in [[0, n]].\exists j \in [[0, n]].i\neq j \land t[i] = t[j]$
Chaque élément du tableau apparait au moins 2 fois.
$[1, 1, 2, 1, 2]$

#### 3.
$\exists i \in [[0, n]].\forall j \in [[0, n]].i\neq j \Rightarrow t[i] = t[j]$
Un élément du tableau est égal à tous les autres.
$[1, 1, 1, 1, 1]$

#### 4.
$\exists i \in [[0, n]].\exists j \in [[0, n]].i\neq j \land t[i] = t[j]$
Il existe deux élément du tableaux égaux l'un à l'autre.
$[1, 2, 3, 1, 5]$
