(** 
  @type [('a * 'b * 'c) array -> 'a * 'b * 'c]
  [max_arr arr] renvoie le maximum de [arr] selon le premier élément de ses triplets
  @param arr un array de triplets
*)
let max_arr arr =
  Array.fold_left
    (fun (y, d1, f1) (x, d2, f2) -> if x > y then (x, d2, f2) else (y, d1, f1))
    arr.(0) arr

(**
  @type [int array -> int * int * int]
  [solution arr] calcul la sous-séquence de [arr] de somme maximale  
  @param arr un array contenant des entiers relatifs
  @return un triplet contenant la somme, l'indice de départ dans l'array et l'indice de fin de la sous-séquence
*)
(* let solution arr =
     let dp = Array.init (Array.length arr) (fun i -> (arr.(i), 0, 0)) in
     for i = 1 to Array.length arr - 1 do
       let v, d, _ = dp.(i - 1) in
       if v < 0 then dp.(i) <- (arr.(i), i, i) else dp.(i) <- (arr.(i) + v, d, i)
     done;
     max_arr dp

   let solution arr =
     let dp = Array.init (Array.length arr) (fun i -> (arr.(i), 0, 0)) in
     let j = ref 0 in
     for i = 1 to Array.length arr - 1 do
       let v, d, _ = dp.(i - 1) in
       if v < 0 then dp.(i) <- (arr.(i), i, i) else dp.(i) <- (arr.(i) + v, d, i);
       let y, _, _ = dp.(!j) and x, _, _ = dp.(i) in
       if x >= y then j := i
     done;
     dp.(!j)

   let solution arr =
     let prev = ref (arr.(0), 0, 0)
     and cur = ref (arr.(1), 0, 0)
     and m = ref (arr.(0), 0, 0) in
     for i = 1 to Array.length arr - 1 do
       let v, d, _ = !prev in
       if v < 0 then cur := (arr.(i), i, i) else cur := (arr.(i) + v, d, i);
       let y, _, _ = !m and x, _, _ = !cur in
       if x >= y then m := !cur;
       cur := !prev
     done;
     !m *)

let solution arr =
  let cur = ref (arr.(0), 0, 0) and m = ref (arr.(0), 0, 0) in
  for i = 1 to Array.length arr - 1 do
    let v, d, _ = !cur in
    if v < 0 then cur := (arr.(i), i, i) else cur := (arr.(i) + v, d, i);
    let y, _, _ = !m and x, _, _ = !cur in
    if x >= y then m := !cur
  done;
  !m

(** 
  @type [int array -> unit]
  [print_int_array array] affiche les éléments de [array] dans le terminal
  @param array un array d'entiers
*)
let print_int_array array =
  if array = [||] then Printf.printf "[||]" else Printf.printf "[|";
  for i = 0 to Array.length array - 2 do
    Printf.printf "%d; " array.(i)
  done;
  Printf.printf "%d|]" array.(Array.length array - 1)

(** 
  @type [int array -> unit]
  [afficher_solution arr] affiche les éléments de [arr] ainsi que sa sous-séquence de somme maximale et cette somme
  @param array un array d'entiers
*)
let afficher_solution arr =
  let somme, debut, fin = solution arr in
  print_int_array arr;
  print_newline ();
  Printf.printf "somme : %d; sous-tableau : " somme;
  print_int_array (Array.sub arr debut (fin - debut + 1))

(* let solution arr =
   let abs_max = -int_of_float infinity and cur_max = 0 and d = ref 0 and f = ref 0 in
   for i = 1 to Array.length arr - 1 do
     cur_max := max !cur_max (!cur_max + arr.(i));
     if curr_max < 0 then dp.(i) <- (arr.(i), i, i) else dp.(i) <- (arr.(i) + v, d, i)
   done;
   max_arr dp *)

let test = [| 16; -2; 7; 3; 5; 12; -8; -15; 7 |]
(* [| 16; 14; 21; 24; 29; 41; 33; 18; 25 |] *)
