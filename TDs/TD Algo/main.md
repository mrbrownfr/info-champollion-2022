---
title: Présentation d'info
date: 2023-03-27
extensions:
  - terminal
---

# Problème d'info

<!-- stop -->

## Enoncé

Dans un tableau contenant des entiers relatifs, trouver la sous-séquence dont la somme des éléments est maximale. 

Une sous-séquence est une suite continue d'éléments.

<!-- stop -->

## Exemple

- `[| 6; -7; 14; 9 |]`

<!-- stop -->

- 23 (14+9)

---

# Solution au problème

```ocaml
(**
  @type [int array -> int * int * int]
  [solution arr] calcul la sous-séquence de [arr] de somme maximale
  @param arr un array contenant des entiers relatifs
  @return un triplet contenant la somme, l'indice de départ dans l'array et l'indice de fin de la sous-séquence
*)
let max_subseq arr =

  (**
    @type [('a * 'b * 'c) array -> 'a * 'b * 'c]
    [max_arr arr] renvoie le maximum de [arr] selon le premier élément de ses triplets
    @param arr un array de triplets
  *)
  let max_arr arr =
    let j = ref 0 in
    for i = 1 to Array.length arr - 1 do
      let x, _, _ = arr.(i) and y, _, _ = arr.(!j) in
      if x > y then j := i
    done;
    arr.(!j)
  in

  let dp = Array.init (Array.length arr) (fun i -> (arr.(i), 0, 0)) in
  for i = 1 to Array.length arr - 1 do
    let v, d, _ = dp.(i - 1) in
    if v < 0 then dp.(i) <- (arr.(i), i, i) else dp.(i) <- (arr.(i) + v, d, i)
  done;
  
  max_arr dp
```

- Entrées:
  - `arr` un tableau d'entiers relatifs
- Retourne:
  - un triplet contenant:
    - La valeur de la somme de la sous-séquence
    - L'indice de début
    - L'indice de fin

---

# Démonstration

```terminal-ex
command: utop -init main.ml
rows: 30
init_text: null
init_wait: null
init_codeblock: false
init_codeblock_lang: bash
```

---

# Comment ça marche ?

<!-- stop -->

`max_arr` est assez explicite, on ne va pas s'attarder dessus car peu intéressante.

<!-- stop -->

## Le gros du code

```ocaml
let dp = Array.init (Array.length arr) (fun i -> (arr.(i), 0, 0)) in
for i = 1 to Array.length arr - 1 do
  let v, d, _ = dp.(i - 1) in
  if v < 0 then dp.(i) <- (arr.(i), i, i) else dp.(i) <- (arr.(i) + v, d, i)
done;
max_arr dp
```

- `dp`: une liste contenant des triplets comme suit: (valeur de la case, indice de départ de la sous-séquence, indice de fin de la sous-séquence)
- Pour chaque sous-séquence:
  - Si le nombre d'avant est négatif, on l'ignore, il fait juste baisser
  - Sinon, on additionne les deux valeurs en conservant l'indice de début de la valeur précédente

On prend enfin la somme maximum des sous-séquences

---

# Exemples

- `[| 16; -2; 7; 3; 5; 12; -8; -15; 7 |]`
<!-- stop -->
- `[| (16, 0, 0); (14, 0, 1); (21, 0, 2); (24, 0; 3); (29, 0, 4); (41, 0, 5); (33, 0, 6); (18, 0, 7); (25, 0, 8) |]`

<!-- stop -->

- `[| 16; -1; -2; 14; 6; -12 |]`
<!-- stop -->
- `[| (16, 0, 0); (15, 0, 1); (13, 0, 2); (27, 0, 3); (33, 0, 4); (21, 0, 5) |]`

<!-- stop -->

- `[| 16; -17; 14; 6 |]`
<!-- stop -->
- `[| (16, 0, 0); (-1, 0, 1); (14, 2, 2); (20, 2, 3) |]`

---
# Pourquoi cet algorithme ?

<!-- stop -->

## Avantages

- Complexité temporelle en __O(n)__ :
  - Un passage pour initialiser la liste
  - Un passage pour tout sommer
  - Un passage pour trouver le maximum
- Complexité spatiale en __O(n)__:
  - Un n-uplet prend n+1 unités de mémoire (car représenté comme un tableau en OCaml)
  - Un tableau prend n+1 unités de mémoire
  - => O(4(n+1)) = O(n)
- Met en place une stratégie de programmation dynamique

<!-- stop -->

## Inconvénients

- Pas forcément évident à comprendre au premier abord
- Peut-être une technique plus rapide ? Mais on ne l'a pas trouvé

<!-- stop -->

## Programmation dynamique ?

Comme on a pu le voir dans le grand bloc de code, on a divisé le problème en plusieurs sous-problèmes non indépendants:
- Maximum de la liste
- Sommation des sous-séquences
On applique aussi de la mémoïsation en gardant en mémoire les résultats précédents (les sommes et indices).

---

# Comment optimiser le programme encore plus ?

- On peut enlever un passage sur la liste en calculant le maximum au moment de la comparaison des valeurs
- On peut enlever un passage sur la liste + une allocation supplémentaire en gardant en mémoire uniquement les valeurs _intéressantes_

<!-- stop -->

```ocaml
let solution arr =
  let cur = ref (arr.(0), 0, 0) and m = ref (arr.(0), 0, 0) in
  for i = 1 to Array.length arr - 1 do
    let v, d, _ = !cur in
    if v < 0 then cur := (arr.(i), i, i) else cur := (arr.(i) + v, d, i);
    let y, _, _ = !m and x, _, _ = !cur in
    if x >= y then m := !cur
  done;
  !m
```

<!-- stop -->

```terminal-ex
command: utop -init main.ml
rows: 15
init_text: null
init_wait: null
init_codeblock: false
init_codeblock_lang: bash
```