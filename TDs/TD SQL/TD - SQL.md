# TD SQL

## Schéma relationnel

![[Web/src/Pasted image 20230130115449.png]]

## Exercices

### Question 1

Insérer les n-uplets ('Zoe', 'rage de dents') et ('Zoe', 'grippe') dans la table LesMaladies.

Insérer le n-uplet ('Charlie', 'femelle', 'insecte archipètre', , , 3) dans la tables LesAnimaux.

### Question 2

#### 1.
```sql
SELECT nomA FROM LesAnimaux;
```

#### 2.
```sql
SELECT DISTINCT nomM FROM LesMaladies;
```

#### 3.
```sql
SELECT nomE FROM LesSurveillants S
JOIN LesEmployés E ON S.nomE = E.nomE
WHERE E.adress LIKE '%Calvi%';
```

### Exercice 3

#### Requête 1
fonctions prises en charge dans la réserve

#### Requête 2
nom de tous les léopards

#### Requête 3
nom du surveillant et n° de la zone pour les zones surveillées par des habitants de Calvi

#### Requête 4

#### Requête 5

### Exercice 4

#### Requête 1
```sql
SELECT nomA FROM LesAnimaux NOT IN
(SELECT nomA FROM LesMaladies);
```

#### Requête 2
```sql
SELECT nomA, type FROM LesAnimaux A
LEFT JOIN LesMaladies M ON A.nomA = M.nomM
WHERE LesMaladies.nomA IS NULL;
```

#### Requête 3
```sql
SELECT nomA FROM LesAnimaux A
JOIN LesMaladies M ON A.nomA = M.nomM
WHERE A.pays = 'France' AND M.nomM = 'grippe';
```

#### Requête 4
```sql
SELECT noZone, fonction FROM LesZones Z
LEFT JOIN LesAnimaux A ON A.noZone = Z.noZone
WHERE A.nom IS NULL;
```

#### Requête 5
```sql
SELECT * FROM LesMaladies M
JOIN LesAnimaux A ON M.nomA = A.nomA
WHERE A.sexe = 'Hermaphrodite';
```

#### Requête 6
```sql
SELECT nomA, type, anNais FROM LesAnimaux
WHERE noZone = (SELECT noZone FROM LesAnimaux WHERE nomA = 'tor');
```

#### Requête 7
```sql
SELECT nomA, type, anNais FROM LesAnimaux A
JOIN LesMaladies M ON A.nomA = M.nomA
GROUP BY M.nomA HAVING count(DISTINCT nomM) =
count(SELECT DISTINCT nomM FROM LesMaladies);
```

#### Requête 8
```sql
SELECT Z.noZone, Z.fonction FROM LesAnimaux A
JOIN LesAnimaux Z ON Z.noZone = A.noZone
GROUP BY A.noZone HAVING count(DISTINCT type) >= 2;
```