# Chapitre 15 - Graphes - TD

## Exercice 7

```mermaid
flowchart LR
subgraph G3
direction TB
G[000] --- H[001] & I[010] & K[100]
K --- L[110] & M[101] --- N[111]
I --- L & O
H --- M & O
O[011] --- N
end
subgraph G2
direction LR
C[00] --- D[01] & E[10] --- F[11]
end
subgraph G1
direction LR
A[0] --- B[1]
end
```

$G_{n}$ est d'ordre $n$ et $n = 1 \Rightarrow |A| = 1,\ \forall n \ge 2,\ |A| = 2 n!$ 
