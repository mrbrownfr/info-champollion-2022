## 1 - Révisions

### Nombres naturels

Le plus petit nombre réprésentable avec 6 chiffres en écriture positionnelle en base 2 est $0$, et le plus grand est $2^{6}-1= 63$.

| Base 10      | Base 2           | Base 8    | Base 16     |
| ------------ | ---------------- | --------- | ----------- |
| $(11)_{10}$  | $(1011)_2$       | $(13)_8$  | $(B)_{16}$  |
| $(15)_{10}$  | $(1111)_2$       | $(17)_8$  | $(F)_{16}$  |
| $(16)_{10}$  | $(1\;0000)_2$    | $(20)_8$  | $(10)_{16}$ |
| $(17)_{10}$  | $(1\;0001)_2$    | $(21)_8$  | $(11)_{16}$ |
| $(162)_{10}$ | $(1010\;0010)_2$ | $(242)_8$ | $(A2)_{16}$ |
| $(128)_{10}$ | $(1000\;0000)_2$    | $(200)_8$ | $(80)_{16}$  |

### Complément à 2

**Principe** : convertir la valeur absolue de l'entier en base $2$ sur $p-1$ bits, inverser tous les bits ($0$ deviennent $1$, $1$ deviennent $0$), ajouter $1$ et rajouter un $1$ devant si le nombre est négatif et un $0$ sinon.

Avec $5$ bits, le plus petit entier relatif représentable de cette manière est $-2^{5-1} = -16$ et le plus grand est $2^{5-1}-1 =15$

### Manipulation de limites

```ocaml
type signe = 
	| Plus
	| Moins
```

```ocaml
type limite = 
	| Infini of signe
	| Zero of signe
	| Nombre of float
	| FormeIndeterminee
```

#### Question 1

```ocaml
let changement_signe signe1 = match signe1 with
	| Plus -> Moins
	| Moins -> Plus
(* type : signe -> signe *)
```

#### Question 2

```ocaml
let regle_des_signes signe1 signe2 = 
	if signe1 = signe2 then Plus
	else Moins
```

#### Question 3

```ocaml
let oppose limite1 = match limite1 with
	| FormeIndeterminee -> FormeIndeterminee
	| Infini signe -> Infini changement_signe signe
	| Zero signe -> Zero changement_signe signe
	| Nombre valeur -> Nombre (-.valeur)
```

#### Question 4

`oppose (Infini Plus)` renvoie `Infini Moins` et `oppose (Nombre 48)` renvoie `Nombre -48` 

#### Question 5

```ocaml
let inverse limite1 = match limite1 with
	| FormeIndeterminee -> FormeIndeterminee
	| Infini signe -> Zero signe
	| Zero signe -> Infini signe
	| Nombre valeur -> Nombre (1. /. valeur)
```

## 2 - Petits problèmes

### Réservation de trains

#### Question 1

```c
int peuvent_prendre_le_train(int *p, int n, int k, int c)
{
	int *places_libres = (int)malloc(sizeof(int) * k);
	for (int i = 0; i < k; i++)
	{
		places_libres[i] = c;
	}
	for (int i = 0; i < n; i++)
	{
		places_libres[p[i]] -= 1;
		if (places_libres[p[i]] < 0)
		{
			return 0;
		}
	}
	free(places_libres);
	return 1;
}
```

#### Question 2

Il n'existe pas toujours une façon de remplir les trains pour faire voyager tout le monde.

```c
int bourrage_train(int *p, int n, int k, int c)
{
	int *places_libres = (int)malloc(sizeof(int) * k);
	for (int i = 0; i < k; i++)
	{
		places_libres[i] = c;
	}
	int j = 0;
	for (int i = 0; i < n; i++)
	{
		if (places_libres[j] < 0)
		{
			j++;
			if (j >= k)
			{
				return 0;
			}
			p[i] = j;
		} 
		places_libres[j] -= 1;
	}
	return 1;
}
```

#### Question 3

```c
int *repartition_voyageurs(int *p, int n, int k, int c)
{
	int *places_libres = (int)malloc(sizeof(int) * k);
	for (int i = 0; i < k; i++)
	{
		places_libres[i] = c;
	}
	for (int i = 0; i < n; i++)
	{
		if (places_libres[p[i]] < 0)
		{
			int j = p[i];
			while (j < k)
			{
				if (places_libres[p[j]] >= 0)
				{
					places_libres[p[j]] -= 1;
					p[i] = j;
				}
				j++;
			}
			return 0;
		}
		places_libres[p[i]] -= 1;
	}
	free(places_libres);
	return 1;
}
```

#### Question 4

Pareil que 3.

### Deuxième plus grand élément

#### Question 1

```c
int deuxieme_plus_grand_element(int *tab, int n)
{
	int max_ind;
	int max_ind2;
	if (tab[0] < tab[1])
	{
		max_ind = 1;
		max_ind2 = 0;
	}
	else
	{
		max_ind = 0;
		max_ind1 = 1;
	}
	for (int i = 2; i < n; i++)
	{
		if (tab[i] > tab[max_ind])
		{
			max_ind2 = max_ind;
			max_ind = i;
		}
		else if (tab[i] > tab[max_ind2])
		{
			max_ind2 = i;
		}
	}
	return max_ind2;
}
// entre n et 2n comparaisons sont effectuées
```

#### Question 2


