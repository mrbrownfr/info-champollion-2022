# Chapitre 12 et 13 - Applications

## Relations d'ordre et induction

### Exercice 1
$(3, 5)$ et $(5, 3)$ sont les éléments minimaux
$(5, 7)$ et $(7, 5)$ sont les éléments maximaux
Il n'y a pas de plus grand ni de plus petit élément.
Les minorants sont $(1, 3), (3, 1)$
Les majorants sont $(7, 7)$
 
### Exercice 2


### Exercice 3

### Exercice 4
#### Question 1
#### Question 2

## Sur les arbres binaires

### Exercice 5

### Exercice 6

### Exercice 7
#### Question 0
#### Question 1
#### Question 2
#### Question 3

### Exercice 8

### Exercice 9
#### Question 1
#### Question 2
#### Question 3
#### Question 4

### Exercice 10

### Exercice 11
#### Question 1
#### Question 2