# Chapitre 19 - Recherche textuelle

```toc
```

## I - Introduction

> [!note] Définition
> Un **alphabet** est un ensemble fini et non vide d'éléments, appelés **symboles**, **lettres** ou **caractères** noté $\Sigma$

> [!note] Définition
> Un **mot** sur un alphabet $\Sigma$ est une suite finie d'éléments de $\Sigma$. La concaténation de deux mots $x$ et $y$ s'écrit $xy$ ou $x \circ y$. Le mot vide est représenté $\varepsilon$.

> [!note] Définition
> Un mot $x$ est **préfixe** d'un mot $y$ s'il existe un mot $z$ tel que $y=xz$.
> Un mot $x$ est **suffixe** d'un mot $y$ s'il existe un mot $z$ tel que $y=zx$.

> [!note] Définition
> Un **texte** est un tableau d'éléments de caractères appartenant à un alphabet.

> [!tip] Recherche d'une chaîne de caractères
> Soit $T$ un texte de longueur $n$ et $P$ un **motif**, un texte de longueur $m \leq n$.
> Le problème est de trouver toutes les **occurences** de $P$ dans $T$.

> [!note] Définition
> Le motif $P$ apparaît avec un **décalage** $s$ si :
> $\begin{cases} s \in [[0, n - m]] \\ \forall i \in [[0, m-1]], T[s+i] = P[i] \end{cases}$

## II - Algorithme naïf

> [!tldr] Consigne
> Ecrire un algorithme naïf qui permet de rechercher un **motif** dans un **texte**. On renverra une liste chaînée de décalages.

```pseudo-code
recherche_naive(P, T):
	l <- liste_vide()
	n <- longueur(T)
	m <- longueur(P)
	pour s <- 0 à n-m :
		si s est un décalage :
			l <- ajout_en_tete(l, s)
	retourner l
```

> [!example] Exercice
> Déterminer la complexité de l'algorithme naïf en pire cas.
> Exhiber une situation dans laquelle la complexité est atteinte.
> $O(m \times (n - m + 1))$

## III - Algorithme de Boyer-Moore

> [!link] Lien
> [Algorithme de Boyer-Moore](https://fr.wikipedia.org/wiki/Algorithme_de_Boyer-Moore)

### A - Variante de Horspool

> [!note] Définition
Une **table de décalage** est une association entre une lettre et sa distance à la dernière lettre du motif.

```pseudo-code
s <- 0
tant que s <= n - m :
	parcourir le motif / texte en partant de la fin
			si on a tout parcouru on a une occurence
				s <- s + 1
			sinon décaler le motif en utilisant le tableau de décalage
				s <- s + delta(c) // avec c le caractère du texte pour lequel il n'y avait pas de correspondance
```

 ### B - Version de Boyer-Moore

- table de décalage qui indique la position de chaque caractère de l'alphabet par rapport à la fin
- table de décalage de la taille du motif qui indique en fonction d'un $j$ dans le motif la position $k$ à laquelle on peut retrouver le sous-motif


### C - Version simplifiée

