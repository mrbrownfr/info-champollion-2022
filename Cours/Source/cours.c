typedef struct tab_s = {
    int capacite,
    int num,
    int *donnees,
} tableau;

void push_back(tableau *t, int c)
{
    if (t->num == 0)
    {
        t->donnees = (int *)malloc(sizeof(int));
        if (t->donnees == NULL)
        {
            exit(EXIT_FAILURE);
        }
        t->capacite = 1;
        t->num = 1;
        t->donnees[0] = x;
    }
    else
    {
        if (t->num < t->capacite)
        {
            t->donnees[t->num] = x;
            t->num++;
        }
        else
        {
            int *nouveau = (int *)malloc(sizeof(int) * t->capacite * 2);
            if (nouveau == NULL)
            {
                exit(EXIT_FAILURE);
            }
            for (int i = 0; i < t->capacite; i++)
            {
                nouveau[i] = t->donnees[i];
            }
            free(t->donnees);
            t->donnees = nouveau;
            t->capacite = t->capacite * 2;
            t->donnees[t->num] = x;
            t->num++;
        }
    }
}