# Chapitre 15 - Graphes et algorithmes associés

```toc
```

## I - Graphes

### A - Graphe orienté

> [!définition]
> Un **graphe orienté** est défini par un ensemble de **sommets / noeuds** $S$ et un ensemble $A \subseteq S \times S$ de couple de sommets, appelés **arcs**.

> [!example] Exemple
$S = \left\{ a, b, c, d, e, f \right\}$
$A = \left\{ (a, b), (a, d), (b, c), (b, d), (c, d), (d, b), (e, f) \right\}$
> ```mermaid
> flowchart LR
> 
> A --> B & D
> B --> C
> C --> D
> B <--> D
> E --> F
> ```


> [!note] Définitions
> Si $(x, y) \in A$ alors on appelle:
> - $y$ le **successeur** $x$
> - $x$ le **prédécesseur** de $y$
> $y$ est un voisin de $x$

> [!note] Définition
> Un arc de la forme $(x, x)$ est appelé une **boucle**.

> [!note] Définitions
> Pour un sommet $x \in S$ :
> - le nombre d'arcs de la forme $(x, y \in S)$ est appelé **degré sortant** du sommet $x$, on le note $d_+(x)$
> - le nombre d'arcs de la forme $(y \in S, x)$ est appelé **degré entrant** du sommet $x$, on le note $d_-(x)$

> [!note] Définitions
> Un **chemin** du sommet $u$ au somet $v$ dans un graphe $G = (S, A)$ est une séquence de sommets $x_{0}, \cdots, x_{n} \in S$ tels que $u = x_{0},\ v = x_n$ et $\forall i \in [[0; n-1]],\ (x_{i}, x_{i+1}) \in A$.
> La **longueur** de ce chemin est $n$, c'est le nombre d'arcs à utiliser.
> Un chemin est **simple** s'il n'y a pas de répétition d'arrêtes.
> Un chemin est **élémentaire** s'il n'y a pas de répétition de sommets.
> Un **cycle** est un chemin de $x$ à $x$ de longueur $n > 0$.

> [!note] Définition
> Un **DAG** : **D**irected **A**cycle **G**raph est un graphe orienté acyclique.

> [!note] Définitions
> On dit qu'un sommet $v$ est **accessible** depuis un sommet $u$ s'il existe un chemin de $u$ à $v$.
> Une composante **fortement connexe** d'un graphe est un ensemble de sommets tous accessibles deux à deux.
> Un graphe est **complet** si tous ses sommets sont voisins deux à deux.

> [!note] Définitions
> Dans un graphe orienté, on note :
> - **racine** (ou **source**) tout sommet de degré entrant nul
> - **feuille** (ou **puits**) tout sommet de degré sortant nul

> [!note] Définition
> Un **ordre topologique** sur un graphe orienté $G$ est un ordre total sur les sommets de $G$ tel que s'il y a un arc de $u$ vers $v$ alors $u \prec v$.

> [!example] Exemple
> Le graphe suivant possède un ordre topologique :
> ```mermaid
flowchart LR
0 --> 1 & 8
7 --> 8
6
1 --> 8 & 2
3 --> 2 & 4 --> 5
>```
>Le graphe suivant ne possède pas d'ordre topologique :
>```mermaid
>flowchart LR
>0 --> 1 --> 2 --> 0
>```



> [!tip] Théorème
Un graphe orienté $G = (S, A)$ admet un ordre topologique $\Leftrightarrow$ il est *acyclique*.

> [!abstract] Preuve
$\Rightarrow$ Soit $G$ un graphe qui admet un ordre topologique.
Si $G$ admet un cycle, choissons deux sommets $u$ et $v$ de ce cyle, alors d'une part il existe un chemin de $u$ à $v$ et d'autre part il existe un chemin de $v$ à $u$ donc $u \prec v$ et $v \prec u$ absurde.
Donc si $G$ admet un ordre topologique, alors il est acyclique.
$\Leftarrow$ Soit $P(n)$ la propriété "si $G = (S, A)$ avec $n = |S|$ est un graphe orienté acyclique alors il admet un ordre topologique"
Conservation :
Soit $n \ge 1$ tel que $P(n)$ soit vraie. Soit $G = (S, A)$ un DAG tel que $|S| = n + 1$. Puisque $G$ est acyclique, il existe un sommet $s$ de degré sortant nul et soit $G'$ le graphe obtenu en retirant ce sommet.
On a $G' = (S\setminus\left\{s\right\}, A\setminus\left\{\cdots, s\right\})$
Par HR, puisque le nombre de sommet de $G'$ est $|A\setminus\left\{s\right\}| = n$ alors il admet un ordre topologique.
(*Preuve incomplète*)

### B - Graphe non-orienté

Il existe plusieurs familles de graphes, voici quelques exemples :
> [!example] Exemples
> Graphe entièrement déconnecté :
> ```mermaid
> flowchart TD
> A
> B
> C
> D
> E
>```
> Graphe complet, noté $K_n$ avec $n$ le nombre de sommets :
> ```mermaid
>flowchart TD
>subgraph K5
>A((1)) --- B((2)) --- C((3)) --- D((4)) --- E((5))
>A --- C --- E --- B --- D --- A
>end
>subgraph K4
>F((1)) & G((2)) --- H((3)) & I((4))
>F --- G
>I --- H
>end
>subgraph K3
>K((1)) --- L((2)) --- M((3)) --- K
>end
>subgraph K2
>N((1)) --- O((2))
>end
>subgraph K1
>P((1))
>end
>```
>Graphe cycle $C_n$ :
>```mermaid
>flowchart TD
>subgraph C5
>A((1)) --- B((2)) --- C((3)) --- D((4)) --- E((5)) --- A
>end
>subgraph C3
>F((1)) --- G((2)) --- H((3)) --- F
>end
>```
>Graphe *bipartis*, pour lesquels on peut partitionner l'ensemble des sommets $S$ et $S1$ et $S2$ de telle sorte que toutes les arêtes du graphe relient un sommet de $S_1$ à $S_2$ :
>```mermaid
>flowchart LR
>subgraph S2
>A
>B
>C
>end
>subgraph S1
>D --> A
>E --> B & C
>F
>end
>```


> [!note] Définitions
> Un **graphe non orienté** est défini par un ensemble $S$ de sommets et un ensemble $A$ de paires non orientées de sommets appelées **arêtes**.
> Le **degré** d'un sommet est son nombre de voisins.

> [!tip] Proposition
$\sum\limits_{i\in S} \text{deg}\ i = 2|A|$ 

> [!tip] Lemme des poignées de main
> Dans un graphe, il y a un nombre pair de sommets de degré impair.

> [!note] Définitions
> Un graphe non orienté $G = (S, A)$ est **connexe** si, pour toute paire de sommets $x$ et $y$ de $S$, il existe un chemin de $x$ à $y$.
> Une **composante connexe** de $G$ est un sous-ensemblee de sommets deux à deux reliés par des chemins, maximal pour l'inclusion.

> [!note] Définitions
>Un **isomorphisme** entre deux graphes orientés $G_{1}= (S_{1},A_1)$ et $G_{2}= (S_{2},A_2)$ est une bijection telle que $f : S_{1} \longrightarrow S_2$ telle que :
> $\forall\ x, y \in S_{1}, (f(x), f(y)) \in A_{2}\Leftrightarrow (x, y) \in A_1$
> Un **isomorphisme** entre deux graphes non orientés $G_{1}= (S_{1},A_1)$ et $G_{2}= (S_{2},A_2)$ est une bijection telle que $f : S_{1} \longrightarrow S_2$ telle que :
> $\forall\ x, y \in S_{1}, \left\{f(x), f(y)\right\} \in A_{2}\Leftrightarrow \left\{x, y\right\} \in A_1$
> Deux graphe sont dits **isomorphes** s'il existe un isomorphisme entre eux.

> [!example] Exemple
> ```mermaid
flowchart TD
subgraph one
1 --> 2 --> 4
1 --> 3 --> 5
2 --> 3
4 --> 5
end
>
subgraph two
direction RL
A --> B --> D
D --> E
B --> C
A --> C --> E
end
>```

### C - Arbres et graphes

> [!info] Remarques
> Un arbre est un graphe :
> - non orienté
> - connexe
> - acyclique
>
>De plus :
> - un graphe de ce type est parfois appelé arbre **libre**, ou arbre **non enraciné**, il n'a en effet pas de racine
> - si un graphe est non orienté et acyclique : il s'agit d'une forêt

> [!tip] Graphe des arbres
> Soit $G = (S, A)$ un graphe non orienté.
> Il y a équivalence entre :
> ```mermaid
flowchart TD
A("(0) G est un arbre") --> B("(1) Deux sommets quelconques de G sont reliés par un chemin élémentaire unique")
B --> C("(2) G est connexe mais si on enlève un sommet à S le graphe résultant n'est plus connexe")
C --> D("(3) G est connexe et |A| = |S| - 1")
D --> E("(4) G est acyclique et |A| = |S| - 1")
E --> F("(5) G est acyclique, mais si on ajoute une arête à A, le graphe résultant contient un cycle") --> A
>```

## II - Représentation des graphes

> [!tldr] Représentations
> Deux représentations classiques des graphes :
> - matrice d'adjacence
> - listes d'adjacence

> [!note] Complexités temporelles
> Soit $G = (S, A)$ avec $n = |S|$, $m = |A|$, on obtient les complexités suivantes sur les opérations :

*Matrice d'adjacence* :

| Opération      | Complexité |
| -------------- | ---------- |
| `create_graph` | $O(n^2)$   |
| `has_edge`     | $O(1)$     |
| `add_edge`     | $O(1)$     |
| `neighbours`   | $O(n)$     |
| `all_egdes`    | $O(n^2)$           |
*Listes d'adjacence* :

| Opération      | Complexité |
| -------------- | ---------- |
| `create_graph` | $O(n + m)$ |
| `has_edge`     | $O(n)$     |
| `add_edge`     | $O(1)$     |
| `neighbours`   | $O(1)$     |
| `all_egdes`    | $O(n + m)$ |

> [!note] Complexité spatiale
> - *matrices d'adjacence* : $O(n^2)$
> - *listes d'adjacences* : $O(n + m)$

## III - Algorithmes

### A - Introduction

> [!example] Exemple : ordre topologique
>```ocaml
> let rec topo ?(ordre : int list = []) (g : graph) =
>   if List.length ordre = nb_vertices g then ordre
>   else
>     let rec aux i =
>       if g.(i) = [] && (not @@ List.mem i ordre) then
>         topo ~ordre:(i :: ordre) (Array.map (List.filter (( <> ) i)) g)
>       else aux (i + 1)
>     in
>     aux 0
> ```

### B - Parcours en profondeur

> [!warning] Référence
> Voir algorithme sur fiche
> ```
> Initialisations :
> début
> 	visités <- ensemble vide
> fin
> Procédure Visiter(s) :
> début
> 	si s pas dans visités alors
> 		visités <- visités U s
> 		pré-traitement(s)
> 		pour chaque successeur v de s faire
> 			Visiter(v)
> 		fin
> 		post-traitement(s)
> 	fin
> fin
> Visiter(s)
> ```

> [!tldr] Terminaison
> Chaque appel de `Visiter()` termine immédiatement ou fait diminuer le nombre de sommets qui ne sont pas dans `visités`, le graphe étant fini l'algorithme termine donc.

> [!tldr] Correction
> - Après un appel à `Visiter(s)` pour tout sommet $y \in \text{visités}$ il existe un chemin entre $s$ et $y$. La pile des appels donne le chemin de $s$ à $y$.
> - Après l'appel à `Visiter(s)`, s'il existe un chemin entre $s$ et $y$ alors `Visiter(y)` a été appelé.
> 	- longueur nulle : $y=s$
> 	- si la longueur est $n>0$, on appelle $w$ le dernier sommet atteint avant $y$, le chemin entre $s$ et $w$ est de longueur $n-1$ donc le sommet $w$ est visité et `Visiter(w)` a été appelé donc d'après le code `Visiter(y)` a été appelé puisqu'il existe un arc entre $w$ et $y$.

> [!tldr] Conclusion
> La parcours en profondeur de $s$ permet de visiter exactement les sommets accessibles depuis $s$.

> [!note] Remarque
> Si $y$ est accessible depuis $s$ alors les traitements sont effectués dans l'ordre $\text{pré}(s) \prec \text{pre}(y) \prec \text{post}(y) \prec \text{post}(s)$

> [!info] Parcours complet
> Pour effectuer un parcours complet du graphe $G = (S, A)$, remplacer la ligne $16$ par une boucle qui visite tous les sommets de $S$

> [!tldr] Complexité
> On impose l'ensemble `visités` à être représenté par un tableau de booléens où `visités[s]` est vrai $\iff$ `Visiter(s)` a été appelé
> Création de `visités` et parcours de la ligne $16$ : $O(|S|)$
> Appel sur un sommet déjà visité : $O(1)$
> Sinon, dépend du nb de successeurs du sommet, mais chaque arc n'est visité qu'une seule fois donc $O(|A|)$
> La complexité finale est $O(|S| + |A|)$

> [!example] Ordre post-fixe
> ```
> Initialisations :
> début
> 	liste <- pile vide
> fin
> pré-traitement(s):
> 	// ne rien faire
> fin
> post-traitement(s):
> 	empiler(liste, s)
> fin
> ```
> Graphe :
> ```mermaid
> flowchart LR
> 5 --> 1 --> 3 --> 6
> 5 --> 4 --> 0 --> 2
> 5 --> 7 --> 2
> 3 --> 7
> 4 --> 7
> 6 --> 4
> 6 --> 0 --> 2
>```
> Parcours infixe :
>`[5; 1; 3; 6; 4; 7; 0; 2]` où le sommet de la pile est à gauche

> [!tip] Propriété
> Sur un DAG, l'ordre postfixe renvoyé par le parcours complet est un tri topologique.

> [!tldr] Preuve
> Soit $u \rightarrow v$ un arc de $G$, lorsque `Visiter(u)` est appelé pour la première fois :
> - soit $v \in$ `visités` et dans ce cas $v$ est déjà dans la pile donc $u$ sera ajouté au dessus
> - sinon `Visiter(u)` entraine l'appel `Visiter(v)` donc `post-traitement(v)` effectué avant `post-traitement(u)` et donc $u$ est ajouté après $v$ dans la pile

> [!example] Graphes d'exemples
> ```mermaid
> flowchart TD
> subgraph G1
> direction LR
> 0 --> 1 --> 2
> 0 --> 2
> 1 --> 3
> 1 --> 4
> 3 --> 0
> 3 --> 5
> 4 --> 1
> 4 --> 2
> 5 --> 10
> 6 --> 1
> 6 --> 9
> 7 --> 8
> 8 --> 6
> 9 --> 7
> 9 --> 10
> 10 --> 11
>11 --> 5
>end
>subgraph G2
>direction LR
>A((0)) --- B((1))
>A((0)) --- E((4))
>B((1)) --- C((2))
>B((1)) --- E((4))
>B((1)) --- H((7))
>C((2)) --- F((5))
>D((3)) --- G((6))
>D((3)) --- I((8))
>F((5)) --- H((7))
>F((5)) --- I((8))
>G((6)) --- I((8))
>H((7)) --- I((8))
>end
>```

> [!pdf] PDF
> [[cours_sem_30_graphes.pdf]]

```pdf
{
	"url" : "Cours/Source/cours_sem_30_graphes.pdf",
	"scale" : 2.0,
	"range" : [1,3]
}
```

## IV - Graphes pondérés

> [!pdf] PDF
> [[cours_sem_30_graphes.pdf]]

```pdf
{
	"url" : "Cours/Source/cours_sem_30_graphes.pdf",
	"scale" : 2.0,
	"page" : 4
}
```

> [!pdf] PDF
> [[cours_sem_30_graphes_fin.pdf]]

```pdf
{
	"url" : "Cours/Source/cours_sem_30_graphes_fin.pdf",
	"scale" : 2.0,
	"page" : 1
}
```

> [!example] Correction de l'algorithme de Floyd-Warshall
> *Invariant* :
> Après la boucle $k$, la matrice $D$ contient le poids du plus petit chemin dont les indices des sommets intermédiaires sont compris entre $0$ et $k$ (inclus).
> 
> *Initialisation* :
> D'après le code, $D \longleftarrow A$ donc $\forall i,j$,  $\begin{cases} D_{i,j} = +\infty \text{ si pas d'arc de i à j} \\ D_{i,j} = p(i, j) \text{ sinon} \end{cases}$ ce qui correspond bien au chemin sans sommet intermédiaire.
> 
> *Conservation* :
> Supposons ok pour $k$.
> - soit passer par $k+1$ permet de construire un chemin plus court, $D_{i,j} = D_{i, k} + D_{k, j}$
> - sinon on conserve $D_{i,j}$
> $\Rightarrow$ la propriété est conservée pour $k+1$
>
> *Conclusion* :
> Après la dernière itération, on a traité $k = n-1$ donc la matrice $D$ contient le poids du plus petit chemin dont les indices des sommets intermédiaires sont compris entre $0$ et $n-1$.
