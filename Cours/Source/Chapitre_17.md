# Chapitre 17 - Fichiers et Entrées-Sorties

```toc
```

## I - Rappels

> [!warning] Voir feuille

## II - Système de fichiers

> [!note] Définition
> **Système de fichiers :** structure de fichiers contenus sur un média
> **Système de gestion de fichiers :** logiciel interne du SE qui gère le SF

Monde Unix et norme POSIX sont associés.

> [!note]
> Stockage d'information :
> - média de stockage (SSD, HDD etc...)
> - mécanisme de nommage / implantation
> 
> Il y a une distinction entre les données dans la mémoire vide de l'ordinateur et les données sur un disque physique.

> [!tldr] Ordres de grandeur
> Temps d'accès aux donées :
> - de $70$ à $300 \micro$s pour un SSD
> - quelques ms pour un disque traditionnel

### A - Structure du contenu d'un fichier

> [!note] Définition
> Un fichier contient une suite d'octets découpés en blocs numérotés.

> [!tip] Fichiers de texte
> Les octets contiennent les codes des caractères.
> Structuration en ligne : suite de caractères + caractère fin

> [!info] Sortes de blocs
> - des blocs de contenus dispersés ou non sur le disque
> - des blocs décrivant l'implantation des blocs de contenu
> - des blocs permettant d'associer des informations à ces octets

### B - Nommage de fichiers

> [!tip] Dans le monde Unix
> - donner un numéro aux fichiers, le numéro d'**inode**
> - maintenir une table de correspondance <nom, inode>
> - créer une arborescence

> [!note] Définition
> **Inode :** contient les caractéristiques d'un fichier :
> - numéro d'inode
> - taille du fichier
> - identifiant du périphérique de stockage
> - date de dernière modification
> - identifiants et groupe propriétaire
> - droits accordés
> - nombre de liens physiques

> [!tip]
> Pour obtenir l'inode d'un fichier sur Unix :
> ```bash
> stat nom_fichier
> ls -i nom_fichier
>```

### C - Liens

> [!note] Définitions
> Un **lien** est une association <nom, inode>
> Un **répertoire** est un fichier qui contient une liste de liens

> [!tip] Créer un lien 
> ```bash
> # création d'un lien symobolique
>ln -s fichier lien_vers_fichier 
>
># création d'un lien physique
>ln fichier lien_vers_fichier
>```
>
>L'extension du lien doit être la même que celle du fichier.
>Un lien physique possède la même inode que le fichier de base.

> [!tip] Supprimer un lien
> Suppression du lien physique :
> ```bash
>rm lien_vers_fichier 
>```
>$\Rightarrow$ diminution du compteur de lien de l'inode.
>Si le compteur arrive à $0$, la mémoire peut être libérée.

![[Recording 20230613090132.webm]]

## III - Entrées / Sorties

### A - Accès séquentiel

> [!info]
> L'**accès séquentiel** est l'accès le plus courant. Tout accès suppose l'**ouverture** du fichier, et donc ensuite la **fermeture** du fichier.

> [!info]
> En mémoire centrale : table des fichiers ouverts.
> Par processus : table des fichiers ouverts.

### B - Flots d'entrée / sortie

> [!info]
> Tout est considéré comme un pseudo fichier. Les **flots** (stream) préouverts par défaut :
> - entrée standard
> - sortie standard
> - sortie standard d'erreur

### C - API C et OCaml

> [!tldr] En C :

| action    | fonction |
| --------- | -------- |
| ouverture | `fopen`  |
| fermeture | `fclose` |
| lecture   | `fscanf` |
| écriture  | `printf`         |

> [!tldr] En OCaml :

| action                | fonction     |
| --------------------- | ------------ |
| ouverture en lecture  | `open_in`    |
| fermeture en lecture  | `close_in`   |
| ouverture en écriture | `open_out`   |
| fermeture en écriture | `close_out`  |
| lecture               | `input_line` |
| écriture              | `output_string`             |

> [!example] Exemple
> ```c
> int main(int argc, char** argv)
> {
> 	assert (argc == 2);
> 	errno = 0;
> 	FILE* fd = fopen(argv[1], "r");
> 	if (fd == NULL)
> 	{
> 		printf("Errno %s : %d (%s)\n", argv[1], errno, strerror(errno));
> 		return 1;
> 	}
> 	fclose(fd);
> 	return 0;
> }
> ```


