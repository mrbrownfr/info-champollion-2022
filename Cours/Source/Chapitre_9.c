#include <stdio.h>
#include <stdlib.h>

struct node
{
    int valeur;
    struct node_t *succ;
};
typedef struct node node_t;

int nb_positifs(node_t *t)
{
    int n = 0;
    node_t *temp = t;
    while (temp != NULL)
    {
        if (temp->valeur >= 0)
        {
            n++;
        }
        temp = temp->succ;
    }
    return n;
}

node_t *creer_liste_queue(int n)
{
    node_t *head = malloc(sizeof(node_t));
    head->valeur = 1;
    head->succ = NULL;
    node_t *queue = head;
    for (int i = 2; i <= n; i++)
    {
        node_t *temp = malloc(sizeof(node_t));
        temp->valeur = i;
        temp->succ = NULL;

        queue->succ = temp;
        queue = queue->succ;
    }
    return head;
}

node_t *creer_liste_tete(int n)
{
    node_t *head = NULL;
    while (n != 0)
    {
        node_t *temp = malloc(sizeof(node_t));
        temp->valeur = n;
        temp->succ = head;

        head = temp;
        n--;
    }
    return head;
}

int est_croissante(node_t *head)
{
    node_t *queue = head;
    while (queue->succ != NULL)
    {
        node_t *prochain = queue->succ;
        if (prochain->valeur < queue->valeur)
        {
            return 0;
        }
        queue = queue->succ;
    }
    return 1;
}

void afficher_liste(node_t *head)
{
    node_t *temp = head;
    while (temp != NULL)
    {
        printf("%d - ", temp->valeur);
        temp = temp->succ;
    }
}

void liberer_liste(node_t *head)
{
    node_t *temp = head->succ;
    while (temp != NULL)
    {
        free(head);
        head = temp;
        temp = temp->succ;
    }
    free(head);
}

int main(void)
{
    node_t n1, n2, n3;
    node_t *head;

    n1.valeur = 1;
    n2.valeur = -1;
    n3.valeur = 2;

    n1.succ = &n2;
    n2.succ = &n3;
    n3.succ = NULL;

    head = &n1;

    afficher_liste(head);
    printf("\n%d", nb_positifs(head));
    // liberer_liste(t);
    return 0;
}