# Chapitre 18 - Graphes de flots de contrôle

```toc
```
## I - Exemple introductif

```mermaid
flowchart TD
A[int s = 1] --> B[int p = 1] --> C(p >= 1) -- vrai --> D[s = s * x\n p = p - 1] --> C
C -- faux --> E[renvoyer]
```

```c
int mystere(int x)
{
	int s = 1;
	int p = 1;
	while (p >= 1)
	{
		s = s * x;
		p = p - 1;
	}
	return s;
}
```

## II - Converture d'un GFC par des tests

```c
int x = 0;
int i = 0;
while (i < n)
{
	if (i % 2 == 0)
	{
		x = x + 2;
	}
	else
	{
		if (i % 3 == 0)
		{
			x = x - 1;
		}
		else
		{
			x = x + 1;
		}
	}
	i = i + 1;
}
```

```mermaid
flowchart TD
A[int x = 0] --> B[int i = 0] --> C(i < n) -- vrai --> D(i % 2 = 0) -- faux --> E(i % 3 == 0) -- faux --> F[x = x + 1] --> G[i = i + 1] --> C
D -- vrai --> H[x = x + 2]
E -- vrai --> I[x = x - 1]
H --> G
I --> G
C -- faux --> J[fin]
```

```c
int max3(int a, int b, int c)
{
	int tmp;
	if (a > b)
	{
		tmp = a;
	} else
	{
		tmp = b;
	}
	if (tmp < c)
	{
		tmp = c;
	}
	return tmp;
}

int max3(int a, int b, int c)
{
	return (a > b ? a : b) < c ? c : (a > b ? a : b);
}
```

```mermaid
flowchart TD
A(x >= y) -- F --> B(y >= z) -- V --> C[y]
A -- V --> D(x >= z) -- F --> E[z]
B -- F --> E
D -- V --> F[x]
```

