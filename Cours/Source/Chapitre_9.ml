type 'a cellule =
	| Nil
	| Cons of 'a * 'a cellule

let T = Cons(1, Cons(2, Nil))

let nb_positif (c : int cellule) : int =
  let rec aux cell acc = match cell with
    | Nil -> acc
    | Cons (v, succ) -> if v >= 0 then aux succ (acc+1) else aux succ acc
in aux c 0

let creer_liste_tete n =
  let rec aux a c = if a=1 then Cons(a,c) else aux (a-1) (Cons(a,c))
  in aux n Nil ;;

let creer_liste_queue (n : int) : int cellule =
  let rec aux count = if count = n then Cons(n, Nil) else Cons(count, aux (count + 1))
in aux 1

let rec est_croissante (c : int cellule) : bool = match c with
  | Nil -> true
  | Cons (v1, succ) -> match succ with
    | Nil -> true
    | Cons (v2, _) -> (v1 <= v2) && est_croissante succ