# Chapitre 16 - Logique

```toc
```

## I - Définitions

### A - Formules en tant qu'arbre

> [!note] Définition
> Une **formule de logique propositionnelle** est un arbre ayant des noeuds binaires, étiquetés par $\land$, $\lor$, $\Rightarrow$, $\Leftrightarrow$ ou des noeuds unitaires étiquetés par $\lnot$ dont les feuilles sont étiquetées par des éléments de $\mathcal{V}$, l'ensemble des variables propositionnelles.

> [!note] Définition
> Pour une formule propositionnelle $P$ :
> - la **hauteur** de $P$ est la hauteur de l'arbre correspondant
> - la **taille** de $P$ est le nombre des noeuds (internes ou non) de l'arbre
> - une **sous-formule** de $P$ est sous-arbre de $P$, soit un arbre enraciné en l'un des noeuds de $P$

### B - Définition inductive

> [!note] Définition
> L'ensemble $F$ des formules de la logique propositionnelle est défini inductivement :
> - toute variable propositionnelle $p$ est une formule
> - si $\varphi$ est une formule alors $\lnot\varphi$ est une formule
> - si $\varphi, \psi$ sont des formules, alors $(\varphi \lor \psi), (\varphi \land \psi), (\varphi \Rightarrow \psi)$ et $(\varphi \Leftrightarrow \psi)$ sont des formules

> [!note] Définition
> L'ensemble $SF(\varphi)$ des **sous-formules** d'une formule $\varphi$ est défini inductivement :
> - $SF(p)= \lbrace p \rbrace$
> - $SF(\lnot \varphi) = \lbrace \lnot \varphi \rbrace \cup SF(\varphi)$
> - $SF(\varphi \lor \psi) = \lbrace \varphi \lor \psi \rbrace \cup SF(\varphi) \cup SF(\psi)$
> - $SF(\varphi \land \psi) = \lbrace \varphi \land \psi \rbrace \cup SF(\varphi) \cup SF(\psi)$
> - $SF(\varphi \Rightarrow \psi) = \lbrace \varphi \Rightarrow \psi \rbrace \cup SF(\varphi) \cup SF(\psi)$

> [!example] Exemple
> $SF(p \Rightarrow (q \lor r)) = \lbrace p \Rightarrow (q \lor r),\ p,\ q \land r,\ q,\ r \rbrace$

## II - Sémantique du calcul propositionnel

### A - Notion de valuation

> [!note] Définition
> Soit un ensemble à deux éléments $\mathbb{B} = \lbrace V, F \rbrace$. Une **valuation** est une application $v : \mathcal{V} \longrightarrow \mathbb{B}$

> [!example] Exercice
> Ecrire la table de vérité de la formule $\varphi = (p \land q) \lor \lnot p$. Evaluer la formule avec la valuation $v(p) = F, v(q) = F$

| $p$   | $q$   | $p \land q$ | $\lnot p$ | $\varphi$   |
| --- | --- | --------- | ------- | --- |
| V   | V   | V         | F       | F   |
| V   | F   | F         | F       | F   |
| F   | F   | F         | V       | V   |
| F   | V   | F         | V       | V    |

> [!note] Définition
> Si une valuation $v$ **satisfait** une formule $\varphi$ on note $v \models \varphi$
> Si $v \models \varphi$ alors on dit que $v$ est un **modèle** de $\varphi$.
> $Mod(\varphi) = \lbrace v\ |\ v \models \varphi \rbrace$ est l'ensemble des modèles de $\varphi$.

> [!example] Example
> $\varphi = (p \land q) \lor \lnot p$, $Mod(\varphi) = \lbrace (p \mapsto V, q \mapsto V),\ (p \mapsto F, q \mapsto V),\ (p \mapsto F, q \mapsto F) \rbrace$

### B - Satisfabilité

> [!note] Définition
> Une formule est :
> - **satisfiable** si elle admet un modèle $(Mod(\varphi) \neq \varnothing)$
> - **tautologique** si toute valuation est un modèle
> - **antilogique** si aucune valuation n'est pas un modèle $(Mod(\varphi) = \varnothing)$
> Une tautologie est représentée par $\top$
> Une antilogie est représentée par $\bot$

### C - Equivalence entre deux formules

> [!note] Définition
> Les formules $\varphi$ et $\psi$ sont **équivalentes** lorsque $Mod(\varphi) = Mod(\psi)$. On le note $\varphi \equiv \psi$

> [!note] Définition
> Si $\varphi$ et $\psi$ sont deux formules propositionnelles :
> $\psi$ est une **conséquence logique** de $\varphi$ si $Mod(\varphi) \subseteq Mod(\psi)$, autrement dit si toute valuation satisfaisant $\varphi$ satisfait également $\psi$. On le note $\varphi \models \psi$

> [!tip] Equivalence
> $\varphi \equiv \psi \Leftrightarrow \varphi \models \psi$ et $\psi \models \varphi$
> $\varphi \equiv \psi \Leftrightarrow\ \models (\varphi \Leftrightarrow \psi)$

> [!tip] Transitivité
> Si $P \models Q$ et $Q \models R$ alors $P \models R$

> [!tip] Elements neutres
> $\varphi \land \top \equiv \varphi$
> $\varphi \lor \bot \equiv \varphi$

> [!warning] Voir photos pour recopier cours

### D - Substitution

> [!note] Définition
> Soient deux formules $\varphi, \psi$ et une variable propositionnelle $p \in \mathcal{V}$. La **substitution**, notée $\varphi\lbrace p \mapsto \psi \rbrace$ (ou $\varphi[\psi \backslash p]$) est définie inductivement par :
> - Si $\varphi = p$, alors $\varphi\lbrace p \mapsto \psi \rbrace = \psi$
> - Si $\varphi = q$ et $q \neq p$, alors $\varphi\lbrace p \mapsto \psi \rbrace = \varphi$
> - Si $\varphi = \lnot \varphi_1$, alors $\varphi\lbrace p \mapsto \psi \rbrace = \lnot (\varphi_1\lbrace p \mapsto \psi \rbrace)$
> - Si $\varphi = \varphi_{1}\land \varphi_{2}$, alors $\varphi\lbrace p \mapsto \psi \rbrace = \varphi_1\lbrace p \mapsto \psi \rbrace \land \varphi_2\lbrace p \mapsto \psi \rbrace$
> - Si $\varphi = \varphi_{1}\lor \varphi_{2}$, alors $\varphi\lbrace p \mapsto \psi \rbrace = \varphi_1\lbrace p \mapsto \psi \rbrace \lor \varphi_2\lbrace p \mapsto \psi \rbrace$
> - Si $\varphi = \varphi_{1}\Rightarrow \varphi_{2}$, alors $\varphi\lbrace p \mapsto \psi \rbrace = \varphi_1\lbrace p \mapsto \psi \rbrace \Rightarrow \varphi_2\lbrace p \mapsto \psi \rbrace$

## III - Formes normales

### A - FNC et FND

#### 1) Forme Normale Conjonctive

> [!note] Définition
> Un **litteral** est une formule de la forme $p$ ou $\lnot p$ avec $p \in \mathcal{V}$.
> Une **clause** est une formule de la forme $\varphi_{1}\lor \cdots \lor \varphi_{m}$ où les $\varphi_{i}$ sont des littéraux.
> Une **forme normale conjonctive** est une formule de la forme $C_{1}\land \cdots \land C_{n}$ où les $C_{i}$ sont des clauses.

#### 2) Forme Normale Disjonctive

> [!note] Définition
> C'est une disjonction de clauses conjonctives.

### B - Mise sous forme normale disjonctive

> [!example] Exemple
> Déterminer la forme normale disjonctive de la formule $\varphi = (p \land q) \lor \lnot p$
> $\varphi = (p \land q) \lor (\lnot p \land \lnot q) \lor (\lnot q \land q)$ (obtenu avec la table de vérité de la formule)

### C - Mise sous forme normale conjonctive

> [!tldr] Méthode
> En partant d'une formule quelconque :
> - transformer les implications
> - faire "descendre" les négations
> - faire "remonter" les connecteurs $\land$ : remplacer $P \lor (Q_{1}\land Q_{2})$ par $(P \lor Q_{1})\land (P \lor Q_{2})$

> [!example] Exemple
> Déterminer la forme normale conjonctive de la formule $\varphi = (p \land q) \lor (z \land r)$
> $\varphi \equiv (p \lor z) \land (q \lor z) \land (p \lor r) \land (q \lor r)$

## IV - Problème SAT

```pdf
{
	"url" : "Cours/Source/cours_sem_33_logique_fin.pdf",
	"scale" : 2.0,
	"page" : 1
}
```

> [!pdf] PDF
>![[cours_sem_33_logique_fin.pdf]]

### D - Application : coloration de graphe

> [!example] Exercice
> chaque sommet possède au moins une couleur $\iff \displaystyle\bigwedge_{i=1}^{n}\displaystyle\bigvee_{j=0}^{k-1}x_{ij}$
> chaque sommet possède au plus une couleur $\iff$
> deux sommets adjacents n'ont pas la même couleur $\iff \displaystyle\bigwedge_{i=1}^{n-1}\displaystyle\bigwedge_{j=i+1}^{n}\displaystyle\bigvee_{q=0}^{k-1}\left((i, j) \in A \Rightarrow x_{iq} \land \lnot x_{jq}\right)$


## V - Logique du premier ordre

> [!example] Exemple 1
> Arbre associé à la formule $\exists x P(x) \lor \exists x (P(x) \Rightarrow \forall y R(x, y))$
> ```mermaid
>flowchart TD
>A[OR] --> B[EXISTS x] --> C["P(x)"]
>A --> D[EXISTS x] --> E[->]
>E --> F["P(x)"]
>E --> G[FORALL y] --> H["R(x, y)"]
>```

