#!/usr/bin/env sh

rm ./tp06_motatrouver && gcc -std=c99 -Wall -Wextra -fsanitize=address,undefined -o tp06_motatrouver tp06_motatrouver.c && ./tp06_motatrouver
