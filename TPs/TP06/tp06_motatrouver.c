#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>

/// @brief Permet de choisir une chaîne de caractères aléatoirement dans un tableau
/// @param liste_de_mots le tableau dans lequel chercher la chaîne de caractères
/// @param lng la longueur du tableau
/// @return un pointeur vers une chaine de caractères choisie dans le tableau
char *choisir_un_mot(char liste_de_mots[][256], int lng)
{
    return liste_de_mots[rand() % lng];
}

/// @brief Retourne la longueur d'une chaîne de caractères
/// @param chaine_a_verifier la chaîne que l'on veut compter
/// @param lng_max la longueur maximum de la chaîne
/// @return la longueur de la chaîne, la longueur maximale sinon
int longueur(char *chaine_a_verifier, int lng_max)
{
    for (int i = 0; i < lng_max; i++)
    {
        if (chaine_a_verifier[i] == '\0')
        {
            return i;
        };
    };
    return lng_max;
}

/// @brief Initialise un mot en le remplissant de '-'
/// @param mot un pointeur vers le mot à initialiser
/// @param lng la longueur du mot
void initialise_mot(char *mot, int lng)
{
    for (int i = 0; i < lng; i++)
    {
        mot[i] = '-';
    }
}

/// @brief Actualise le mot en cours de recherche et l'affiche de façon jolie
/// @param mot le mot en cours de recherche
/// @param lng_mot la longueur des mots
/// @param lettre la lettre entrée par l'utilisateur
/// @param mot_a_deviner le mot à deviner
/// @return le nombre d'occurences de la lettre dans le mot
int mise_a_jour(char *mot, int lng_mot, char lettre, char *mot_a_deviner)
{
    int nb_occurences = 0;
    for (int i = 0; i < lng_mot; i++)
    {
        if (mot_a_deviner[i] == lettre)
        {
            mot[i] = lettre;
            nb_occurences++;
        }
    };
    return nb_occurences;
}

void pretty_print(char *mot_en_cours, char lettre, int nb_occurences)
{
    printf("La lettre %c ", lettre);
    printf("a été trouvé %d fois dans le mot!\n", nb_occurences);
    printf("ETAT DU MOT: %s\n\n", mot_en_cours);
}

int sont_egaux(char *mot1, char *mot2, int lng)
{
    for (int i = 0; i < lng; i++)
    {
        if (mot1[i] != mot2[i])
        {
            return 0;
        }
    };
    return 1;
}

void jeu(char liste_mots[][256], int nb_mots, char *mot_courant)
{
    char *mot_a_deviner = choisir_un_mot(liste_mots, nb_mots);
    int longueur_mot = longueur(mot_a_deviner, 256);
    int essais = 0;
    printf("La longueur du mot à deviner est %d. Bonne chance !\n",
           longueur_mot);
    initialise_mot(mot_courant, longueur_mot);

    // Consigne 12
    while (sont_egaux(mot_a_deviner, mot_courant, longueur_mot) == 0)
    {
        char lettre = ' ';
        printf("Entrez une lettre à deviner !\n");
        scanf("%c", &lettre);
        int nb_occurences = mise_a_jour(mot_courant, longueur_mot, lettre, mot_a_deviner);
        pretty_print(mot_courant, lettre, nb_occurences);
        essais++;
    };
    printf("Bien joué! Vous avez deviné %s en %d essais!\n", mot_a_deviner, essais);
}

/* FONCTION PRINCIPALE */
int main()
{
    srand(time(NULL)); // Définition d'une nouvelle seed

    /* DEFINITIONS DE VARIABLES UTILES */
    char liste_mots[/* taille 3 */][256] = {"bonjour", "enveloppe", "neige"};
    const int nb_mots = 3;
    char mot_courant[256];

    // Question 1: ces 3 lignes affichent bonjour, enveloppe, et b
    printf("%s\n", liste_mots[0]);
    printf("%s\n", liste_mots[1]);
    printf("%c\n", liste_mots[0][0]);

    // Test consigne 5
    printf("%s\n", choisir_un_mot(liste_mots, nb_mots));

    // Test consigne 6
    printf("%d\n", longueur("Utilisez Rust c'est mieux", 100));
    printf("%d\n", longueur("Fedora <3", 5));

    printf("\n");

    jeu(liste_mots, nb_mots, mot_courant);

    return 0
}
