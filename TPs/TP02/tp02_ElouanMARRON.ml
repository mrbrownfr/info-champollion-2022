(** Question 2: renvoie une liste contenant n fois un élément x
    @param n nombre de fois à répéter l'élément
    @param x l'élément à répéter
    @return liste contenant n fois l'élément x*)
let rec repete_elt n x = if n > 0 then x :: repete_elt (n - 1) x else []
;;

assert (repete_elt 5 'a' = [ 'a'; 'a'; 'a'; 'a'; 'a' ]);
Printf.printf "OK\n"

(** Question 3: prend 2 listes et les ajoute bout à bout en conservant l'ordre original des éléments
    @param liste1 première liste (celle qui restera au début)
    @param liste2 deuxième liste (celle qui ira à la fin)
    @return liste contenant les éléments des 2 listes dans l'ordre *)
let rec concat liste1 liste2 =
  match liste1 with [] -> liste2 | head :: tail -> head :: concat tail liste2
;;

assert (
  concat [ 'a'; 'b'; 'c' ] [ 'd'; 'e'; 'f' ] = [ 'a'; 'b'; 'c'; 'd'; 'e'; 'f' ]);
Printf.printf "OK\n"

(** Question 4: prend une liste et répète n fois ses éléments
    @param n le nombre de fois à répéter les éléments
    @param l la liste à laquelle appliquer la fonction
    @return liste contenant les éléments répétés n fois
    @raise Failure si n est négatif *)
let rec repete_liste n l =
  if n >= 0 then
    match n with 0 | 1 -> l | _ -> repete_liste (n - 1) (concat l l)
  else failwith "n doit être positif !"
;;

assert (repete_liste 2 [ 'a'; 'b' ] = [ 'a'; 'b'; 'a'; 'b' ]);
Printf.printf "OK\n"

(** Question 5: double les éléments d'une liste
    @param liste la liste à laquelle appliquer la fonction
    @return liste contenant les éléments doublés *)
let rec double liste =
  match liste with
  | [] -> []
  | head :: tail -> concat [ head; head ] (double tail)
;;

assert (double [ 0; 2; 4 ] = [ 0; 0; 2; 2; 4; 4 ]);
Printf.printf "OK\n"

(** Question 6: répète plusieurs fois chaque élément d'une liste
    @param n le nombre de fois par lequel répéter les éléments
    @param liste la liste à laquelle appliquer la fonction
    @return liste contenant ses éléments répétés n fois
    @raise Failure si n est négatif *)
let rec repete_par_elt n liste =
  if n < 0 then failwith "n doit être positif!"
  else
    match liste with
    | [] -> []
    | head :: tail -> concat (repete_elt n head) (repete_par_elt n tail)
;;

assert (repete_par_elt 3 [ 0; 2; 4 ] = [ 0; 0; 0; 2; 2; 2; 4; 4; 4 ]);
Printf.printf "OK\n"

(** Question 7: élimine les répétitions successives d'éléments
    @param l la liste à laquelle éliminer les répétitions
    @return liste contenant aucune répétition successive *)
let rec elimine_repetitions_successives l =
  match l with
  | [] -> []
  | x :: [] -> [ x ]
  | head :: tail ->
      if head = List.hd tail then elimine_repetitions_successives tail
      else concat [ head ] (elimine_repetitions_successives tail)
;;

assert (
  elimine_repetitions_successives [ 0; 0; 2; 2; 4; 4; 0; 0 ] = [ 0; 2; 4; 0 ]);
assert (
  [ 0; 1; 2; 3 ]
  = elimine_repetitions_successives (repete_par_elt 3 [ 0; 1; 2; 3 ]));
Printf.printf "OK\n"

(* Le nombre de vérifications effectuées est de 2n pour une liste de taille n *)

(** Question 8: indique si une liste contient au moins 1 élément qui se répète de façon successive
    @param liste la liste à vérifier
    @return true si répétition, false sinon *)
let rec avec_repetitions_successives liste =
  match liste with
  | [] -> false
  | x :: [] -> false
  | head :: tail ->
      if head = List.hd tail then true else avec_repetitions_successives tail
;;

assert (avec_repetitions_successives [ 0; 1; 2; 3; 3; 4 ]);
assert (not (avec_repetitions_successives [ 0; 1; 2; 3; 4 ]));
Printf.printf "OK\n"

(** Question 9: vérifie pour des doublons dans une liste
    @param liste la liste à vérifier
    @return true si aucun doublon, false sinon *)
let rec sans_doublons liste =
  let rec appartient element liste =
    match liste with
    | [] -> false
    | head :: tail -> if element = head then true else appartient element tail
  in
  match liste with
  | [] -> true
  | x :: [] -> true
  | head :: tail -> if appartient head tail then false else sans_doublons tail
;;

assert (not (sans_doublons [ 0; 2; 4; 0 ]));
Printf.printf "OK\n"

(** Question 10: élimine les doublons dans une liste
    @param liste la liste à trier
    @return liste triée, sans doublons *)
let rec elimine_doublons liste =
  if sans_doublons liste then liste
  else
    let rec appartient element liste =
      match liste with
      | [] -> false
      | head :: tail -> if element = head then true else appartient element tail
    in
    if appartient (List.hd liste) (List.tl liste) then
      elimine_doublons (List.tl liste)
    else concat [ List.hd liste ] (elimine_doublons (List.tl liste))
;;

assert (elimine_doublons [ 0; 2; 4; 0 ] = [ 2; 4; 0 ]);
Printf.printf "OK\n"

(* Le nombre de comparaisons effectuées est n! pour une liste de taille n *)

(** Question 11: crée une liste contenant tous les entiers de i à j-1
    @param i entier de départ (inclus)
    @param j entier d'arrivée (exclus)
    @return liste contenant tous les entiers entre i et j-1 *)
let rec range i j = if i >= j then [] else concat [ i ] (range (i + 1) j)
;;

assert (range 1 8 = [ 1; 2; 3; 4; 5; 6; 7 ]);
Printf.printf "OK\n"

(** Question 12: renvoie le dernier élément d'une liste
    @param liste la liste à vérifier
    @return le dernier élément de la liste
    @raise Failure si la liste est vide *)
let rec dernier liste =
  match liste with
  | [] -> failwith "Aucun élément dans la liste !"
  | x :: [] -> x
  | head :: tail -> dernier tail
;;

assert (dernier [ 1; 2; 3; 4; 5 ] = 5);
Printf.printf "OK\n"

(** Question 13: effectue un produit cartésien de 2 listes
    @param liste1 le premier "ensemble" d'éléments
    @param liste2 le deuxième "ensemble" d'élements
    @return liste contenant des couples consistant en le produit cartésien
    @raise Failure si une des 2 listes est vide ou si les listes ne sont pas de même taille *)
let rec produit liste1 liste2 =
  let rec aux u v =
    match u with
    | [] -> failwith "Erreur !"
    | x :: [] ->
        if v <> [] then (x, List.hd v) :: aux [ x ] (List.tl v)
        else produit (List.tl liste1) liste2
    | head :: tail -> (
        match v with
        | [] -> failwith "La liste 2 est vide !"
        | x :: [] -> failwith "Pas assez d'éléments"
        | h :: t -> (head, List.hd v) :: aux [ head ] (List.tl v))
  in
  if liste1 = [] || liste2 = [] then [] else aux liste1 liste2
;;

assert (
  produit [ 1; 2; 3 ] [ 'a'; 'b'; 'c' ]
  = [
      (1, 'a');
      (1, 'b');
      (1, 'c');
      (2, 'a');
      (2, 'b');
      (2, 'c');
      (3, 'a');
      (3, 'b');
      (3, 'c');
    ]);
Printf.printf "OK\n"

(** Question 14: trouve un élément dans une liste répondant à une condition
    @param predicat fonction booléenne chargé de vérifier la condition
    @param liste la liste à vérifier
    @return le premier élément répondant au prédicat
    @raise Not_found si aucun élément n'est trouvé *)
let rec find predicat liste =
  match liste with
  | [] -> raise Not_found
  | head :: tail -> if predicat head then head else find predicat tail
;;

assert (find (fun x -> x mod 2 = 0) [ 0; 1; 3; 2; 4 ] = 0);
Printf.printf "OK\n"

(* Le type de la fonction est ('a -> bool) -> 'a list -> 'a, donné par le LSP de OCaml *)

(** Question 15: réplique la fonction map de la librairie standard
    @param fonction la fonction à appliquer aux éléments de la liste
    @param liste la liste à laquelle appliquer la fonction
    @return la fonction "mappée" *)
let rec map fonction liste =
  match liste with
  | [] -> []
  | head :: tail -> fonction head :: map fonction tail
;;

assert (map (fun x -> x ** 3.) [ 1.; 2.; 3.; 4. ] = [ 1.; 8.; 27.; 64. ]);
Printf.printf "OK\n"

(** Question 16: additionne tous les entiers d'une liste à l'aide de fold
    @param fonction la fonction somme
    @param liste la liste à additionner
    @return la liste additionnée *)
let somme_liste_entiers_v1 fonction liste = List.fold_left fonction 0 liste

let somme_liste_entiers_v2 fonction liste = List.fold_right fonction liste 0;;

assert (
  somme_liste_entiers_v1 (fun x y -> x + y) [ 1; 2; 3; 4; 5 ]
  = somme_liste_entiers_v2 (fun x y -> x + y) [ 1; 2; 3; 4; 5 ]);
assert (somme_liste_entiers_v2 (fun x y -> x + y) [ 1; 2; 3; 4; 5 ] = 15);
Printf.printf "OK\n"

(* Le type de ces fonctions est respectivement (int -> 'a -> int) -> 'a list -> int et ('a -> int -> int) -> 'a list -> int *)

(** Question 17: compte le nombre d'éléments dans la liste
    @param fonction_comptage la fonction de comptage
    @param liste la liste à compter
    @return le nombre d'éléments dans la liste *)
let compter_elements_v1 fonction_comptage liste =
  List.fold_left fonction_comptage 0 liste
;;

assert (compter_elements_v1 (fun x y -> 1 + x) [ 'a'; 'b'; 'c'; 'd' ] = 4);
Printf.printf "OK\n"

let compter_elements_v2 fonction_comptage liste =
  List.fold_right fonction_comptage liste 0
;;

assert (compter_elements_v2 (fun x y -> 1 + y) [ 'a'; 'b'; 'c'; 'd' ] = 4);
Printf.printf "OK\n"

(** Question 18: réplique fold_left dans la librairie standard *)
let rec fold_left fonction init liste =
  match liste with
  | [] -> failwith "Liste vide !"
  | x :: [] -> fonction init x
  | head :: tail -> fonction (fold_left fonction init tail) head
;;

assert (
  fold_left (fun x y -> x * y) 1 [ 1; 2; 3; 4 ]
  = List.fold_left (fun x y -> x * y) 1 [ 1; 2; 3; 4 ]);
Printf.printf "OK\n"

(** Question 19: réplique fold_right dans la librairie standard *)
let rec fold_right fonction liste init =
  match liste with
  | [] -> failwith "Liste vide !"
  | x :: [] -> fonction x init
  | head :: tail -> fonction head (fold_right fonction tail init)
;;

assert (
  fold_right (fun x y -> x * y) [ 1; 2; 3; 4 ] 1
  = List.fold_right (fun x y -> x * y) [ 1; 2; 3; 4 ] 1);
Printf.printf "OK\n"

(** Question 20: concatène des listes à l'aide de fold_right
    @param liste1 la liste du "début"
    @param liste2 la liste de "fin"
    @return la liste concaténée *)
let concatv2 liste1 liste2 =
  List.fold_right (fun last liste2 -> last :: liste2) liste1 liste2
;;

assert (concatv2 [ 1; 2; 3 ] [ 4; 5; 6 ] = concat [ 1; 2; 3 ] [ 4; 5; 6 ]);
Printf.printf "OK\n"

(** Question 21: retourne une liste à l'aide de fold_left
    @param liste la liste à retiurner
    @return la liste retournée *)
let retournerv2 liste = List.fold_left (fun x liste -> liste :: x) [] liste
;;

assert (retournerv2 [ 1; 2; 3; 4 ] = [ 4; 3; 2; 1 ]);
Printf.printf "OK\n"

(** Question 22: réplique map dans la librairie standard *)
let rec map fonction liste =
  if liste <> [] then fonction (List.hd liste) :: map fonction (List.tl liste)
  else []
;;

assert (
  map (fun x -> x * x) [ 1; 2; 3; 4 ] = List.map (fun x -> x * x) [ 1; 2; 3; 4 ]);
Printf.printf "OK\n"

(** Question 23: map une fonction aux éléments de 2 listes
    @param liste1 la première liste à qui "mapper" les éléments
    @param liste2 la deuxième liste à qui "mapper" les éléments
    @return la liste des éléments mappés (même longueur que les listes orginales) *)
let rec map2 fonction liste1 liste2 =
  if liste1 <> [] || liste2 <> [] then
    fonction (List.hd liste1) (List.hd liste2)
    :: map2 fonction (List.tl liste1) (List.tl liste2)
  else []
;;

assert (
  map2 (fun x y -> x * y) [ 1; 2; 3; 4 ] [ 5; 6; 7; 8 ] = [ 5; 12; 21; 32 ]);
Printf.printf "OK\n"

(** Question 24: permet de créer une liste de couple à partir de 2 listes.
    @param liste1 liste contenant les premiers éléments des couples
    @param liste2 liste contenant les deuxièmes éléments des couples
    @return liste contenant des couples *)
let accouple liste1 liste2 = map2 (fun x y -> (x, y)) liste1 liste2
;;

assert (
  accouple [ 1; 2; 3; 4 ] [ 'a'; 'b'; 'c'; 'd' ]
  = [ (1, 'a'); (2, 'b'); (3, 'c'); (4, 'd') ]);
Printf.printf "OK\n"
