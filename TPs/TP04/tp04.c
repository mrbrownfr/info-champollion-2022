#include <stdio.h>

/* gcc -std=c99 -Wall -Wextra -Wvla -o tp04 tp04.c */

// Question 2: L'expresion 1 renverra VRAI et l'expression 2 FAUX.
// Question 3: le code renvoie "x vaut 24".
// Question 4: le code renvoie "la valeur de t est 1".
// Idem pour la question 5.

void q8(int n)
{
    int somme = 0;
    for (int i = 0; i <= n; i++)
    {
        somme += i;
    };
    printf("La somme des entiers jusqu'à %d est %d \n", n, somme);
}

void q9(int n)
{
    int somme = 0;
    int _i = n;
    while (_i > 0)
    {
        somme += _i;
        _i -= 1;
    };
    printf("La somme des entiers jusqu'à %d est %d \n", n, somme);
}

// Question 10:
int incrementation_interne(int n)
{
    n++;
    return n;
}

// Question 11:
int valeur_absolue(int entier)
{
    int entier_abs = entier;
    if (entier < 0)
    {
        entier_abs = -entier;
    };
    return entier_abs;
}

// Question 12:
int somme_rec(int n)
{
    if (n == 0)
    {
        return n;
    };
    return n + somme_rec(n - 1);
}

// Exercice 1:
int pair_proche(int n)
{
    if ((n % 2) == 0)
    {
        return n;
    }
    else
    {
        return (n + 1);
    }
}

// Exercice 2:
int min(int nb1, int nb2)
{
    if (nb1 > nb2)
    {
        return nb2;
    };
    return nb1;
}
int max(int nb1, int nb2)
{
    if (nb1 > nb2)
    {
        return nb1;
    };
    return nb2;
}
// Exercice 3:
int perimetre(int L, int l)
{
    return L * l;
}

// Exercice 4:
int est_pythagore(int c1, int c2, int c3)
{
    return (((c1 * c1) + (c2 * c2)) == (c3 * c3));
}

// Exercice 5:
int puissance_it(int a, int n)
{
    int resultat = 1;
    if (n <= 0)
    {
    }
    else if ((n % 2) == 0)
    {
        for (int i = 1; i <= n / 2; i++)
        {
            resultat = resultat * (a * a);
        };
    }
    else if ((n % 2) == 1)
    {
        for (int i = 1; i <= (n - 1) / 2; i++)
        {
            resultat = resultat * (a * a);
        };
        resultat *= a;
    }
    return resultat;
}
int puissance_rec(int a, int n)
{
    int resultat = 1;
    if (n <= 0)
    {
    }
    else if ((n % 2) == 0)
    {
        resultat = puissance_rec((a * a), (n / 2));
    }
    else if ((n % 2) == 1)
    {
        resultat = a * puissance_rec((a * a), ((n - 1) / 2));
    }
    return resultat;
}

// Exercice 6:
int factorielle(int n)
{
    if (n <= 0)
    {
        return 1;
    };
    return n * factorielle(n - 1);
}

// Exercice 7:
int coef_binom(int n, int k)
{
    if (k == n || k == 0 || k <= 0 || n <= 0)
    {
        return 1;
    };

    return coef_binom((n - 1), (k - 1)) + coef_binom((n - 1), k);
}
void triangle_pascal(int n)
{
    for (int i = 0; i <= n; i++)
    {
        printf("%d\n", coef_binom(n, i));
    }
};

void figures(int n)
{
    // Carré
    for (int i = 0; i < n; i++)
    {
        for (int i = 0; i < n; i++)
        {
            printf("*");
        };
        printf("\n");
    }

    // Triangle
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            printf("*");
        };
        printf("\n");
    };
    printf("\n");

    // Triangle inversé
    for (int i = n; i >= 0; i--)
    {
        for (int j = i; j > 0; j--)
        {
            printf("*");
        };
        printf("\n");
    }

    // Carré vide
    for (int i = 0; i < n; i++)
    {
        if (i == 0 || i == (n - 1))
        {
            for (int i = 0; i < n; i++)
            {
                printf("*");
            };
            printf("\n");
        }
        else
        {
            printf("*");
            for (int i = 0; i < (n - 2); i++)
            {
                printf(" ");
            };
            printf("*");
            printf("\n");
        }
    };
    printf("\n");

    // Triangle vide
    for (int i = 0; i < n; i++)
    {
        if (i == 0)
        {
            printf("*");
            printf("\n");
        }
        else if (i == (n - 1))
        {
            for (int i = 0; i < n; i++)
            {
                printf("*");
            };
            printf("\n");
        }
        else
        {
            printf("*");
            for (int j = -1; j < (i - 2); j++)
            {
                printf(" ");
            };
            printf("*");
            printf("\n");
        };
    };
    printf("\n");

    // Pyramide
    for (int i = 0; i < (n - 1); i++)
    {
        for (int j = (n / 2); j >= i; j--)
        {
            printf(" ");
        };
        for (int k = 1; k <= (1 + (2 * i)); k++)
        {
            printf("*");
        };
        printf("\n");
    }
}

int main()
{
    int t = 3;
    if (t = 1)
    {
        printf(" la valeur de t est %d \n ", t);
    }
    else
    {
        printf(" t ne vaut pas 1... enfin si maintenant ... %d \n ", t);
    }
    int resultat;
    int x = 2;
    if (x == 2)
    {
        resultat = 0;
    }
    else
    {
        resultat = 2;
    }
    printf("le resultat est : %d \n", resultat);

    q8(4);
    q9(6);

    int m = 3;
    int res = incrementation_interne(m);
    printf("La valeur de m est: %d, la valeur de res est: %d\n", m, res);

    printf("La valeur absolue de -7 est %d\n", valeur_absolue(-7));
    printf("La valeur absolue de 7 est %d\n", valeur_absolue(7));

    printf("La somme des entiers jusqu'à 15 est %d\n", somme_rec(15));

    printf("\nTESTS FONCTIONS PUISSANCES\n==========================\n");
    printf("%d\n", puissance_it(2, 4));
    printf("%d\n", puissance_rec(2, 4));

    printf("\nTESTS COEFS BINOMIAUX ET PASCAL\n===============================\n");
    printf("%d\n", coef_binom(9, 6));

    triangle_pascal(7);

    printf("\nTRANSFORMATIONS FOR EN WHILE\n============================\n");
    /* Boucle de base:
    for (int i = 0; i < n; i = i + 1){
        if (i % 2 == 0){
            printf("%d\n", i);
            }
        } */
    int i = 0;
    int n = 10;
    while (i < n)
    {
        if (i % 2 == 0)
        {
            printf("%d\n", i);
        };
        i++;
    };

    /* Boucle de base:
    for (; i < n; ){
        if (i % 2 == 1){
            printf("%d\n", i);
            i = i + 1;
        } else {
            i = i * 2;
        }
    } */
    while (i < n)
    {
        if (i % 2 == 1)
        {
            printf("%d\n", i);
            i = i + 1;
        }
        else
        {
            i = i * 2;
        }
    };
    /* Boucle de base:
    for (i = 0; n != 0; n = n / 2){
        i = i + 1;
    } */
    while (n != 0)
    {
        n /= 2;
        i++;
    };
    // Si on remplaçait `i = 0` par `int i = 0` alors i serait remis à 0 à chaque itération car il serait dans la boucle.

    printf("\nFIGURES\n=======\n");

    figures(6);

    return 69;
}
