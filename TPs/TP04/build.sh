#!/usr/bin/env sh

gcc -std=c99 -Wall -Wextra -Wvla -fsanitize=address,undefined -o main
$1
