(** PARTIE 1 *)

type morceau = { titre : string; duree : int; auteur : string; annee : int }
(* La durée est en secondes *)

(* Le titre des 2 variables est indiqué *)
let m1 : morceau =
  { titre = "SICKO MODE"; duree = 312; auteur = "Travis Scott"; annee = 2021 }

let m2 : morceau = { titre = "44"; duree = 167; auteur = "Niska"; annee = 2022 }
let catalogue = [ m1; m2 ];;

List.sort
  (fun m1 m2 ->
    match (m1.titre, m2.titre) with
    | ( > ) when true -> 1
    | ( < ) when true -> -1
    | ( = ) -> 0)
    (* Ma fonction anonyme a pour type morceau -> morceau -> int *)
  catalogue

type perturbation = { titre : string };;

List.sort
  (fun (m1 : morceau) (m2 : morceau) ->
    match (m1.titre, m2.titre) with
    | ( > ) when true -> 1
    | ( < ) when true -> -1
    | ( = ) -> 0)
    (* Ma fonction anonyme a pour type morceau -> morceau -> int *)
  catalogue
(* L'appel à la fonction anonyme a un type perturbation -> perturbation -> int *)

type coordonnees = int * int
(* coordonnees est de _type_ int * int *)

let point = (3, 2)

type unecouleur = Bleu

let b = Bleu

type forme = Petales of int

let f = Petales 3

type couleur = Bleu | Rouge | Jaune | Vert | Violet | Orange

let clr : couleur = Jaune

type fleur = couleur * forme

let bouton_d_or = (Jaune, Petales 5)
let c = Rouge
let est_jaune = c = Jaune (* Valeur : false *)

let melange (clr1, clr2) =
  match (clr1, clr2) with
  | Bleu, Rouge | Rouge, Bleu -> Some Violet
  | Violet, Jaune | Jaune, Violet -> Some Rouge
  | Bleu, Jaune | Jaune, Bleu -> Some Vert
  | Jaune, Rouge | Rouge, Jaune -> Some Orange
  | _ -> None

type couleur = Bleu | Rouge | Jaune | Melange of couleur * couleur

let orange = Melange (Jaune, Rouge)
let emeraude = Melange (Melange (Bleu, Jaune), Bleu)
(* Melange (Bleu, Jaune) = Vert *)

let rec mystere c =
  match c with
  | Rouge | Jaune -> false
  | Bleu -> true
  | Melange (c1, c2) -> mystere c1 || mystere c2
(* Cette fonction détermine si une couleur primaire de c est le bleu *)

(** PARTIE SUIVANTE *)

type personne = { nom : string; mutable age : int }

let personne1 = { nom = "Ada"; age = 20 }
(*? Modifier la partie mutable : personne1.age <- personne1.age + 1 *)
