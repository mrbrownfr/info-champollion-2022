(** 4.1 Type somme et révisons sur les listes *)

type t = B | N | R

(* Ecrire une fonction permute qui étant donné une liste d’éléments du type t renvoie une nouvelle liste dans laquelle les valeurs B sont remplacées par N, les valeurs N par R et les valeurs R par B. *)
let permute (liste : t list) =
  List.map (fun (elt : t) -> match elt with B -> N | N -> R | R -> B) liste
;;

assert (permute [ R; B; B; B; N; N; B; R; B ] = [ B; N; N; N; R; R; N; B; N ]);
Printf.printf "OK\n"

(* Ecrire une fonction compte qui prend en paramètre une liste d’éléments du type t et un élément du type t et qui compte le nombre d’apparitions de l’élément dans la liste. Annoter compte pour qu’elle soit de type t list -> t -> int. *)
let rec compte (liste : t list) (elt : t) =
  match liste with
  | [] -> 0
  | head :: tail -> if head = elt then 1 + compte tail elt else compte tail elt
;;

assert (compte [ R; B; B; B; N; N; B; R; B ] B = 5);
Printf.printf "OK\n"

(* Ecrire une fonction plus_grande_sequence qui renvoie la longueur de la plus grande séquence de B dans une liste passée en paramètre. Par séquence on entend suite continue d’un élément. *)
let plus_grande_sequence (liste : t list) =
  let rec aux lst max_counter counter =
    match lst with
    | [] -> max_counter
    | head :: tail ->
        if head = B then
          if max_counter = counter then aux tail (counter + 1) (counter + 1)
          else aux tail max_counter (counter + 1)
        else aux tail max_counter 0
  in
  aux liste 0 0
;;

assert (plus_grande_sequence [ R; B; B; B; N; N; B; R; B ] = 3);
Printf.printf "OK\n"

(* Ecrire toutes les fonctions précédentes à l'aide de l'API List *)
let compte_list (liste : t list) (elt : t) =
  List.fold_left (fun x y -> x + if y = elt then 1 else 0) 0 liste
;;

assert (compte_list [ R; B; B; B; N; N; B; R; B ] B = 5);
Printf.printf "OK\n"

let plus_grande_sequence_list (liste : t list) =
  let result, _ =
    List.fold_left
      (fun (max_counter, counter) y ->
        if y = B then
          if max_counter = counter then (counter + 1, counter + 1)
          else (max_counter, counter + 1)
        else (max_counter, 0))
      (0, 0) liste
  in
  result
;;

assert (plus_grande_sequence_list [ R; B; B; B; N; N; B; R; B ] = 3);
Printf.printf "OK\n"

(** 4.2 Entiers naturels de Peano *)

type nat = Zero | Succ of nat

let est_nul (peano : nat) = peano = Zero

let valeur_entier (peano : nat) =
  let rec aux number value =
    match number with Zero -> value | Succ next -> aux next (value + 1)
  in
  aux peano 0
(* Le type de la fonction est nat -> int *)

let rec peano (entier : int) =
  if entier < 0 then failwith "L'entier doit être positif !"
  else match entier with 0 -> Zero | _ -> Succ (peano (entier - 1))
(* Le type de la fonction est int -> nat *)

let addition (peano1 : nat) (peano2 : nat) =
  match (peano1, peano2) with
  | Zero, Zero -> Zero
  | _, Zero -> peano1
  | Zero, _ -> peano2
  | _, _ -> peano (valeur_entier peano1 + valeur_entier peano2)
(* Le type de la fonction est nat -> nat -> nat *)
