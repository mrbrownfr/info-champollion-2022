#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef int entier;
typedef int *ptr_entier;
typedef entier *ptr_entier_v2;

struct personne_s
{
    char prenom[256];
    int age;
};
typedef struct personne_s personne;

/// @brief Affiche le prénom et l'âge d'une personne
/// @param prsn un pointeur vers une personne
void afficher_personne(personne *prsn)
{
    printf("Prénom de la personne: %s\nAge de la personne: %d\n", prsn->prenom, prsn->age);
}

int main()
{
    struct personne_s *ptr_personne = malloc(sizeof(struct personne_s));
    personne personne1 = {.prenom = "Toto", .age = 23};
    strncpy(ptr_personne->prenom, "Titi", 256);
    ptr_personne->age = 50;

    afficher_personne(&personne1);

    free(ptr_personne);

    return 0;
}
