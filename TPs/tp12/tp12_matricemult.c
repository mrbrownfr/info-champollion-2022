#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include "outils.h"

#define TAILLE_MAX_AFFICHAGE 16

/* Calcule le produit de deux matrices A et B et ajoute le résultat dans C.
 * A est de taille M x K
 * B est de taille K x N
 * C est de taille M x N
 * Entrées : M nombre de lignes de C
 *           N nombre de colonnes de C
 *           K nombre de colonnes de A et de lignes de B
 *           A matrice
 * Sortie : pas de valeur de retour, le produit AxB a été ajouté à la matrice C.
 */
void mat_mm(unsigned M, unsigned N, unsigned K, const double *A, const double *B, double *C)
{
}

/* Calcule par bloc le produit de deux matrices A et B et ajoute le résultat dans C.
 * A est de taille M x K
 * B est de taille K x N
 * C est de taille M x N
 * Entrées : b la taille de blocage
 *           M nombre de lignes de C
 *           N nombre de colonnes de C
 *           K nombre de colonnes de A et de lignes de B
 *           A matrice
 * Sortie : pas de valeur de retour, le produit AxB a été ajouté à la matrice C.
 */
// void mat_mm_bloc(unsigned b, unsigned M, unsigned N, unsigned K, double *A, double *B, double *C)
// {
// }

void usage()
{
  printf("./tp12_matricemult [M [N K b]]\n");
  printf("\n");
  exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
  unsigned M, N, K; /* dimension des matrices */
  int b = 0;        /* taille de blocage */

  struct timespec temps_debut, temps_fin;

  double duree_mm;
  double duree_mm_bloc;

  if (argc == 2)
  {
    sscanf(argv[1], "%u", &M);
    N = M;
    K = M;
  }
  else if (argc == 4 || argc == 5)
  {
    sscanf(argv[1], "%u", &M);
    sscanf(argv[2], "%u", &N);
    sscanf(argv[3], "%u", &K);
  }
  else if (argc == 1)
  {
    M = N = K = 4;
  }
  else
  {
    usage();
  }

  if (argc == 5)
  {
    sscanf(argv[4], "%u", &b);
  }

  /* pour s'assurer que les tailles des matrices sont des multiples de la taille
   * de blocage */
  if (b > 0)
  {
    M = (M + b - 1) / b * b;
    N = (N + b - 1) / b * b;
    K = (K + b - 1) / b * b;
  }

  printf("M:%i, N:%i, K:%i\n", M, N, K);
  printf("Taille de blocage :%i\n", b);

  /* allocation des espaces mémoires */
  double *A = (double *)malloc(sizeof(double) * M * K);
  if (A == NULL)
    exit(EXIT_FAILURE);
  double *B = (double *)malloc(sizeof(double) * K * N);
  if (B == NULL)
    exit(EXIT_FAILURE);
  double *C = (double *)malloc(sizeof(double) * M * N);
  if (C == NULL)
    exit(EXIT_FAILURE);
  double *Cb = (double *)malloc(sizeof(double) * M * N);
  if (Cb == NULL)
    exit(EXIT_FAILURE);

  /* initialisation des matrices */
  mat_shift(M, K, A);
  mat_fill(K, N, B);
  // mat_random(K, N, B);

  if (M <= TAILLE_MAX_AFFICHAGE)
  {
    printf("A:\n");
    mat_print(M, K, A);
    printf("B:\n");
    mat_print(K, N, B);
  }

  /* multiplication de matrices, calcul de C = C + AxB */
  for (int i = 0; i < 3; i++)
  {
    mat_zero(M, N, C);
    clock_gettime(CLOCK_MONOTONIC, &temps_debut);
    mat_mm(M, N, K, A, B, C);
    clock_gettime(CLOCK_MONOTONIC, &temps_fin);
    duree_mm = difference_temps(&temps_debut, &temps_fin);
    printf("Temps C = AxB classique %f (ms)\n", duree_mm / 1.e6);
    printf("Valeur de C[45] %f\n", C[45]);
  }

  if (M <= TAILLE_MAX_AFFICHAGE)
  {
    printf("C:\n");
    mat_print(M, N, C);
  }

  if (b > 0)
  {
    /* multiplication de matrices, calcul de C = C + AxB par bloc */
    for (int i = 0; i < 3; i++)
    {
      mat_zero(M, N, Cb);
      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      // mat_mm_bloc(b,M,N,K,A,B,Cb);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);
      duree_mm_bloc = difference_temps(&temps_debut, &temps_fin);
      printf("Temps C = AxB par bloc %f (ms)\n", duree_mm_bloc / 1.e6);
      printf("Valeur de C[45] %f\n", C[45]);
    }

    if (!mat_is_eq(M, N, C, Cb))
    {
      fprintf(stderr, "Error: C != Cblock\n");
      free(A);
      free(B);
      free(C);
      free(Cb);
      return -1;
    }

    if (M <= TAILLE_MAX_AFFICHAGE)
      mat_print(M, N, Cb);
  }

  /* libération des espaces mémoires */
  free(Cb);
  free(C);
  free(B);
  free(A);
  return 0;
}
