## Présentation du modèle

### Question 1

Le temps nécessaire à l'exécution est $T = \dfrac{W}{\pi}$ et la performance est $P = \pi$

### Question 2

Le temps nécessaire à l'exécution est $T = \dfrac{Q}{\beta}$ et la performance est $P = I\beta$

### Question 3

Dans le cas général, une borne de performance est $\dfrac{I\beta}{\pi}$. Lorsque $I$ est faible, la performance est limitée par le débit mémoire et lorsque $I$ est élevé, la performance est limitée par l'unité d'exécution.

### Question 4

La performance vaut $P = \dfrac{W}{\dfrac{W}{\pi} + \dfrac{Q}{\beta}} = \dfrac{\pi I}{I + \dfrac{\pi}{\beta}} \longrightarrow \pi$ lorsque $I \longrightarrow +\infty$
### Question 5

Pour $I = 2$, $\dfrac{10\times10^9\times2}{2 + \dfrac{10\times10^9}{8\times10^9}} = \dfrac{8}{13} \times10^{10}$ 
Pour $I = 5$, $\dfrac{10\times10^9\times5}{5 + \dfrac{10\times10^9}{8\times10^9}} = \dfrac{8}{13} \times10^{10}$
Pour $I = 2$, $\dfrac{10\times10^9\times2}{2 + \dfrac{10\times10^9}{8\times10^9}} = \dfrac{8}{13} \times10^{10}$

## Produit matrice vecteur

### Question 6

Il y a $Q = 3n + n^2$ échanges avec la mémoire.

### Question 7

Il y a $W = 2n^2$ opérations arithmétiques.

### Question 8

$I = \dfrac{W}{Q} = \dfrac{3n+n^2}{2n^2} = \dfrac{n+3}{2n} \longrightarrow \dfrac{1}{2}$ lorsque $n$ est très grand.

### Question 9

## Produit matrice matrice

### Question 10

Il y a $W = 2n^3$ opérations arithmétiques.

### Question 11

Il y a $Q = n^2 + n^2 + n^3 + n^2 = 3n^2 + n^3$ échanges avec la mémoire.

### Question 12

$I = \dfrac{W}{Q} = \dfrac{2n^3}{3n^2 + n^3} = \dfrac{2n}{3+n}$

## Produit matrice par bloc

### Question 13

$W = N^3 \times 2b^3 = 2n^3$, $Q = N^2 \times b^2 + 2N^3 \times b^2 + N^2\times b^2 = 2n^2 + 2n^3N$ et $I = \dfrac{W}{Q} = \dfrac{2n^3}{2n^2 + 2n^3N} = \dfrac{n}{1+nN} =$ 

