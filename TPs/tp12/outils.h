#include <time.h>
#include <stdbool.h>

/* Renvoie la difference de temps en nanosecondes entre deux structures de type
 * timespec initialisées avec la fonction clock_gettime().
 * Entrées : start, end deux pointeurs vers des structures timespec contenant
 *           une représentation de temps.
 * Sortie : la fonction renvoie un double représentant le temps en nanosecondes
 *          écoulé entre les deux temps.
 */
double difference_temps(const struct timespec *start, const struct timespec *end);

/**
 * Affiche une matrice de taille M x N.
 * Entrées : M nombre de lignes
 *           N nombre de colonnes
 *           A pointeur vers la première case de la matrice de double, supposée
 *           allouée et initialisée.
 */
void mat_print(unsigned M, unsigned N, const double *A);


/**
 * Remplit une matrice de taille M x N avec pour chaque case la somme des indices de la case.
 * Entrées : M nombre de lignes
 *           N nombre de colonnes
 *           A pointeur vers la première case de la matrice de double, supposée
 *           allouée.
 * Sortie  : pas de valeur de retour, les cases de la matrice A sont remplies
 *           avec la somme de leurs indices.
 */
void mat_fill(unsigned M, unsigned N, double *A);


/**
 * Initialise une matrice de taille MxN avec 0.
 * Entrées : M nombre de lignes
 *           N nombre de colonnes
 *           A pointeur vers la première case de la matrice de double, supposée
 *           déjà allouée.
 * Sortie : pas de valeur de retour, les cases de la matrice A contiennent des 0.
 */
void mat_zero(unsigned M, unsigned N, double *A);


/**
 * Initialise une matrice de taille MxN avec 0 sauf sur la diagonale située
 * après la diagonale.
 * Entrées : M nombre de lignes
 *           N nombre de colonnes
 *           A pointeur vers la première case de la matrice de double, supposée
 *           déjà allouée.
 * Sortie : pas de valeur de retour, les cases de la matrice A contiennent des 0 sauf sur la diagonale située après la diagonale médiane..
 */
void mat_shift(unsigned M, unsigned N, double *A);


/**
 * Initialise une matrice de taille MxN avec des valeurs aléatoires.
 * Entrées : M nombre de lignes
 *           N nombre de colonnes
 *           A pointeur vers la première case de la matrice de double, supposée
 *           déjà allouée.
 * Sortie : pas de valeur de retour, les cases de la matrice A contiennent des valeurs aléatoires.
 */
void mat_random(unsigned M, unsigned N, double *A);


/**
 * Détermine si deux matrices sont égales.
 * Entrées : M nombre de lignes
 *           N nombre de colonnes
 *           A pointeur vers la première case d'une matrice
 *           B pointeur vers la première case d'une autre matrice
 * Sortie :  true si les deux matrices contiennent les mêmes valeurs
 *           false sinon
 */
bool mat_is_eq(unsigned M, unsigned N, const double *A, const double *B);



