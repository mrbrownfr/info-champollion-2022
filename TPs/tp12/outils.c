#include <stdio.h>
#include <stdlib.h>
#include "outils.h"
#include <time.h>

/********************************************/
/* La spécification complète des fonctions se trouvent dans le fichier outils.h */
/********************************************/

double difference_temps(const struct timespec *start, const struct timespec *end)
{
  return (((end->tv_sec-start->tv_sec)*1e9)+end->tv_nsec - start->tv_nsec);
}


void mat_print(unsigned M, unsigned N, const double *A)
{
  for(unsigned i = 0; i < M; i++)
  {
    for(unsigned j = 0; j < N; j++)
    {
      printf("%f ", A[i*N+j]);
    }
    printf("\n");
  }
  printf("\n");
}


void mat_fill(unsigned M, unsigned N, double *A)
{
  for(unsigned i = 0; i < M; i++)
  {
    for(unsigned j = 0; j < N; j++)
    {
      A[i*N+j] = i+j;
    }
  }
}

void mat_zero(unsigned M, unsigned N, double *A)
{
  for(unsigned i = 0; i < M; i++)
  {
    for(unsigned j = 0; j < N; j++)
    {
      A[i*N+j] = 0;
    }
  }
}

void mat_random(unsigned M, unsigned N, double *A)
{
  for(unsigned i = 0; i < M; i++)
  {
    for(unsigned j = 0; j < N; j++)
    {
      A[i*N+j] = drand48();
    }
  }
}

void mat_shift(unsigned M, unsigned N, double *A) {
  for (unsigned i =0; i < M; i++) {
    for (unsigned j = 0; j < N; j++) {
      A[i*N+j] = j == (i+1)%M ? 1 : 0;
    }
  }
}

double abs_difference(double a, double b)
{
  if (a > b)
    return a - b;
  else
    return b - a;
}

bool mat_is_eq(unsigned M, unsigned N, const double *A, const double *B)
{
  const double epsilon = 1.e-8;
  for(unsigned i = 0; i < M; i++)
  {
    for(unsigned j = 0; j < N; j++)
    {
      if (abs_difference(A[i*N+j], B[i*N+j])/A[i*N+j] > epsilon) return false;
    }
  }
  return true;
}



