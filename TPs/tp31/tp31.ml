type formule =
  | Vrai
  | Faux
  | Var of int
  | Non of formule
  | Et of formule * formule
  | Ou of formule * formule
  | Implique of formule * formule

type valuation = bool array

(* Question 1 *)

let phi =
  Implique
    (Et (Var 0, Non (Var 2)), Et (Non (Var 0), Non (Et (Var 1, Non (Var 2)))))

(*
   | x | y | z | x et non z | non (y et non z) | non x et non (y et non z) | phi |
   | V | V | V | F          | V                | F                         | V   |
   | V | V | F | V          | F                | F                         | F   |
   | V | F | V | F          | V                | F                         | V   |
   | V | F | F | V          | V                | F                         | F   |
   | F | V | V | F          | V                | V                         | V   |
   | F | V | F | F          | F                | F                         | V   |
   | F | F | V | F          | V                | V                         | V   |
   | F | F | F | F          | V                | V                         | V   |
*)

(* Question 2 *)

let rec eval f v =
  match f with
  | Vrai -> true
  | Faux -> false
  | Var i -> v.(i)
  | Non f -> not @@ eval f v
  | Et (f1, f2) -> eval f1 v && eval f2 v
  | Ou (f1, f2) -> eval f1 v || eval f2 v
  | Implique (f1, f2) -> eval (Non f1) v || eval f2 v
;;

(* Question 3 *)
assert (eval phi [| true; true; true |] = true);;
assert (eval phi [| true; true; false |] = false);;
assert (eval phi [| true; false; true |] = true);;
assert (eval phi [| true; false; false |] = false);;
assert (eval phi [| false; true; true |] = true);;
assert (eval phi [| false; true; false |] = true);;
assert (eval phi [| false; false; true |] = true);;
assert (eval phi [| false; false; false |] = true)

(* Question 4 *)

let rec formule_vers_expr f =
  match f with
  | Vrai -> "vrai"
  | Faux -> "faux"
  | Var i -> Printf.sprintf "%d" i
  | Non f -> "non (" ^ formule_vers_expr f ^ ")"
  | Et (f1, f2) ->
      "(" ^ formule_vers_expr f1 ^ " et " ^ formule_vers_expr f2 ^ ")"
  | Ou (f1, f2) ->
      "(" ^ formule_vers_expr f1 ^ " ou " ^ formule_vers_expr f2 ^ ")"
  | Implique (f1, f2) ->
      "(" ^ formule_vers_expr f1 ^ " implique " ^ formule_vers_expr f2 ^ ")"
;;

Printf.printf "%s" (formule_vers_expr phi)

let rec formule_vers_expr_v2 f t =
  match f with
  | Vrai -> "vrai"
  | Faux -> "faux"
  | Var i -> t.(i)
  | Non f -> "non (" ^ formule_vers_expr_v2 f t ^ ")"
  | Et (f1, f2) ->
      "(" ^ formule_vers_expr_v2 f1 t ^ " et " ^ formule_vers_expr_v2 f2 t ^ ")"
  | Ou (f1, f2) ->
      "(" ^ formule_vers_expr_v2 f1 t ^ " ou " ^ formule_vers_expr_v2 f2 t ^ ")"
  | Implique (f1, f2) ->
      "(" ^ formule_vers_expr_v2 f1 t ^ " implique " ^ formule_vers_expr_v2 f2 t
      ^ ")"
;;

Printf.printf "%s" (formule_vers_expr_v2 phi [| "x"; "y"; "z" |])

let rec taille f =
  match f with
  | Vrai -> 1
  | Faux -> 1
  | Var i -> 1
  | Non f -> 1 + taille f
  | Et (f1, f2) | Ou (f1, f2) | Implique (f1, f2) -> 1 + taille f1 + taille f2

let var_max f =
  let rec aux f m =
    match f with
    | Vrai -> m
    | Faux -> m
    | Var i -> max i m
    | Non f -> aux f m
    | Et (f1, f2) | Ou (f1, f2) | Implique (f1, f2) -> max (aux f1 m) (aux f2 m)
  in
  aux f (-1)
;;

Printf.printf "%d" (var_max phi)

(* Question 8 *)
(* On peut tester la satisfiabilité de toutes les valuations en les considérant comme leur
   entier x et en parcourant tous les entiers de 0 à 2^{n+1} afin d'obtenir
   leur valuation en binaire *)

(* Question 9 *)

let rec substitution f i g =
  match f with
  | Vrai -> Vrai
  | Faux -> Faux
  | Var j -> if j = i then g else Var j
  | Non f -> Non (substitution f i g)
  | Et (f1, f2) -> Et (substitution f1 i g, substitution f2 i g)
  | Ou (f1, f2) -> Ou (substitution f1 i g, substitution f2 i g)
  | Implique (f1, f2) -> Implique (substitution f1 i g, substitution f2 i g)

(* Question 10 *)

let f10 =
  Et
    ( Implique (Var 0, Et (Var 1, Ou (Non (Var 0), Var 2))),
      Non (Et (Var 0, Var 2)) )

let rec elimination_constantes f =
  match f with
  | Vrai -> Vrai
  | Faux -> Faux
  | Var i -> Var i
  | Non f -> (
      match elimination_constantes f with Vrai -> Faux | Faux -> Vrai | f -> f)
  | Et (f1, f2) -> (
      match (elimination_constantes f1, elimination_constantes f2) with
      | Faux, _ | _, Faux -> Faux
      | Vrai, f | f, Vrai -> f
      | f1, f2 -> Et (f1, f2))
  | Ou (f1, f2) -> (
      match (elimination_constantes f1, elimination_constantes f2) with
      | Vrai, _ | _, Vrai -> Vrai
      | Faux, f | f, Faux -> f
      | f1, f2 -> Ou (f1, f2))
  | Implique (f1, f2) -> (
      match (elimination_constantes f1, elimination_constantes f2) with
      | Faux, _ | _, Vrai -> Vrai
      | Vrai, f | f, Faux -> f
      | f1, f2 -> Implique (f1, f2))

(* Question 13 *)

type decision = Feuille of bool | Noeud of int * decision * decision

let rec construire_arbre f =
  match elimination_constantes f with
  | Vrai -> Feuille true
  | Faux -> Feuille false
  | _ ->
      let i = var_max f in
      Noeud
        ( i,
          construire_arbre (substitution f i Vrai),
          construire_arbre (substitution f i Faux) )

(* Question 14 *)

let satisfiable_via_arbre f =
  let arbre = construire_arbre f in
  let rec aux a =
    match a with Feuille v -> v | Noeud (_, g, d) -> aux g || aux d
  in
  aux arbre

(* Question 15 *)

(* Question 16 *)

type graphe = int list array

(* let rec encode (g : int list array) k =
   let n = Array.length g in
   let var i j = Var ((i * n) + j) in
   let rec ou_f1 i j =
     if j = k - 1 then var i j else Ou (var i j, ou_f1 i (j + 1))
   in
   let rec f1 i = if i = n then ou_f1 i 0 else Et (ou_f1 i 0, f1 (i + 1)) in
   let rec f2 i = () in f3 = () in
   Et (Et (f1 1, f2), f3) *)

let rec encode (g : graphe) k =
  let n = Array.length g in
  let var i j = Var ((i * n) + j) in
  let bool_to_formule b = match b with true -> Vrai | false -> Faux in
  let rec gen_or l =
    match l with
    | [ e ] -> e
    | e :: q -> Ou (e, gen_or q)
    | _ -> failwith "liste vide"
  in
  let rec gen_and l =
    match l with
    | [ e ] -> e
    | e :: q -> Ou (e, gen_and q)
    | _ -> failwith "liste vide"
  in
  let f1 =
    List.init n (fun i -> List.init k (fun j -> var (i + 1) j) |> gen_or)
    |> gen_and
  in
  let f2 =
    List.init n (fun i ->
        List.init (k - 1) (fun j1 ->
            List.init (k - j1) (fun j2 ->
                Ou (var (i + 1) j1, var (i + 1) (j1 + j2 + 1)))
            |> gen_and)
        |> gen_and)
    |> gen_and
  in
  let f3 =
    List.init n (fun i ->
        List.init (n - i) (fun j ->
            List.init k (fun q ->
                Implique
                  ( bool_to_formule (List.mem (i + j + 1) g.(i)),
                    Et (var i q, Non (var (j + 1 + i) q)) ))
            |> gen_or)
        |> gen_and)
    |> gen_and
  in
  Et (Et (f1, f2), f3)

let est_k_coloriable (g : graphe) k =
  if k = 1 then Array.for_all (( = ) []) g
  else satisfiable_via_arbre (encode g k)

let g1 : graphe =
  [| [ 1; 2 ]; [ 0; 2 ]; [ 0; 1; 3 ]; [ 2; 4 ]; [ 3; 5 ]; [ 4 ]; [] |]
