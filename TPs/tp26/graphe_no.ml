type vertex = int
type edge = vertex * vertex

(* Question 7 *)
type graph = bool array array

let create_graph n : graph = Array.make_matrix n n false
(* O(n^2) *)

let nb_vertices (g : graph) = Array.length g
(* O(n) *)

let has_edge (g : graph) ((i, j) : edge) = g.(i).(j)
(* O(1) *)

let add_edge (g : graph) ((i, j) : edge) =
  g.(i).(j) <- true;
  g.(j).(i) <- true
(* O(1) *)

let remove_edge (g : graph) ((i, j) : edge) =
  g.(i).(j) <- false;
  g.(j).(i) <- false
(* O(1) *)

let neighbours (g : graph) (v : vertex) =
  List.init (nb_vertices g) (fun i -> i) |> List.filteri (fun j i -> g.(v).(i))
(* O(n) *)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  let rec filter l =
    match l with
    | [] -> []
    | ((i, j) as e) :: q ->
        if List.mem (i, j) q || List.mem (j, i) q then filter q
        else e :: filter q
  in
  aux 0 |> filter
(* O(n^2) *)

(* Question 8 *)
(* voir commentaire sous fonctions *)

(* Question 9 *)
(* La complexité spatiale de cette représentation est n*n où n est le nombre de sommets du graphe *)

(* Question 10 *)
let outdegree (g : graph) (v : vertex) = neighbours g v |> List.length

let outdegree (g : graph) (v : vertex) : int =
  Array.fold_left (fun acc x -> if x then acc + 1 else acc) 0 g.(v)
(* O(n) *)

let indegree (g : graph) (v : vertex) = outdegree g v

(* Question 12 *)
let to_oriented (g : graph) = ()
