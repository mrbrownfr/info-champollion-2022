type vertex = int
type edge = vertex * vertex
type graph = vertex list array

let create_graph n : graph = Array.init n (fun _ -> [])
(* O(n) *)

let nb_vertices (g : graph) = Array.length g
(* O(n) *)

let has_edge (g : graph) ((i, j) : edge) = List.mem j g.(i)
(* O(n) *)

let add_edge (g : graph) ((i, j) : edge) =
  let rec insert x l =
    match l with
    | [] -> [ x ]
    | e :: q -> if e >= x then x :: e :: q else e :: insert x q
  in
  g.(i) <- insert j g.(i)
(* O(n) *)

let remove_edge (g : graph) ((i, j) : edge) =
  let rec remove x l =
    match l with [] -> [] | e :: q -> if e = x then q else e :: remove x q
  in
  g.(i) <- remove j g.(i)
(* O(n) *)

let neighbours (g : graph) (v : vertex) = g.(v)
(* O(1) *)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  aux 0
(* O(n) *)

let outdegree (g : graph) (v : vertex) : int = List.length g.(v)
(* O(n) *)

let indegree (g : graph) (v : vertex) : int =
  Array.fold_left (fun acc l -> if List.mem v l then acc + 1 else acc) 0 g
(* O(n^2) *)

let reverse (g : graph) : graph =
  let rev = create_graph (nb_vertices g) in
  List.iter (fun (i, j) -> add_edge rev (j, i)) (all_egdes g);
  rev

let to_non_oriented (g : graph) =
  List.iter
    (fun (i, j) -> if not @@ has_edge g (j, i) then add_edge g (j, i))
    (all_egdes g)
