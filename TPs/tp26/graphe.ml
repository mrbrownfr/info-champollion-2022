type vertex = int
type edge = vertex * vertex

(* Question 7 *)
type graph = bool array array

let create_graph n : graph = Array.make_matrix n n false
(* O(n^2) *)

let nb_vertices (g : graph) = Array.length g
(* O(n) *)

let has_edge (g : graph) ((i, j) : edge) = g.(i).(j)
(* O(1) *)

let add_edge (g : graph) ((i, j) : edge) = g.(i).(j) <- true
(* O(1) *)

let remove_edge (g : graph) ((i, j) : edge) = g.(i).(j) <- false
(* O(1) *)

let neighbours (g : graph) (v : vertex) =
  List.init (nb_vertices g) (fun i -> i) |> List.filteri (fun j i -> g.(v).(i))
(* O(n) *)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  aux 0
(* O(n^2) *)

(* Question 8 *)
(* voir commentaire sous fonctions *)

(* Question 9 *)
(* La complexité spatiale de cette représentation est n*n où n est le nombre de sommets du graphe *)

(* Question 10 *)
(* let outdegree (g : graph) (v : vertex) = neighbours g v |> List.length *)

let outdegree (g : graph) (v : vertex) : int =
  Array.fold_left (fun acc x -> if x then acc + 1 else acc) 0 g.(v)
(* O(n) *)

let indegree (g : graph) (v : vertex) : int =
  Array.fold_left (fun acc arr -> if arr.(v) then acc + 1 else acc) 0 g
(* O(n) *)

(* Question 19 *)
let reverse (g : graph) : graph =
  let transpose mat =
    let w, h = (Array.length mat.(0), Array.length mat) in
    let t = Array.make_matrix w h mat.(0).(0) in
    for i = 0 to h - 1 do
      for j = 0 to w - 1 do
        t.(j).(i) <- mat.(i).(j)
      done
    done;
    t
  in
  transpose g

(* Question 12 *)
let to_non_oriented (g : graph) =
  List.iter (fun (i, j) -> add_edge g (j, i)) (all_egdes g)
