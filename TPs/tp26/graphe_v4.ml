type vertex = int
type edge = vertex * vertex

(* Question 21 *)
type 'a graph = 'a option array array

let create_graph n : 'a graph = Array.make_matrix n n None
(* O(n^2) *)

let nb_vertices (g : 'a graph) = Array.length g
(* O(n) *)

let has_edge (g : 'a graph) ((i, j) : edge) = Option.is_some g.(i).(j)
(* O(1) *)

let add_edge (g : 'a graph) ((i, j) : edge) (v : 'a) = g.(i).(j) <- Some v
(* O(1) *)

let remove_edge (g : 'a graph) ((i, j) : edge) = g.(i).(j) <- None
(* O(1) *)

let neighbours (g : 'a graph) (v : vertex) =
  List.init (nb_vertices g) (fun i -> i)
  |> List.filteri (fun j i -> has_edge g (v, i))
(* O(n) *)

let all_egdes (g : 'a graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  aux 0
(* O(n^2) *)

let outdegree (g : 'a graph) (v : vertex) : int =
  Array.fold_left
    (fun acc x -> if Option.is_some x then acc + 1 else acc)
    0 g.(v)
(* O(n) *)

let indegree (g : 'a graph) (v : vertex) : int =
  Array.fold_left
    (fun acc arr -> if Option.is_some arr.(v) then acc + 1 else acc)
    0 g
(* O(n) *)

let reverse (g : 'a graph) : 'a graph =
  let transpose mat =
    let w, h = (Array.length mat.(0), Array.length mat) in
    let t = Array.make_matrix w h mat.(0).(0) in
    for i = 0 to h - 1 do
      for j = 0 to w - 1 do
        t.(j).(i) <- mat.(i).(j)
      done
    done;
    t
  in
  transpose g

let is_valid_color (g : 'a graph) (color : 'b array) =
  List.for_all (fun (i, j) -> color.(i) <> color.(j)) (all_egdes g)
