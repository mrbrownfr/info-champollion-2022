type vertex = int
type edge = vertex * vertex

(* Question 13 *)
type graph = vertex list array

let create_graph n : graph = Array.init n (fun _ -> [])
(* O(n) *)

let nb_vertices (g : graph) = Array.length g
(* O(n) *)

let has_edge (g : graph) ((i, j) : edge) = List.mem j g.(i)
(* O(n) *)

let add_edge (g : graph) ((i, j) : edge) =
  g.(i) <- j :: g.(i);
  g.(j) <- i :: g.(j)
(* O(1) *)

let remove_edge (g : graph) ((i, j) : edge) =
  g.(i) <- List.filter (fun v -> v <> j) g.(i);
  g.(j) <- List.filter (fun v -> v <> i) g.(j)
(* O(n) *)

let neighbours (g : graph) (v : vertex) = g.(v)
(* O(1) *)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  let rec filter l =
    match l with
    | [] -> []
    | ((i, j) as e) :: q ->
        if List.mem (i, j) q || List.mem (j, i) q then filter q
        else e :: filter q
  in
  aux 0 |> filter
(* O(n) *)

(* Question 14 *)
(* voir commentaire sous fonctions *)

(* Question 15 *)
(* La complexité spatiale de cette représentation est |A| *)

(* Question 16 *)
(* let outdegree (g : graph) (v : vertex) = neighbours g v |> List.length *)

let outdegree (g : graph) (v : vertex) : int = List.length g.(v)
(* O(n) *)

let indegree (g : graph) (v : vertex) : int = outdegree g v
(* O(n^2) *)

(* Question 18 *)
let to_oriented (g : graph) = ()
