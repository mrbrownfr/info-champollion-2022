(* Question 1 *)
(* On obtient la signature des fonctions du fichier tri.ml *)

(* Question 2 *)
(* On obtient deux fichiers tri.cmo et tri.cmi *)

(* Question 3 *)
(* Le nom de l'exécutable est test_tri *)

(* Question 4 *)
(* Lorsqu'on inverse les deux, le compilateur renvoie une erreur stipulant que l'ordre
   des fichiers est le mauvais *)

(* Question 5 *)
(* Il s'agit d'une structure de données impérative (mutable) *)

(* Question 6 *)
(* On ne peut pas ajouter de sommest car sinon c'est un tout nouveau graphe
   que l'on représente. *)
