type vertex = int
type edge = vertex * vertex

(* Question 20 *)
type graph = { vertices : vertex list array; mutable nb_edges : int }

let create_graph n : graph =
  { vertices = Array.init n (fun _ -> []); nb_edges = 0 }
(* O(n) *)

let nb_vertices (g : graph) = Array.length g.vertices
(* O(n) *)

let has_edge (g : graph) ((i, j) : edge) = List.mem j g.vertices.(i)
(* O(n) *)

let add_edge (g : graph) ((i, j) : edge) =
  g.vertices.(i) <- j :: g.vertices.(i);
  g.nb_edges <- g.nb_edges + 1
(* O(1) *)

let remove_edge (g : graph) ((i, j) : edge) =
  g.vertices.(i) <- List.filter (fun v -> v <> j) g.vertices.(i);
  g.nb_edges <- g.nb_edges - 1
(* O(n) *)

let neighbours (g : graph) (v : vertex) = g.vertices.(v)
(* O(1) *)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  aux 0
(* O(n) *)

let outdegree (g : graph) (v : vertex) : int = List.length g.vertices.(v)
(* O(n) *)

let indegree (g : graph) (v : vertex) : int =
  Array.fold_left
    (fun acc l -> if List.mem v l then acc + 1 else acc)
    0 g.vertices
(* O(n^2) *)

let reverse (g : graph) : graph =
  let rev = create_graph (nb_vertices g) in
  List.iter (fun (i, j) -> add_edge rev (j, i)) (all_egdes g);
  rev

let to_non_oriented (g : graph) =
  List.iter
    (fun (i, j) -> if not @@ has_edge g (j, i) then add_edge g (j, i))
    (all_egdes g)

let nb_edges (g : graph) = g.nb_edges
