let rec inserer n l = match l with
  | [] -> [n]
  | e::q -> if n < e then n::e::q else e::(inserer n q)

let tri_insertion l =
  let rec aux l1 l2 = match l2 with
    | [] -> l1
    | e::q -> aux (inserer e l1) q
in aux [] l