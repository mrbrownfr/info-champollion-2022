let liste_aleatoire nb =
let max_rand = nb * 2 in
let rec liste_gen n = if n = 0 then []
  else (Random.int max_rand)::(liste_gen (n-1))
in liste_gen nb

let listeAlea100 = liste_aleatoire 100;;

let rec est_croissante l = match l with
  | [] -> true
  | [e] -> true
  | e1::e2::q -> e1 <= e2 && est_croissante (e2::q)


let c = Sys.time ();;

let res = Tri.tri_insertion listeAlea100;;

let d = Sys.time ();;

assert (est_croissante res);;

print_string "Tri insertion Alea 1000\n";;
print_float (d -. c);;
print_string "\n";;
