type vertex = int
type edge = vertex * vertex

(* Question 13 *)
type graph = vertex list array

let create_graph n : graph = Array.init n (fun _ -> [])
(* O(n) *)

let nb_vertices (g : graph) = Array.length g
(* O(n) *)

let has_edge (g : graph) ((i, j) : edge) = List.mem j g.(i)
(* O(n) *)

let add_edge (g : graph) ((i, j) : edge) = g.(i) <- j :: g.(i)
(* O(1) *)

let remove_edge (g : graph) ((i, j) : edge) =
  g.(i) <- List.filter (fun v -> v <> j) g.(i)
(* O(n) *)

let neighbours (g : graph) (v : vertex) = g.(v)
(* O(1) *)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  aux 0
(* O(n) *)

(* Question 14 *)
(* voir commentaire sous fonctions *)

(* Question 15 *)
(* La complexité spatiale de cette représentation est |A| *)

(* Question 16 *)
(* let outdegree (g : graph) (v : vertex) = neighbours g v |> List.length *)

let outdegree (g : graph) (v : vertex) : int = List.length g.(v)
(* O(n) *)

let indegree (g : graph) (v : vertex) : int =
  Array.fold_left (fun acc l -> if List.mem v l then acc + 1 else acc) 0 g
(* O(n^2) *)

(* Question 19 *)
let reverse (g : graph) : graph =
  let rev = create_graph (nb_vertices g) in
  List.iter (fun (i, j) -> add_edge rev (j, i)) (all_egdes g);
  rev

(* Question 18 *)
let to_non_oriented (g : graph) =
  List.iter
    (fun (i, j) -> if not @@ has_edge g (j, i) then add_edge g (j, i))
    (all_egdes g)

(* let rec topo ?(ordre : int list = []) (g : graph) =
   if List.length ordre = nb_vertices g then ordre
   else
     let new_g = ref @@ Array.copy g and indices = ref [] in
     for i = 0 to nb_vertices g - 1 do
       if g.(i) = [] && (not @@ List.mem i ordre) then (
         indices := i :: !indices;
         new_g := Array.map (List.filter (( <> ) i)) !new_g)
     done;
     topo ~ordre:(!indices @ ordre) !new_g *)

let rec topo ?(ordre : int list = []) (g : graph) =
  if List.length ordre = nb_vertices g then ordre
  else
    let rec aux i =
      if g.(i) = [] && (not @@ List.mem i ordre) then
        topo ~ordre:(i :: ordre) (Array.map (List.filter (( <> ) i)) g)
      else aux (i + 1)
    in
    aux 0

let g1 = [| [ 1; 8 ]; [ 2; 8 ]; [ 5 ]; [ 4; 2 ]; [ 5 ]; []; []; [ 8 ]; [] |]
