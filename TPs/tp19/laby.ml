(* Auteur : M. Journault *)
(* #load "graphics.cma";; *)
open Graphics

type cell = { mutable north : bool ; mutable east : bool }
type labyrinthe = {
  walls  : cell array array;
  height : int;
  width  : int
};;

let draw_laby ?(visited=None) ?(path=None) (laby: labyrinthe) : unit =
  let sx = Graphics.size_x () in
  let sy = Graphics.size_y () in
  let dx = laby.width in
  let dy = laby.height in
  let r = min (sx / dx) (sy / dy) in
  let extrax = (sx - r * dx) / 2 in
  let extray = (sy - r * dy) / 2 in
  let draw_cell (i: int) (j: int) =
    if laby.walls.(i).(j).east then (
      Graphics.moveto (extrax + (j+1) * r) (extray + (i+1) * r);
      Graphics.lineto (extrax + (j+1) * r) (extray + (i  ) * r);
    );
    if laby.walls.(i).(j).north then (
      Graphics.moveto (extrax + (j+1) * r) (extray + (i+1) * r);
      Graphics.lineto (extrax + (j  ) * r) (extray + (i+1) * r);
    )
  in
  Graphics.moveto extrax (extray + 0 * r);
  Graphics.lineto (extrax+ dx * r) (extray + 0 * r);
  Graphics.moveto extrax (extray + laby.height * r);
  Graphics.lineto (extrax+ dx * r) (extray + laby.height * r);
  Graphics.moveto (extrax + 0 * r) (extray);
  Graphics.lineto (extrax + 0 * r) (extray + dy * r);
  Graphics.moveto (extrax + laby.width * r) (extray);
  Graphics.lineto (extrax + laby.width * r) (extray + dy * r);
  for i = 0 to laby.height-1 do
    for j = 0 to laby.width-1 do
      draw_cell i j
    done
  done;
  (match visited with
   | None -> ()
   | Some a ->
     for i = 0 to laby.height-1 do
       for j = 0 to laby.width-1 do
         if a.(i).(j) then (
           Graphics.set_color (Graphics.rgb 168 27 3);
           Graphics.fill_circle (extrax + j * r + r / 2) (extray + i * r + r / 2) (r / 3);
           Graphics.set_color Graphics.black
         )
       done
     done);
  (match path with
  | None -> ()
  | Some p ->
    let rec aux p = match p with
      | (i, j)::(i', j')::p' ->
        (
          Graphics.set_color (Graphics.rgb 210 160 4);
          Graphics.moveto (extrax + j * r + r / 2) (extray + i * r + r / 2);
          Graphics.lineto (extrax + j' * r + r / 2) (extray + i' * r + r / 2);
          Graphics.set_color Graphics.black;
          aux ((i', j') :: p')
        )
      | _ -> ()
    in aux p);
    (* vert : 105 105 0 *)
  (Graphics.set_color (Graphics.rgb 210 160 4);
   Graphics.fill_circle (extrax + (laby.width - 1) * r + r / 2) 
                        (extray + (laby.height - 1) * r + r / 2) (r / 3);
  Graphics.set_color (Graphics.rgb 210 160 4);
   Graphics.fill_circle (extrax +  r / 2) 
                        (extray +  r / 2) (r / 3);
  Graphics.set_color Graphics.black
  );;



let print_murs (laby: labyrinthe) : unit =
  for i = 0 to laby.height-1 do
    print_int i; print_newline ();
    print_char ' ';
    for j = 0 to laby.width-1 do
      print_int j; print_newline ();
      if laby.walls.(i).(j).east then begin print_endline ("est") end else begin
        print_endline ("pas est") end ;
      if laby.walls.(i).(j).north then begin print_endline ("nord") end else
        begin print_endline ("pas nord") end;
    done
  done


(* question 1 : init_full_laby *)

(* question 3 : neighbours *)

(* question 4 : destroy_wall *)

(* question 5 : accessible *)

(* question 6 : generate_laby_v1 *)

(* question 7 : generate_laby_v1_recursif *)

(* question 8 : generate_laby_v2 *)

(* question 9 : solve_laby *)


(* Affichage du résultat *)
let () =
  Graphics.open_graph " 800x800";
  Graphics.set_window_title "ocamlgraphics";

  (* Insérer votre code pour tester les fonctions et afficher le labyrinthe ici *)
  (* question 2 *)


  (* pour effacer la fenêtre on peut utiliser : *)
  Graphics.clear_graph();

  
  Graphics.loop_at_exit [] (fun _ -> ())

