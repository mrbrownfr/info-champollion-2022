#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tp05_outils.h"

/* Les spécifications des fonctions se trouvent dans le fichier tp05_outils.h */

int comp(const void *a, const void *b)
{
  return (*(int *)a - *(int *)b);
}

double difference_temps(const struct timespec *start, const struct timespec *end)
{
  return (end->tv_sec - start->tv_sec) * 1e9 + end->tv_nsec - start->tv_nsec;
}

int determiner_nombre_tailles(const int *tailles, const int nb, const int taille_max)
{
  int i = 0;
  while (i < nb && tailles[i] <= taille_max)
  {
    i++;
  }
  return i;
}

void random_fill(int *tableau, int nb)
{
  /* choix des éléments du tableau dans l'intervalle [0; nb * 10[ */
  for (int i = 0; i < nb; i++)
  {
    tableau[i] = rand() % (nb * 10);
  }
}

void affiche_tableau(const int *tableau, int nb)
{
  for (int i = 0; i < nb; i++)
  {
    printf("%d ", tableau[i]);
  };
  printf("\n");
}

bool est_croissant(const int *tableau, int nb)
{
  for (int i = 1; i < nb; i++)
  {
    if (tableau[i - 1] > tableau[i])
    {
      return false;
    };
  };
  return true;
};

void copie_tableau(const int *tableau, int nb, int *copie)
{
  for (int i = 0; i < nb; i++)
  {
    copie[i] = tableau[i];
  };
}

bool verifie_tri(const int *reference, const int *tableau, int nb)
{
  for (int i = 0; i < nb; i++)
  {
    if (tableau[i] != reference[i])
    {
      return false;
    }
  };
  return true;
}
