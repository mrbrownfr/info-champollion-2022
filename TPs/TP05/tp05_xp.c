/*************************************************************************
 *        Évaluation de performance de tris de tableaux d'éléments
 *
 *
 *************************************************************************/
#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "tp05_outils.h"
#include "tp05_tris.h"
#include "tp05_stats.h"


/* Affiche de l'aide sur l'utilisation du programme.
 */
void afficher_usage()
{
  printf("Utilisation : ./tp05_xp taille_max nb_iterations [file]\n");
  printf("taille_max    est un entier donnant la taille max sur laquelle effectuer les expériences\n");
  printf("nb_iterations est un entier donnant le nombre de répétitions à effectuer\n");
  printf("file          est un paramètre officiel, si vous voulez modifier le nom du fichier de résultats, par defaut : resultats.csv\n");
}

/* Lit les arguments et met à jour le nombre d'itérations, la taille max des
 * tableaux à utiliser et éventuellement le nom du fichier.
 * Entrées : argc, argv les arguments de la fonction mais
 * Sorties : mise à jour appropriée du contenu des pointeurs vers nb_iterations,
 *           taille_max et filename.
 */
void lire_arguments(int argc, char **argv, int *taille_max, int *nb_iterations, char *filename)
{
  char *str;
  str = argv[1];
  *taille_max = atoi(str);

  if (*taille_max > 10000000)
  {
    *taille_max = 524388;
  }
  if (*taille_max < 8)
  {
    *taille_max = 8;
  }

  str = argv[2];
  *nb_iterations = atoi(str);

  if (*nb_iterations < 3)
  {
    *nb_iterations = 3;
  }
  if (*nb_iterations > 50)
  {
    *nb_iterations = 32;
  }

  int length = 0;
  if (argc == 4)
  {
    str = argv[4];
    strncpy(filename, str, 256);
    filename[255] = '\0';
    length = strlen(filename);
  }
  if (length == 0)
  {
    strcpy(filename, "resultats.csv");
  }
}


int main(int argc, char **argv)
{
  int nb_elements;
  char filename[256];

  int nb_iterations;
  int taille_max;

  // vérification du nombre d'arguments
  if (argc < 3 || argc > 4)
  {
    afficher_usage();
    exit(EXIT_FAILURE);
  }

  // lecture des arguments
  lire_arguments(argc, argv, &taille_max, &nb_iterations, filename);

  printf("Initialisation des expériences avec %d iterations, et une taille maximum de %d\n", nb_iterations, taille_max);

  int tailles[] = {8, 16, 32, 64, 100, 128, 256, 500, 512, 750, 1000, 1024, 1500, 2000, 2048, 3000, 4000, 4096, 5000, 6000, 7000, 8000, 8192, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 16384, 17000, 18000, 19000, 20000, 30000, 32768, 40000, 50000, 60000, 65536, 70000, 80000, 90000, 100000, 131072, 150000, 200000, 262144, 300000, 400000, 500000, 524288};

  int nb_tailles = determiner_nombre_tailles(tailles, 54, taille_max);
  // mise à jour de la taille max pour correspondre aux entrées du tableau
  taille_max = tailles[nb_tailles-1];
  printf("nb_tailles %d, taille_max new %d\n", nb_tailles, taille_max);

  /* allocation des tableaux contenant les donnees */
  int *tableau_entree = malloc(taille_max*sizeof(int));
  if (tableau_entree == NULL)
  {
    fprintf(stderr, "tableau_entree pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_qsort = malloc(taille_max*sizeof(int));
  if (tableau_tri_qsort == NULL)
  {
    fprintf(stderr, "tableau_tri_qsort pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_sel = malloc(taille_max*sizeof(int));
  if (tableau_tri_sel == NULL)
  {
    fprintf(stderr, "tableau_tri_sel pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_ins = malloc(taille_max*sizeof(int));
  if (tableau_tri_sel == NULL)
  {
    fprintf(stderr, "tableau_tri_sel pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_rapide = malloc(taille_max*sizeof(int));
  if (tableau_tri_sel == NULL)
  {
    fprintf(stderr, "tableau_tri_sel pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  /* il n'est pas possible de tester sur des tailles trop grosses avec le tri
   * par insertion et le tri par selection */
  int nb_max_insertion = 18;

  /* déclarations des tableaux pour stocker les résultats en conservant la
   * moyenne et l'écart type pour chaque taille de tableau pour un tri donné. */
  double resultats[4][nb_tailles][2];
  /* double qsort_res_ns[nb_tailles][2];
  double insertion_res_ns[nb_max_insertion][2];
  double selection_res_ns[nb_max_insertion][2];
  double rapide_res_ns[nb_tailles][2]; */


  /* déclarations des tableaux pour stocker les valeurs lors de chaque itération
   * d'une taille donnée */
  double qsort_ns_courant[nb_iterations];
  double insertion_ns_courant[nb_iterations];
  double selection_ns_courant[nb_iterations];
  double rapide_ns_courant[nb_iterations];

  struct timespec temps_debut, temps_fin;

  /* ouverture du fichier de résultats */
  FILE *fichier_resultats = fopen(filename, "w");
  if (fichier_resultats == NULL)
  {
    fprintf(stderr, "Impossible d'ouvrir le fichier %s!\n", filename);
    exit(EXIT_FAILURE);
  }

  /*************************************************************
   *   Tests avec les éléments en ordre aléatoire.
   *************************************************************/

  /* pour l'aléatoire */
  time_t old_time = time(NULL);

  /* Répétitions des XPs pour toutes les valeurs de nombre d'éléments
   * souhaitées. */
  for(int i_taille = 0; i_taille < nb_tailles; i_taille++)
  {
    nb_elements = tailles[i_taille];
    printf("Tests aléatoires avec #%d elements, %d\n", nb_elements, nb_tailles);

    for(int iteration = 0; iteration < nb_iterations; iteration++)
    {
      /* choix d'une graine pour l'aléatoire */
      srand( old_time++);

      /* remplissage des éléments du tableau aléatoire */
      random_fill(tableau_entree, nb_elements);


      /* Quick sort */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_qsort);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      qsort(tableau_tri_qsort, nb_elements, sizeof(int), comp);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      qsort_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);

      if (i_taille < nb_max_insertion)
      {
	/* Tri par insertion */
	copie_tableau(tableau_entree, nb_elements, tableau_tri_ins);

	clock_gettime(CLOCK_MONOTONIC, &temps_debut);
	tri_insertion(tableau_tri_ins, nb_elements);
	clock_gettime(CLOCK_MONOTONIC, &temps_fin);

	insertion_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);


	/* Tri par selection */
	copie_tableau(tableau_entree, nb_elements, tableau_tri_sel);

	clock_gettime(CLOCK_MONOTONIC, &temps_debut);
	tri_selection(tableau_tri_sel, nb_elements);
	clock_gettime(CLOCK_MONOTONIC, &temps_fin);

	selection_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);
      }

      /* Tri rapide personnel */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_rapide);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_rapide(tableau_tri_rapide, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      rapide_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);
    }// nb_iterations

    /* Calcul des valeurs moyennes */
    calcule_stats(qsort_ns_courant, nb_iterations, resultats[0][i_taille]);

    fprintf(stdout, "%d,%.12g,%.12g (qsort)\n", nb_elements, resultats[0][i_taille][0], resultats[0][i_taille][1]);

    if (i_taille < nb_max_insertion)
    {
      calcule_stats(insertion_ns_courant, nb_iterations, resultats[1][i_taille]);

      fprintf(stdout, "%d,%.12g,%.12g (tri insertion)\n", nb_elements, resultats[1][i_taille][0], resultats[1][i_taille][1]);

      calcule_stats(selection_ns_courant, nb_iterations, resultats[2][i_taille]);

      fprintf(stdout, "%d,%.12g,%.12g (tri selection)\n", nb_elements, resultats[2][i_taille][0], resultats[2][i_taille][1]);
    }

    calcule_stats(rapide_ns_courant, nb_iterations, resultats[3][i_taille]);

    fprintf(stdout, "%d,%.12g,%.12g (tri_rapide)\n", nb_elements, resultats[3][i_taille][0], resultats[3][i_taille][1]);

  }// nb_tailles

  char **chaines = malloc(sizeof(double *)*4);
  char *chaine_qsort = "Qsort";
  char *chaine_insertion = "Tri insertion";
  char *chaine_selection = "Tri selection";
  char *chaine_rapide = "Tri rapide";
  chaines[0] = chaine_qsort;
  chaines[1] = chaine_insertion;
  chaines[2] = chaine_selection;
  chaines[3] = chaine_rapide;




  fprintf(fichier_resultats, "Résultats pour des éléments choisis aléatoirement (en ms)\n");
  for(int tri = 0; tri < 4; tri++)
  {
    fprintf(fichier_resultats, "%s\n", chaines[tri]);
    for(int i_taille = 0; i_taille < nb_tailles; i_taille++)
    {
      if ((tri == 0 || tri == 3) || i_taille < nb_max_insertion)
      {
	nb_elements = tailles[i_taille];
	fprintf(fichier_resultats, "%d,%.12g,%.12g\n", nb_elements, resultats[tri][i_taille][0]/1e6, resultats[tri][i_taille][1])/1e6;
      }
    }
  }

  /*************************************************************
   *   Tests avec les éléments dans l'ordre croissant.
   *************************************************************/
  if (nb_max_insertion > nb_tailles)
  {
    nb_max_insertion = nb_tailles;
  }

  for(int i_taille = 0; i_taille < nb_max_insertion; i_taille++)
  {
    nb_elements = tailles[i_taille];
    printf("Tests triés avec #%d elements\n", nb_elements);

    for(int iteration = 0; iteration < nb_iterations; iteration++)
    {
      /* remplissage des éléments du tableau */
      for(int i = 0; i < nb_elements; i++)
      {
	tableau_entree[i] = i;
      }

      /* Quick sort */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_qsort);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      qsort(tableau_tri_qsort, nb_elements, sizeof(int), comp);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      qsort_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);

      /* Tri par insertion */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_ins);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_insertion(tableau_tri_ins, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      insertion_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);


      /* Tri par selection */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_sel);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_selection(tableau_tri_sel, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      selection_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);

      /* Tri rapide personnel */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_rapide);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_rapide(tableau_tri_rapide, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      rapide_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);
    }// nb_iterations

    /* Calcul des valeurs moyennes */
    calcule_stats(qsort_ns_courant, nb_iterations, resultats[0][i_taille]);

    fprintf(stdout, "%d,%.12g,%.12g (qsort)\n", nb_elements, resultats[0][i_taille][0], resultats[0][i_taille][1]);

    if (i_taille < nb_max_insertion)
    {
      calcule_stats(insertion_ns_courant, nb_iterations, resultats[1][i_taille]);

      fprintf(stdout, "%d,%.12g,%.12g (tri insertion)\n", nb_elements, resultats[1][i_taille][0], resultats[1][i_taille][1]);

      calcule_stats(selection_ns_courant, nb_iterations, resultats[2][i_taille]);

      fprintf(stdout, "%d,%.12g,%.12g (tri selection)\n", nb_elements, resultats[2][i_taille][0], resultats[2][i_taille][1]);
    }

    calcule_stats(rapide_ns_courant, nb_iterations, resultats[3][i_taille]);

    fprintf(stdout, "%d,%.12g,%.12g (tri_rapide)\n", nb_elements, resultats[3][i_taille][0], resultats[3][i_taille][1]);
  }// nb_tailles

  fprintf(fichier_resultats, "Résultats pour des éléments dans l'ordre croissant (en ms)\n");
  for(int tri = 0; tri < 4; tri++)
  {
    fprintf(fichier_resultats, "%s\n", chaines[tri]);
    for(int i_taille = 0; i_taille < nb_max_insertion; i_taille++)
    {
      nb_elements = tailles[i_taille];
      fprintf(fichier_resultats, "%d,%.12g,%.12g\n", nb_elements, resultats[tri][i_taille][0]/1e6, resultats[tri][i_taille][1]/1e6);
    }
  }

  for(int i_taille = 0; i_taille < nb_max_insertion; i_taille++)
  {
    nb_elements = tailles[i_taille];
    printf("Tests ordre décroissant avec #%d elements\n", nb_elements);

    for(int iteration = 0; iteration < nb_iterations; iteration++)
    {
      /* remplissage des éléments du tableau */
      for(int i = 0; i < nb_elements; i++)
      {
	tableau_entree[i] = nb_elements - i - 1;
      }

      /* Quick sort */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_qsort);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      qsort(tableau_tri_qsort, nb_elements, sizeof(int), comp);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      qsort_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);

      /* Tri par insertion */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_ins);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_insertion(tableau_tri_ins, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      insertion_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);


      /* Tri par selection */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_sel);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_selection(tableau_tri_sel, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      selection_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);

      /* Tri rapide personnel */
      copie_tableau(tableau_entree, nb_elements, tableau_tri_rapide);

      clock_gettime(CLOCK_MONOTONIC, &temps_debut);
      tri_rapide(tableau_tri_rapide, nb_elements);
      clock_gettime(CLOCK_MONOTONIC, &temps_fin);

      rapide_ns_courant[iteration] = difference_temps(&temps_debut, &temps_fin);
    }// nb_iterations

    /* Calcul des valeurs moyennes */
    calcule_stats(qsort_ns_courant, nb_iterations, resultats[0][i_taille]);

    fprintf(stdout, "%d,%.12g,%.12g (qsort)\n", nb_elements, resultats[0][i_taille][0], resultats[0][i_taille][1]);

    if (i_taille < nb_max_insertion)
    {
      calcule_stats(insertion_ns_courant, nb_iterations, resultats[1][i_taille]);

      fprintf(stdout, "%d,%.12g,%.12g (tri insertion)\n", nb_elements, resultats[1][i_taille][0], resultats[1][i_taille][1]);

      calcule_stats(selection_ns_courant, nb_iterations, resultats[2][i_taille]);

      fprintf(stdout, "%d,%.12g,%.12g (tri selection)\n", nb_elements, resultats[2][i_taille][0], resultats[2][i_taille][1]);
    }

    calcule_stats(rapide_ns_courant, nb_iterations, resultats[3][i_taille]);

    fprintf(stdout, "%d,%.12g,%.12g (tri_rapide)\n", nb_elements, resultats[3][i_taille][0], resultats[3][i_taille][1]);
  }// nb_tailles

  fprintf(fichier_resultats, "Résultats pour des éléments dans l'ordre décroissant (en ms)\n");
  for(int tri = 0; tri < 4; tri++)
  {
    fprintf(fichier_resultats, "%s\n", chaines[tri]);
    for(int i_taille = 0; i_taille < nb_max_insertion; i_taille++)
    {
      nb_elements = tailles[i_taille];
      fprintf(fichier_resultats, "%d,%.12g,%.12g\n", nb_elements, resultats[tri][i_taille][0]/1e6, resultats[tri][i_taille][1]/1e6);
    }
  }
  /* fermeture du fichier de résultats */
  fclose(fichier_resultats);

  free(chaines);

  free(tableau_tri_rapide);
  free(tableau_tri_qsort);
  free(tableau_tri_ins);
  free(tableau_tri_sel);
  free(tableau_entree);
  return 0;
}

