/*************************************************************************
 *       Implémentation d'algorithmes de tris.
 *
 *
 *************************************************************************/

#include "tp05_outils.h"

/* Tri d'un tableau en appliquant le tri par sélection.
 * Entrées : tableau un pointeur vers un tableau d'entiers
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */

/// @brief Trouve le minimum dans une liste
/// @param tableau le tableau à vérifier
/// @param n le nombre d'éléments dans le tableau
/// @param depart l'index de départ de la zone à trier
/// @return le minimum du tableau
int minimum(int *tableau, int n, int depart)
{
  int mini = tableau[depart];
  int index = 0;
  for (int i = depart + 1; i < n; i++)
  {
    if (tableau[i] <= mini)
    {
      mini = tableau[i];
      index = i;
    }
  };
  return index;
}

/// @brief Implémentation du tri par sélection
/// @param tableau le tableau à trier
/// @param n le nombre d'éléments du tableau
void tri_selection(int *tableau, int n)
{
  /* Algorithme du tri par sélection */
  /* parcourir les positions du tableau jusqu'à l'avant dernière
   *	    rechercher l'élément minimum dans la partie non triée du tableau
   *	    faire un échange avec l'élément à la position courante et le minimum
   *	    trouvé
   */

  for (int i = 0; i < n - 1; i++)
  {
    int swap = tableau[minimum(tableau, n, i)];
    tableau[minimum(tableau, n, i)] = tableau[i];
    tableau[i] = swap;
  }
}

/* Tri d'un tableau en appliquant le tri par insertion.
 * Entrées : tableau un pointeur vers un tableau d'entiers
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
/// @brief Implémentation du tri par insertion
/// @param tableau le tableau à trier
/// @param n Le nombre d'éléments du tableau
void tri_insertion(int *tableau, int n)
{
  /* Algorithme du tri par insertion */
  /* parcourir les positions à partir de la deuxième
   *	    tant que l'élément courant est plus petit que l'élément précédent,
   *		  décaler l'élément précédent à la place de l'élément et recommancer
   */

  for (int i = 1; i < n; i++)
  {
    int index = 1;
    while (i - index >= 0 && tableau[i - index + 1] < tableau[i - index])
    {
      int swap = tableau[i - index + 1]; // Variable temporaire pour l'échange
      tableau[i - index + 1] = tableau[i - index];
      tableau[i - index] = swap;
      index++;
    }
  };
}

/* Effectue l'échange entre deux éléments d'un tableau.
 * Entrée : tableau  un pointeur vers un tableau d'entiers
 *          ind1     un indice d'éléments dans tableau
 *          ind2     un indice d'éléments dans tableau
 * Sortie : pas de retour, les valeurs aux indices ind1 et ind2 ont été
 * échangées.
 */
void echange(int *tableau, int ind1, int ind2)
{
  int swap = tableau[ind1]; // Variable temporaire pour l'échange
  tableau[ind1] = tableau[ind2];
  tableau[ind2] = swap;
}

/* Effectue la partition d'une partie d'un tableau de manière à mettre dans la
 * partie de gauche les éléments plus petits que le pivot, dans la partie de
 * droite les éléments plus grand que le pivot et le pivot au milieu.
 * Entrées : tableau,   un pointeur vers un tableau d'entier
 *           ind_debut, indice du début de la partie à partitionner
 *           ind_fin,   indice de la fin de la partie à partitionner (exclus)
 *           ind_pivot, l'indice auquel se trouve le pivot dans le tableau
 *           avant la partition.
 * Sorties : ind_pivot, l'indice auquel se trouve le pivot dans le tableau
 *           après la partition.
 */
void partition(int *tableau, int ind_debut, int ind_fin, int *ind_pivot)
{
  for (int i = ind_debut; i < ind_fin; i++)
  {
    if (tableau[i] > tableau[*ind_pivot] && *ind_pivot > i || tableau[i] < tableau[*ind_pivot] && *ind_pivot < i)
    {
      echange(tableau, i, *ind_pivot);
      *ind_pivot++;
    }
  }
}

/* Tri une partie d'un tableau en appliquant le tri rapide.
 * Entrées : tableau   un pointeur vers un tableau d'entier
 *           ind_debut indice du début de la partie à trier
 *           ind_fin   indice de la fin de la partie à trier (borne non
 *           atteinte)
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_rapide_rec(int *tableau, int ind_debut, int ind_fin)
{
  if (ind_debut < ind_fin)
  {
    int pivot = ((rand() % (ind_fin - ind_debut)) + ind_debut);
    partition(tableau, ind_debut, ind_fin, &pivot);
    tri_rapide_rec(tableau, ind_debut, pivot - 1);
    tri_rapide_rec(tableau, pivot + 1, ind_fin);
  }
}

/* Tri d'un tableau en appliquant le tri rapide.
 * Entrées : tableau un pointeur vers un tableau d'entier
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_rapide(int *tableau, int n)
{
  /* FONCTION A ECRIRE */
}

/* Effectue la fusion des éléments triés contenus dans deux tableaux qui
 * correspondent à deux parties d'un même tableau.
 * Entrées : tab1 un pointeur vers un tableau d'entiers triés
 *           n1 la taille de la partie du tableau pointé par tab1
 *           tab2 un pointeur vers la deuxième partie contenant des entiers
 *           triés
 *           n2 la taille de la partie du tableau pointé par tab2
 * Sortie : les éléments contenus par le tableau pointé par tab1 et
 *          de taille n1 + n2 sont triés en ordre croissant
 */
void fusion(int *tab1, int n1, int *tab2, int n2, int *tmp)
{
  int index = 0;
  while (index < n1 && index < n2)
  {
    if (tab1[index] <= tab2[index])
    {
      tmp[2 * index] = tab1[index];
      tmp[2 * index + 1] = tab2[index];
    }
    else
    {
      tmp[2 * index] = tab2[index];
      tmp[2 * index + 1] = tab1[index];
    };
    index++;
  }
  if (index = n1)
  {
  }
}

/* Tri d'un tableau en appliquant le tri fusion.
 * Entrées : tableau un pointeur vers un tableau d'entier
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_fusion(int *tableau, int n)
{
  /* SEULEMENT S IL VOUS RESTE DU TEMPS */
}

/// @brief Implémentation du tri par bulle
/// @param tableau le tableau à trier
/// @param n le nombre d'éléments du tableau
void tri_bubulle(int *tableau, int n)
{
  while (!est_croissant(tableau, n))
  {
    for (int i = 1; i < n; i++)
    {
      if (tableau[i] < tableau[i - 1])
      {
        int swap; // Variable temporaire pour l'échange
        /* Echange des éléments du tableau */
        swap = tableau[i];
        tableau[i] = tableau[i - 1];
        tableau[i - 1] = swap;
      }
    }
  };
}
