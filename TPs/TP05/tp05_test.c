/*************************************************************************
 *      Tests de tris de tableaux d'éléments
 *
 *
 *************************************************************************/
#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "tp05_outils.h"
#include "tp05_tris.h"

/* Lit les arguments et met à jour le nombre d'itérations, la taille max des
 * tableaux à utiliser et éventuellement le nom du fichier.
 * Entrées : argc, argv les arguments de la fonction mais
 * Sorties : mise à jour appropriée du contenu du pointeur vers nb_iterations.
 */
void lire_arguments(char **argv, int *taille_max)
{
  char *str;
  str = argv[1];
  *taille_max = atoi(str);

  if (*taille_max > 10000)
  {
    *taille_max = 10000;
  }
  if (*taille_max < 8)
  {
    *taille_max = 8;
  }
}

void afficher_usage()
{
  printf("Utilisation : ./tp05_test taille_max\n");
  printf("taille_max    est un entier donnant la taille max sur laquelle effectuer les tests\n");
}

int main(int argc, char **argv)
{
  int taille_max;

  // vérification du nombre d'arguments
  if (argc != 2)
  {
    afficher_usage();
    exit(EXIT_FAILURE);
  }

  // lecture des arguments
  lire_arguments(argv, &taille_max);

  printf("Tests du programme jusqu'à %d éléments\n", taille_max);

  /* allocation des tableaux contenant les donnees */
  int *tableau_entree = malloc(taille_max * sizeof(int));
  if (tableau_entree == NULL)
  {
    fprintf(stderr, "tableau_entree pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_qsort = malloc(taille_max * sizeof(int));
  if (tableau_tri_qsort == NULL)
  {
    fprintf(stderr, "tableau_tri_qsort pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_ins = malloc(taille_max * sizeof(int));
  if (tableau_tri_ins == NULL)
  {
    fprintf(stderr, "tableau_tri_ins pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_sel = malloc(taille_max * sizeof(int));
  if (tableau_tri_sel == NULL)
  {
    fprintf(stderr, "tableau_tri_sel pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_rapide = malloc(taille_max * sizeof(int));
  if (tableau_tri_rapide == NULL)
  {
    fprintf(stderr, "tableau_tri_rapide pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  int *tableau_tri_bulle = malloc(taille_max * sizeof(int));
  if (tableau_tri_bulle == NULL)
  {
    fprintf(stderr, "tableau_tri_bulle pointer is NULL\n");
    exit(EXIT_FAILURE);
  }

  struct timespec temps_debut, temps_fin;

  int nb_elements = 8;
  while (nb_elements <= taille_max)
  {
    printf("Traitement du cas avec %d eléments \n", nb_elements);
    random_fill(tableau_entree, nb_elements);

    /* Tri bulle */
    printf("Essai avec tri bulle \n");
    printf("   Début du tableau d'entrée \n    ");
    affiche_tableau(tableau_entree, 8);

    copie_tableau(tableau_entree, nb_elements, tableau_tri_bulle);

    clock_gettime(CLOCK_MONOTONIC, &temps_debut);
    tri_bubulle(tableau_tri_bulle, nb_elements, sizeof(int), comp);
    clock_gettime(CLOCK_MONOTONIC, &temps_fin);

    printf("   Début du tableau obtenu : \n     ");
    affiche_tableau(tableau_tri_bulle, 8);
    assert(est_croissant(tableau_tri_bulle, nb_elements));

    printf("Temps d'éxécution du tri par bulle pour %d éléments : %.12g ns\n", nb_elements,
           difference_temps(&temps_debut, &temps_fin));

    /* Quick sort */
    printf("Essai avec qsort \n");
    printf("   Début du tableau d'entrée \n    ");
    affiche_tableau(tableau_entree, 8);

    copie_tableau(tableau_entree, nb_elements, tableau_tri_qsort);

    clock_gettime(CLOCK_MONOTONIC, &temps_debut);
    qsort(tableau_tri_qsort, nb_elements, sizeof(int), comp);
    clock_gettime(CLOCK_MONOTONIC, &temps_fin);

    printf("   Début du tableau obtenu : \n     ");
    affiche_tableau(tableau_tri_qsort, 8);
    assert(est_croissant(tableau_tri_qsort, nb_elements));

    printf("Temps d'éxécution QuickSort pour %d éléments : %.12g ns\n", nb_elements,
           difference_temps(&temps_debut, &temps_fin));

    /* Tri par insertion */
    printf("Essai avec tri par insertion \n");
    printf("   Début du tableau d'entrée \n    ");
    affiche_tableau(tableau_entree, 8);

    copie_tableau(tableau_entree, nb_elements, tableau_tri_ins);

    clock_gettime(CLOCK_MONOTONIC, &temps_debut);
    tri_insertion(tableau_tri_ins, nb_elements);
    clock_gettime(CLOCK_MONOTONIC, &temps_fin);

    printf("   Début du tableau obtenu : \n     ");
    affiche_tableau(tableau_tri_ins, 8);
    assert(verifie_tri(tableau_tri_qsort, tableau_tri_ins, nb_elements));

    printf("Temps d'éxécution Tri par insertion pour %d éléments : %.12g ns\n", nb_elements,
           difference_temps(&temps_debut, &temps_fin));

    /* Tri par selection */
    printf("Essai avec tri par selection \n");
    printf("   Début du tableau d'entrée \n    ");
    affiche_tableau(tableau_entree, 8);
    copie_tableau(tableau_entree, nb_elements, tableau_tri_sel);

    clock_gettime(CLOCK_MONOTONIC, &temps_debut);
    tri_selection(tableau_tri_sel, nb_elements);
    clock_gettime(CLOCK_MONOTONIC, &temps_fin);

    printf("   Début du tableau obtenu : \n     ");
    affiche_tableau(tableau_tri_sel, 8);
    assert(verifie_tri(tableau_tri_qsort, tableau_tri_sel, nb_elements));

    printf("Temps d'éxécution Tri par selection pour %d éléments : %.12g ns\n", nb_elements,
           difference_temps(&temps_debut, &temps_fin));

    /* Tri rapide */
    printf("Essai avec tri rapide \n");
    printf("   Début du tableau d'entrée :\n    ");
    affiche_tableau(tableau_entree, 8);
    copie_tableau(tableau_entree, nb_elements, tableau_tri_rapide);

    clock_gettime(CLOCK_MONOTONIC, &temps_debut);
    tri_rapide(tableau_tri_rapide, nb_elements);
    clock_gettime(CLOCK_MONOTONIC, &temps_fin);

    printf("   Début du tableau obtenu : \n     ");
    affiche_tableau(tableau_tri_rapide, 8);
    assert(verifie_tri(tableau_tri_qsort, tableau_tri_rapide, nb_elements));

    printf("Temps d'éxécution Tri rapide pour %d éléments : %.12g ns\n", nb_elements,
           difference_temps(&temps_debut, &temps_fin));

    nb_elements = nb_elements * 2;
  }

  free(tableau_tri_rapide);
  free(tableau_tri_sel);
  free(tableau_tri_ins);
  free(tableau_tri_qsort);
  free(tableau_entree);
  free(tableau_tri_bulle);
  return 0;
}
