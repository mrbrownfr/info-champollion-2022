/**************************************************************
 *
 *            Calcul des moyennes et des écarts types
 *
 **************************************************************/



/* Calcule et met à jour la moyenne et l'écart type dans le tableau de résultats fourni.
 * Entrées : valeurs     un tableau de double
 *           nb          la taille du tableau
 * Sortie :  resultat    un tableau de deux cases est mis à jour avec la moyenne
 *                       puis l'écart type.
 */
void calcule_stats(const double *valeurs, int nb, double *resultat);
