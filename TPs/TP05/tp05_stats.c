#include <math.h>
#include "tp05_stats.h"

/* Calcule la moyenne de nb_valeurs valeurs en commençant à l'offset 0.
 * Le tableau de valeurs doit contenir nb_valeurs + offset éléments.
 * Entrées : valeurs    un tableau de double
 *           nb_valeurs le nombre de valeurs à prendre en compte
 *           offset     l'indice de la valeur à partir de laquelle commencer
 * Sortie  :  la moyenne des valeurs considérées
 */
double moyenne(const double *valeurs, int nb_valeurs, int offset)
{
  double sum = 0;
  for(int i = offset; i < nb_valeurs + offset; i++)
  {
    sum += valeurs[i];
  }

  return sum/(double)nb_valeurs;
}


/* Calcule l'écart type comme la moyenne des écarts à la moyenne.
 * Le tableau de valeurs doit contenir nb_valeurs + offset éléments.
 * Entrées : valeurs    un tableu de double
 *           nb_valeurs le nombre de valeurs à prendre en compte
 *           moyenne    la moyenne des valeurs des éléments
 *           offset     l'indice de la valeur à partir de laquelle commencer
 * Sortie : l'écart type
 */
double ecart_type(const double *valeurs, int nb_valeurs, double moyenne, int offset)
{
  double sum = 0;
  for(int i = offset; i < nb_valeurs + offset; i++)
  {
    sum += (valeurs[i] - moyenne)*(valeurs[i] - moyenne);
  }

  return sqrt(sum/(double)(nb_valeurs - 1));
}


void calcule_stats(const double *valeurs, int nb, double *resultat)
{
  if (nb >= 2)
  {
    // on calcule moyenne et écart type sur toutes les valeurs obtenues sauf la
    // première
    resultat[0] = moyenne(valeurs, nb - 1, 1);
    resultat[1] = ecart_type(valeurs, nb - 1, resultat[0], 1);
  }
  else
  { // ce cas ne devrait pas se produire
    resultat[0] = 0;
    resultat[1] = 0;
  }
}
