#include <time.h>
#include <stdbool.h>

/* Fonction de comparaison utilisée par qsort.
 */
int comp(const void *a, const void *b);

/* Renvoie la difference de temps en nanosecondes entre deux structures de type
 * timespec initialisées avec la fonction clock_gettime().
 * Entrées : start, end deux pointeurs vers des structures timespec contenant
 *           une représentation de temps.
 * Sortie : la fonction renvoie un double représentant le temps en nanosecondes
 *          écoulé entre les deux temps.
 */
double difference_temps(const struct timespec *start, const struct timespec *end);

/* Détermine le nombre de tailles différentes à tester.
 * Entrées : tailles     un tableau contenant différents nombre d'éléments
 *           nb		 la taille du tableau tailles (!)
 *           taille_max  la taille max souhaitée pour cette XP
 */
int determiner_nombre_tailles(const int *tailles, const int nb, const int taille_max);

/* Remplit un tableau avec des valeurs aléatoires.
 * Entrées : tableau   un pointeur vers un tableau d'entiers
 *           nb        le nombre d'éléments du tableau
 * Sorties : les valeurs de tableau sont mises à jour et contiennent un nombre
 *           aléatoire entre 0 (inclus) et nb * 10 (exclus).
 */
void random_fill(int *tableau, int nb);

/* Affiche les nb premières valeurs d'un tableau.
 * Entrees : tableau	pointeur vers un tableau d'entier
 *           nb         nombre d'éléments du tableau.
 * Sortie : pas de valeur de retour, affichage des valeurs du tableau.
 */
void affiche_tableau(const int *tableau, int nb);

/* Copie les éléments d'un tableau dans un autre tableau.
 * Entrées : tableau   un pointeur vers un tableau d'entiers
 *           nb        le nombre d'éléments du tableau
 * Sortie : les valeurs du tableau copie sont modifiées et sont les mêmes que
 *          celles du tableau tableau.
 */
void copie_tableau(const int *tableau, int nb, int *copie);

/* Vérifie si les éléments d'un tableau sont dans l'ordre croissant.
 * Entrées : tableau   un pointeur vers un tableau d'entiers
 *           nb        le nombre d'éléments du tableau.
 * Sortie :  true      si les nb éléments du tableau sont en ordre croissant
 *           false     sinon
 */
bool est_croissant(const int *tableau, int nb);

/* Fonction vérifiant si les éléments du tableau sont dans le même ordre que
 * ceux du tableau de reference.
 * Entrées : reference   pointeur vers les éléments du tableau de référence
 *           tableau     pointeur vers les éléments du tableau à vérifier
 *           nb_elements nombre d'éléments des tableaux
 * Sortie : true si les deux tableaux contiennent les mêmes éléments
 *          false sinon
 */
bool verifie_tri(const int *reference, const int *tableau, int nb);
