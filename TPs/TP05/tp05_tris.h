/*************************************************************************
 *       Implémentation d'algorithmes de tris.
 *
 *
 *************************************************************************/

/* Tri d'un tableau en appliquant le tri par sélection.
 * Entrées : tableau un pointeur vers un tableau d'entiers
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_selection(int *tableau, int n);

/* Tri d'un tableau en appliquant le tri par insertion.
 * Entrées : tableau un pointeur vers un tableau d'entiers
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_insertion(int *tableau, int n);

/* Tri d'un tableau en appliquant le tri rapide.
 * Entrées : tableau un pointeur vers un tableau d'entier
 *           n la taille du tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_rapide(int *tableau, int n);

/* Effectue la fusion des éléments triés contenus dans deux tableaux qui
 * correspondent à deux parties d'un même tableau.
 * Entrées : tab1 un pointeur vers un tableau d'entiers triés
 *           n1 la taille de la partie du tableau pointé par tab1
 *           tab2 un pointeur vers la deuxième partie contenant des entiers
 *           triés
 *           n2 la taille de la partie du tableau pointé par tab2
 *           tmp  un pointeur vers un tableau de taille n1 + n2 à utiliser pour
 *                les fusions
 * Sortie : les éléments contenus par le tableau pointé par tab1 et
 *          de taille n1 + n2 sont triés en ordre croissant
 */
void fusion(int *tab1, int n1, int *tab2, int n2, int *tmp);


/* Tri d'un tableau en appliquant le tri fusion.
 * Entrées : tableau un pointeur vers un tableau d'entier
 *           n       la taille du tableau
 *           tmp     un pointeur vers un tableau d'entiers de la même taille que
 *                   tableau
 * En sortie, pas de valeur de retour, le tableau contient les éléments du
 * tableau initial dans l'ordre croissant.
 */
void tri_fusion(int *tableau, int n, int *tmp);
