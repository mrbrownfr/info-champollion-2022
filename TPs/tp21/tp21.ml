(* Question 1 *)
(* Les deux paramètres de type ('a, 'b) correspondent
   au type des clés et des valeurs du dictionnaire *)

(* Question 2 *)
(* C'est parce que les valeurs ne sont pas uniques alors que les clés si *)

(* Question 3 *)
(* Les clés sont uniques, elles peuvent donc correspondre aux éléments de l'ensemble.
   Les valeurs n'ont alors aucune utilité. *)

type 'a abr = Vide | Noeud of 'a abr * 'a * 'a abr

(* Question 4 *)
let arbre =
  Noeud
    ( Noeud (Noeud (Vide, 2, Vide), 3, Noeud (Vide, 5, Vide)),
      5,
      Noeud (Vide, 7, Noeud (Vide, 8, Vide)) )

(* Question 5 *)
(* Un parcours infixe permet d'afficher toutes les étiquettes de l'arbre dans l'ordre croissant *)

(* Question 6 *)
let rec liste arbre =
  match arbre with Vide -> [] | Noeud (g, v, d) -> liste g @ (v :: liste d)

let rec print_liste l =
  match l with
  | [] -> ()
  | e :: q ->
      Printf.printf "%d, " e;
      print_liste q
;;

liste arbre |> print_liste

(* Question 7 *)
(* contre-exemple *)

(* Question 8 *)
let est_abr arbre =
  let rec est_croissant l =
    match l with
    | [] | [ _ ] -> true
    | a :: b :: q -> a <= b && (est_croissant @@ (b :: q))
  in
  liste arbre |> est_croissant

(* Question 9 *)
let rec min_elt arbre =
  match arbre with
  | Vide -> None
  | Noeud (Vide, v, _) -> Some v
  | Noeud (g, _, _) -> min_elt g

type 'a ensemble = 'a abr

let vide : 'a ensemble = Vide

(* Question 10 *)
let rec cardinal e =
  match e with Vide -> 0 | Noeud (g, _, d) -> cardinal g + 1 + cardinal d

(* Sa complexité est O(n) *)

(* Question 11 *)
let rec appartient elem ens =
  match ens with
  | Vide -> false
  | Noeud (g, v, d) ->
      v = elem || if elem < v then appartient elem g else appartient elem d

(* Dans le pire des cas, l'élément qu'on recherche ne se trouve pas dans l'arbre ou alors
   est une feuille de profondeur maximale *)
(* La complexité est O(log n) meilleur qu'une complexité de O(n) dans une liste *)

(* Question 12 *)
let rec ajouter elem ens =
  match ens with
  | Vide -> Noeud (Vide, elem, Vide)
  | Noeud (g, v, d) ->
      if elem <= v then Noeud (ajouter elem g, v, d)
      else Noeud (g, v, ajouter elem d)

(* La complexité est O(log n) *)

(* Question 15 *)
let rec supprime elem ens =
  (* Question 14 *)
  let rec supprime_min_elt arbre =
    match arbre with
    | Vide -> (None, Vide)
    | Noeud (Vide, v, d) -> (Some v, d)
    | Noeud (g, v, d) ->
        let m, a = supprime_min_elt g in
        (m, Noeud (a, v, d))
  in
  match ens with
  | Vide -> Vide
  | Noeud (g, v, d) ->
      if elem = v then
        let m, a = supprime_min_elt d in
        match m with None -> Vide | Some e -> Noeud (g, e, a)
      else if elem < v then Noeud (supprime elem g, v, d)
      else Noeud (g, v, supprime elem d)
;;

print_newline ();
supprime 5 arbre |> liste |> print_liste

(* Sa complexité est *)

type ('a, 'b) dictionnaire =
  | Vide
  | Noeud of ('a, 'b) dictionnaire * 'a * 'b * ('a, 'b) dictionnaire

(* Question 16 *)

let vide = Vide

let rec ajoute cle valeur dict =
  match dict with
  | Vide -> Noeud (Vide, cle, valeur, Vide)
  | Noeud (g, c, v, d) ->
      if cle <= c then Noeud (ajoute cle valeur g, c, v, d)
      else Noeud (g, c, v, ajoute cle valeur d)

let rec appartient cle dict =
  match dict with
  | Vide -> false
  | Noeud (g, c, _, d) ->
      c = cle || if cle < c then appartient cle g else appartient cle d

let rec trouve cle dict =
  match dict with
  | Vide -> None
  | Noeud (g, c, v, d) ->
      if c = cle then Some v else if cle < c then trouve cle g else trouve cle d

let rec supprime elem ens =
  let rec supprime_min_elt arbre =
    match arbre with
    | Vide -> (None, Vide)
    | Noeud (Vide, c, v, d) -> (Some (c, v), d)
    | Noeud (g, c, v, d) ->
        let m, a = supprime_min_elt g in
        (m, Noeud (a, c, v, d))
  in
  match ens with
  | Vide -> Vide
  | Noeud (g, c, v, d) ->
      if elem = c then
        let m, a = supprime_min_elt d in
        match m with
        | None -> Vide
        | Some (min_c, min_v) -> Noeud (g, min_c, min_v, a)
      else if elem < c then Noeud (supprime elem g, c, v, d)
      else Noeud (g, c, v, supprime elem d)

let rec cardinal dict =
  match dict with
  | Vide -> 0
  | Noeud (g, _, _, d) -> 1 + cardinal g + cardinal d
