#include <stdlib.h>
#include <stdio.h>
#include "outils.h"

int compare(const void *a, const void *b)
{
  int int_a = *((int *)a);
  int int_b = *((int *)b);

  // an easy expression for comparing
  return (int_a < int_b) - (int_a > int_b);
}

void sort(int *tab, int nb)
{

  qsort(tab, nb, sizeof(int), compare);
}

int hauteur_pile(int *pile, int nb)
{
  int res = 0;
  for (int i = 0; i < nb; i++)
  {
    res += pile[i];
  }
  return res;
}

int *intdup(int const *src, size_t len)
{
  int *p = malloc(len * sizeof(int));
  memcpy(p, src, len * sizeof(int));
  return p;
}

int *lecture_fichier(char *nom_fichier, int *nb)
{
  FILE *f = fopen(nom_fichier, "r");
  int *serie = NULL;
  *nb = 0;
  fscanf(f, "%d\n", nb);
  if (*nb != 0)
  {
    serie = (int *)malloc(sizeof(int) * (*nb));
    if (serie == NULL)
    {
      exit(EXIT_FAILURE);
    }

    for (int i = 0; i < *nb; i++)
    {
      fscanf(f, "%d\n", &(serie[i]));
    }
  }

  fclose(f);
  return serie;
}

void affichage_serie(int *serie, int nb)
{
  printf("Les éléments à répartir sont \n [ ");
  for (int i = 0; i < nb; i++)
  {
    printf("%d ", serie[i]);
  }
  printf("]\n");
}

void affichage_pile(int *pile, int nb)
{
  printf(" - ");
  for (int i = 0; i < nb; i++)
  {
    printf("%d - ", pile[i]);
  }
  printf("\n");
}

void affichage_resultat(int *pile1, int nb1, int *pile2, int nb2)
{
  printf("La pile 1 contient\n    ");
  affichage_pile(pile1, nb1);
  printf("La pile 2 contient\n    ");
  affichage_pile(pile2, nb2);
  printf("\n");
}
