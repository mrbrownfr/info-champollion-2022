
void sort(int *tab, int nb);

int hauteur_pile(int *pile, int nb);

int *intdup(int const *src, size_t len);

int *lecture_fichier(char *nom_fichier, int *nb);

void affichage_serie(int *serie, int nb);

void affichage_pile(int *pile, int nb);

void affichage_resultat(int *pile1, int nb1, int *pile2, int nb2);
