#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include "outils.h"

/** Détermine une distribution des éléments en utilisant l'algorithme glouton.
 * Entrées : serie     un tableau contenant nb entiers
 *           nb        nombre d'éléments du tableau serie
 *           pile1     un tableau pouvant contenir jusqu'à nb entiers
 *           pile2     un tableau pouvant contenir jusqu'à nb entiers
 * Sortie  : pile1     un tableau contenant les nb1 entiers choisis pour figurer
 *                     dans la pile1
 *           nb1       pointeur vers le nombre d'éléments affectés à la pile 1
 *           pile2     un tableau contenant les nb2 entiers choisis pour figurer
 *                     dans la pile2
 *           nb2       pointeur vers le nombre d'éléments affectés à la pile 2
 */
void pilzegal_glouton(int *serie, int nb, int *pile1, int *nb1, int *pile2, int *nb2)
{
  int *sorted_serie = intdup(serie, nb);
  sort(sorted_serie, nb);
  for (int i = 0; i < nb; i++)
  {
    if (hauteur_pile(pile1, *nb1) <= hauteur_pile(pile2, *nb2))
    {
      pile1[*nb1] = sorted_serie[i];
      (*nb1)++;
    }
    else
    {
      pile2[*nb2] = sorted_serie[i];
      (*nb2)++;
    }
  }
  free(sorted_serie);
}

/** Détermine une distribution des éléments en utilisant la stratégie de
 * programmation dynamique.
 * Entrées : serie     un tableau contenant nb entiers
 *           nb        nombre d'éléments du tableau serie
 *           pile1     un tableau pouvant contenir jusqu'à nb entiers
 *           pile2     un tableau pouvant contenir jusqu'à nb entiers
 * Sortie  : pile1     un tableau contenant les nb1 entiers choisis pour figurer
 *                     dans la pile1
 *           nb1       pointeur vers le nombre d'éléments affectés à la pile 1
 *           pile2     un tableau contenant les nb2 entiers choisis pour figurer
 *                     dans la pile2
 *           nb2       pointeur vers le nombre d'éléments affectés à la pile 2
 */
void pilzegal_progdyn(int *serie, int nb, int *pile1, int *nb1, int *pile2, int *nb2)
{
  assert(hauteur_pile(serie, nb) % 2 == 0);
  int moitie = hauteur_pile(serie, nb) / 2;
  int *regle = (int *)malloc(sizeof(int) * moitie + 1);
  if (regle == NULL)
  {
    exit(EXIT_FAILURE);
  }
  for (int i = 1; i < moitie + 1; i++)
  {
    regle[i] = -1;
  }
  regle[0] = 0;

  for (int i = 0; i < nb; i++)
  {
    int elem = serie[i];
    // affichage_pile(regle, moitie + 1);
    // printf("\n%d", elem);
    for (int j = moitie; j >= 0; j--)
    {
      if (regle[j] == -1 || j + elem > moitie)
      {
        continue;
      }

      if (regle[j + elem] == -1)
      {
        regle[j + elem] = i;
      }
    }
    if (regle[moitie] != -1)
    {
      break;
    }
  }
  regle[0] = -1;
  // affichage_pile(regle, moitie + 1);
  int compteur = moitie;
  while (compteur != 0)
  {
    pile1[*nb1] = serie[regle[compteur]];
    (*nb1)++;
    int temp = serie[regle[compteur]];
    serie[regle[compteur]] = -1;
    compteur = compteur - temp;
  }
  for (int i = 0; i < nb; i++)
  {
    if (serie[i] != -1)
    {
      pile2[*nb2] = serie[i];
      (*nb2)++;
    }
  }
}

/** Détermine une distribution des éléments en utilisant une stratégie de recherche exhaustive.
 * Entrées : serie     un tableau contenant nb entiers
 *           nb        nombre d'éléments du tableau serie
 *           pile1     un tableau pouvant contenir jusqu'à nb entiers
 *           pile2     un tableau pouvant contenir jusqu'à nb entiers
 * Sortie  : pile1     un tableau contenant les nb1 entiers choisis pour figurer
 *                     dans la pile1
 *           nb1       pointeur vers nombre d'éléments affectés à la pile 1
 *           pile2     un tableau contenant les nb2 entiers choisis pour figurer
 *                     dans la pile2
 *           nb2       pointeur vers nombre d'éléments affectés à la pile 2
 */
void pilzegal_exhaustif(int *serie, int nb, int *pile1, int *nb1, int *pile2, int *nb2)
{
  /* code à écrire */
}

int main(int argc, char *argv[])
{
  char nom_fichier[256] = "serie01.txt";
  if (argc == 2)
  {
    strncpy(nom_fichier, argv[1], 255);
    nom_fichier[255] = '\0';
  }

  /* Initialisation des structures */
  int nb;
  int *serie = lecture_fichier(nom_fichier, &nb);

  int *pile1 = (int *)malloc(sizeof(int) * (nb));
  if (pile1 == NULL)
  {
    free(serie);
    exit(EXIT_FAILURE);
  }

  int *pile2 = (int *)malloc(sizeof(int) * (nb));
  if (pile2 == NULL)
  {
    free(serie);
    free(pile1);
    exit(EXIT_FAILURE);
  }

  int nb1 = 0;
  int nb2 = 0;

  /* Affichage de l'instance */
  affichage_serie(serie, nb);

  /* Utilisation de l'algorithme glouton */
  printf("\n\nAlgorithme glouton\n");
  pilzegal_glouton(serie, nb, pile1, &nb1, pile2, &nb2);

  affichage_resultat(pile1, nb1, pile2, nb2);

  /* Stratégie de programmation dynamique */
  nb1 = 0;
  nb2 = 0;
  printf("\n\nStratégie en programmation dynamique \n");
  pilzegal_progdyn(serie, nb, pile1, &nb1, pile2, &nb2);

  affichage_resultat(pile1, nb1, pile2, nb2);

  /* Recherche exhaustive */
  nb1 = 0;
  nb2 = 0;
  printf("\n\nRecherche exhaustive\n");
  pilzegal_exhaustif(serie, nb, pile1, &nb1, pile2, &nb2);

  affichage_resultat(pile1, nb1, pile2, nb2);

  /* Libération de la mémoire associée aux structures */
  free(pile2);
  free(pile1);
  free(serie);

  return 0;
}
