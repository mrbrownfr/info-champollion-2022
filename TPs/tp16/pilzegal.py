### Squelette de code pour résoudre le problème Pilzegal
### 

def pilzegal_somme(liste):
    return sum(liste)


def pilzegal_glouton(liste):
    """ Essaie de renvoyer deux listes qui contiennent les pièces à utiliser dans
    chacune des piles pour qu'elles aient la même taille.
        Entrées : une liste d'entiers > 0.
        Sortie : un couple de deux listes d'entiers. Les piles renvoyees peuvent
                 ne pas avoir la meme taille. Soit que ce ne soit pas du tout
                 possible, soit que l'algorithme n'ait pas trouve la solution."""
    pass


def pilzegal_progdyn(liste):
    """ Renvoie deux listes qui contiennent les pièces à utiliser dans
    chacune des piles pour qu'elles aient la même taille.
        Entrées : une liste d'entiers > 0.
        Sortie : un couple de deux listes d'entiers si c'est possible, None sinon."""
    pass


def pilzegal_exhaustif(liste):
    """ Renvoie deux listes qui contiennent les pièces à utiliser dans
    chacune des piles pour qu'elles aient la même taille.
        Entrées : une liste d'entiers > 0.
        Sortie : un couple de deux listes d'entiers si c'est possible, None sinon."""
    pass







def valider_piles(piles):
    """Renvoie True si la somme des elements de la pile1 est egale à la somme
       des elements de la pile2.
       Entree : un couple de piles."""

    assert len(piles) > 0
    somme1 = pilzegal_somme(piles[0])
    somme2 = pilzegal_somme(piles[1])

    return somme1 == somme2



# listes a tester
liste1 = [29, 23, 22, 21, 20, 13, 16, 13, 10, 7]

liste_a = [4,5,9]
liste_b = [4,6,7,9]
liste_c = [4,5,5,6]

liste_d = [4,4,5,9,10,14]
liste_e = [7,8,10,11,13,15]
liste_f = [4,5,7,8,10,11,12,13]

liste_g = [4,5,7,9,10,11,12]
liste_h = [5,7,10,10,11,14,16,25]
liste_i = [4,6,10,11,13,15,19,23,24,25,30]

liste_i2 = [6,10,14,17,20,24,29]

liste_j = [4,5,5,7,11,15,17,24,28]
liste_k = [5,7,8,10,12,15,17,20,22,30]

print("******************************************")
print("** Test du glouton                      **")
print("******************************************")
print("Test de le liste a", liste_a)
print("   => ", valider_piles(pilzegal_glouton(liste_a)))
print("Test de le liste b", liste_b)
print("   => ", valider_piles(pilzegal_glouton(liste_b)))
print("Test de le liste c", liste_c)
print("   => ", valider_piles(pilzegal_glouton(liste_c)))
print("Test de le liste d", liste_d)
print("   => ", valider_piles(pilzegal_glouton(liste_d)))
print("Test de le liste e", liste_e)
print("   => ", valider_piles(pilzegal_glouton(liste_e)))
print("Test de le liste f", liste_f)
print("   => ", valider_piles(pilzegal_glouton(liste_f)))
print("A partir du g, on s'attend a ce que le glouton ne fonctionne plus")
print("Test de le liste g", liste_g)
print("   => ", valider_piles(pilzegal_glouton(liste_g)))
print("Test de le liste h", liste_h)
print("   => ", valider_piles(pilzegal_glouton(liste_h)))
print("Test de le liste i", liste_i)
print("   => ", valider_piles(pilzegal_glouton(liste_i)))
print("Test de le liste j", liste_j)
print("   => ", valider_piles(pilzegal_glouton(liste_j)))
print("Test de le liste k", liste_k)
print("   => ", valider_piles(pilzegal_glouton(liste_k)))
print("Test de le liste i2", liste_i2)
print("   => ", valider_piles(pilzegal_glouton(liste_i2)))

# print("******************************************")
# print("** Test du code programmation dynamique **")
# print("******************************************")
# print("Test de la liste ", liste_a)
# assert valider_piles(pilzegal_progdyn(liste_a))
# print("Test de la liste ", liste_b)
# assert valider_piles(pilzegal_progdyn(liste_b))
# print("Test de la liste ", liste_c)
# assert valider_piles(pilzegal_progdyn(liste_c))
# print("Test de la liste ", liste1)
# assert valider_piles(pilzegal_progdyn(liste1))
# print("Test de la liste ", liste_d)
# assert valider_piles(pilzegal_progdyn(liste_d))
# print("Test de la liste ", liste_e)
# assert valider_piles(pilzegal_progdyn(liste_e))
# print("Test de la liste ", liste_f)
# assert valider_piles(pilzegal_progdyn(liste_f))
# print("Test de la liste ", liste_g)
# assert valider_piles(pilzegal_progdyn(liste_g))
# print("Test de la liste ", liste_h)
# assert valider_piles(pilzegal_progdyn(liste_h))
# print("Test de la liste ", liste_i)
# assert valider_piles(pilzegal_progdyn(liste_i))
# print("Test de la liste ", liste_i2)
# assert valider_piles(pilzegal_progdyn(liste_i2))
# print("Test de la liste ", liste_j)
# assert valider_piles(pilzegal_progdyn(liste_j))
# print("Test de la liste ", liste_k)
# assert valider_piles(pilzegal_progdyn(liste_k))
# 
# print("******************************************")
# print("** Test du code recherche exhaustive    **")
# print("******************************************")
# print("Test de la liste ", liste_a)
# assert valider_piles(pilzegal_exhaustif(liste_a))
# print("Test de la liste ", liste_b)
# assert valider_piles(pilzegal_exhaustif(liste_b))
# print("Test de la liste ", liste_c)
# assert valider_piles(pilzegal_exhaustif(liste_c))
# print("Test de la liste ", liste1)
# assert valider_piles(pilzegal_exhaustif(liste1))
# print("Test de la liste ", liste_d)
# assert valider_piles(pilzegal_exhaustif(liste_d))
# print("Test de la liste ", liste_e)
# assert valider_piles(pilzegal_exhaustif(liste_e))
# print("Test de la liste ", liste_f)
# assert valider_piles(pilzegal_exhaustif(liste_f))
# print("Test de la liste ", liste_g)
# assert valider_piles(pilzegal_exhaustif(liste_g))
# print("Test de la liste ", liste_h)
# assert valider_piles(pilzegal_exhaustif(liste_h))
# print("Test de la liste ", liste_i)
# assert valider_piles(pilzegal_exhaustif(liste_i))
# print("Test de la liste ", liste_i2)
# assert valider_piles(pilzegal_exhaustif(liste_i2))
# print("Test de la liste ", liste_j)
# assert valider_piles(pilzegal_exhaustif(liste_j))
# print("Test de la liste ", liste_k)
# assert valider_piles(pilzegal_exhaustif(liste_k))

