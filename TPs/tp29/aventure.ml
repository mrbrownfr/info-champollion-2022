(* Partie 2 *)
(* Définitions utile pour la mission *)
type pixel = int * int * int

let p = (0, 0, 0)
let montagne = (139, 70, 39)
let prairie = (0, 134, 0)
let difficile = (7, 57, 7)
let lac = (56, 126, 170)

(* Renvoie une matrice représentant le terrain et les dimensions de la matrice :
  hauteur, largeur (nombre de lignes, nombre de colonnes) *)
let lecture_terrain nom_fichier = 
  let fc = open_in nom_fichier in
  let _, _ = input_line fc, input_line fc in
  let dimensions = input_line fc in
  let _ = input_line fc in
  let largeur, hauteur = 
  match String.split_on_char ' ' dimensions with
  | [l; h] -> int_of_string l, int_of_string h
  | _ -> failwith "Format incorrect"
  in 
  let g = Array.make_matrix hauteur largeur p in
  for i = 0 to hauteur -1 do
    for j = 0 to largeur -1 do
      let r = int_of_string (input_line fc) in
      let v = int_of_string (input_line fc) in
      let b = int_of_string (input_line fc) in
      g.(i).(j) <- (r, v, b);
    done;
  done; 
  close_in fc;
  g, hauteur, largeur

  

