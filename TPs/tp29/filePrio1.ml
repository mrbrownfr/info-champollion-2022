(* File de priorité min impérative *)
(* Interface donnée dans le fichier .mli *)


(* Représentation d'un tas par un enregistrement.
   Le champ nb permet de conserver le nombre d'éléments valides dans le tas.
   Le champ prio est un tableau qui contient la priorité du sommet stocké à un
   indice donné dans le tas.
   Le champ sommets est un tableau qui contient les numéros des sommets stockés
   dans le tas.
   Le champ positions est un tableau qui contient pour chaque numéro de sommet
   sa position dans le tas.
*)
type fileprio =
  {mutable nb : int;
   prio : float array;
   sommets : int array;
   positions : int array;}

exception FilePleine
exception FileVide
exception ElementAbsent



(* Fonctions utiles *)
let igauche i = 2*i +1
let idroit i = 2*i + 2
let iparent i = (i-1)/2

let swap tab i j =
  let tmp = tab.(i) in
  tab.(i) <- tab.(j);
  tab.(j) <- tmp

let echange fp i j =
  swap fp.prio i j;
  swap fp.sommets i j;
  swap fp.positions fp.sommets.(i) fp.sommets.(j)

let rec remontee fp i =
  let j = iparent i in
  if i > 0 && fp.prio.(i) < fp.prio.(j)  then
    begin
      echange fp i j;
      remontee fp j
    end

let rec descente fp i =
  let imin = ref i in
  let ig = igauche i and id = idroit i in
  if ig < fp.nb && fp.prio.(ig) < fp.prio.(i) then
    imin := ig;
  if id < fp.nb && fp.prio.(id) < fp.prio.(!imin) then
    imin := id;
  if !imin <> i then (
    echange fp i (!imin);
    descente fp (!imin)
  )


(* Fonctions de l'interface *)

let creation n =
  {nb = 0; 
   prio = Array.make n 0.0;
   sommets = Array.make n (-1);
   positions = Array.make n (-1);
  }

let est_vide fp = fp.nb = 0

let ajout fp (e, p) = 
  if fp.nb >= Array.length fp.prio then
    raise FilePleine;
  fp.prio.(fp.nb) <- p;
  fp.sommets.(fp.nb) <- e;
  fp.positions.(e) <- fp.nb;
  fp.nb <- fp.nb + 1;
  remontee fp (fp.nb-1)

let consultation_min fp = 
  if fp.nb <= 0 then raise FileVide;
  fp.sommets.(0)

let extraction_min fp = 
  if fp.nb <= 0 then raise FileVide;
  let elt_min = fp.sommets.(0) in
    if fp.nb > 1 then (
      echange fp 0 (fp.nb - 1);
      descente fp 0
      );
  fp.positions.(elt_min) <- -1;
  fp.nb <- fp.nb - 1;
  elt_min

let diminuer_cle fp e p =
  if fp.positions.(e) = -1 then
    raise ElementAbsent;
  let i = fp.positions.(e) in
  (* assert (fp.prio.(i) > p); *)
  fp.prio.(i) <- p;
  remontee fp i


