open FilePrio1
open Aventure

let pp_int_array outchan arr =
  let n = Array.length arr in
  Printf.fprintf outchan "[|";
  for i = 0 to n - 2 do
    Printf.fprintf outchan "%d; " arr.(i)
  done;
  if n <> 0 then Printf.fprintf outchan "%d" arr.(n - 1);
  Printf.fprintf outchan "|]"

let pp_int_matrix outchan mat =
  let n = Array.length mat in
  Printf.fprintf outchan "[|\n";
  for i = 0 to n - 2 do
    Printf.fprintf outchan "  %a;\n" pp_int_array mat.(i)
  done;
  if n <> 0 then Printf.fprintf outchan "  %a\n" pp_int_array mat.(n - 1);
  Printf.fprintf outchan "|]"

type graph = int array array

let voisins (g : graph) (v : int) =
  List.init (Array.length g) (fun i -> i)
  |> List.filteri (fun j i -> g.(v).(i) <> 0)

let dijkstra (g : graph) (s0 : int) =
  let n = Array.length g in
  let plus_court = Array.make n (-1)
  and pred = Array.make n s0
  and fp = creation n in
  ajout fp (s0, 0.);
  plus_court.(s0) <- 0;
  while not @@ est_vide fp do
    let s = extraction_min fp in
    let d = plus_court.(s) in
    List.iter
      (fun v ->
        if plus_court.(v) = -1 || plus_court.(v) > d + g.(s).(v) then (
          plus_court.(v) <- d + g.(s).(v);
          pred.(v) <- s;
          ajout fp (v, float plus_court.(v))))
      (voisins g s)
  done;
  (plus_court, pred)

let g1 =
  [|
    [| 0; 6; 4; 0; 0 |];
    [| 0; 0; 2; 3; 0 |];
    [| 0; 1; 0; 9; 3 |];
    [| 0; 0; 0; 0; 4 |];
    [| 7; 0; 0; 5; 0 |];
  |]

let () =
  let plus_court, pred = dijkstra g1 0 in
  Printf.printf "%a\n%a\n%a\n" pp_int_matrix g1 pp_int_array plus_court
    pp_int_array pred
