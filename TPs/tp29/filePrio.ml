(* File de priorité min impérative *)
(* Interface donnée dans le fichier .mli *)

(* Représentation d'un tas par un enregistrement.
   Le champ nb permet de conserver le nombre d'éléments valides dans le tas.
   Le champ prio est un tableau qui contient la priorité du sommet stocké à un
   indice donné dans le tas.
   Le champ sommets est un tableau qui contient les numéros des sommets stockés
   dans le tas.
   Le champ positions est un tableau qui contient pour chaque numéro de sommet
   sa position dans le tas.
*)
type fileprio = {
  mutable nb : int;
  prio : float array;
  sommets : int array;
  positions : int array;
}

exception FilePleine
exception FileVide
exception ElementAbsent

(* Question 1 *)
(* Un tas est un arbre binaire équilibré dont tous les niveaux sont rempli sauf éventuellement
   le dernier qui est tassé à gauche *)

(* Question 2 *)
(* L'intérêt d'avoir un nombre maximum d'élément est de pouvoir les stocker dans un
   tableau de taille connue. Ce n'est pas un inconvénient pour Dijkstra car on connait
   à l'avance le nombre de noeuds du graphe. *)

(* Question 3 *)
(* Les éléments deuvent être entier car ils représentent des numéros désignant les sommets *)
(* Tous les éléments doivent avoir une valeur différente pour avoir une priorité différente *)
(* Les valeurs doivent être comprises entre 0 et n-1 car elles servent d'indice pour les tableaux *)

(* Question 4 *)
(*  *)

(* Fonctions de l'interface *)
let creation n =
  {
    nb = 0;
    prio = Array.make n 0.;
    sommets = Array.make n 0;
    positions = Array.make n 0;
  }

let parent i = (i - 1) / 2
let enfant_gauche i = (i * 2) + 1
let enfant_droit i = (i * 2) + 2
let est_vide fp = fp.nb = 0

let swap array i j =
  let tmp = array.(i) in
  array.(i) <- array.(j);
  array.(j) <- tmp

let ajout fp (e, p) =
  if fp.nb = Array.length fp.prio then raise FilePleine;
  let i = ref fp.nb in
  fp.positions.(e) <- !i;
  fp.sommets.(!i) <- e;
  fp.prio.(!i) <- p;
  while !i <> 0 && fp.prio.(!i) < fp.prio.(parent !i) do
    swap fp.prio !i (parent !i);
    swap fp.positions e fp.sommets.(parent !i);
    swap fp.sommets !i (parent !i);
    i := parent !i
  done;
  fp.nb <- fp.nb + 1

let consultation_min fp = if est_vide fp then raise FileVide else fp.sommets.(0)

let rec descendre fp i =
  if i < fp.nb then
    if fp.prio.(i) > fp.prio.(enfant_droit i) then (
      swap fp.prio i (enfant_droit i);
      swap fp.positions fp.sommets.(i) fp.sommets.(enfant_droit i);
      swap fp.sommets i (enfant_droit i);
      descendre fp (enfant_droit i))
    else if fp.prio.(i) > fp.prio.(enfant_gauche i) then (
      swap fp.prio i (enfant_gauche i);
      swap fp.positions fp.sommets.(i) fp.sommets.(enfant_gauche i);
      swap fp.sommets i (enfant_gauche i);
      descendre fp (enfant_gauche i))

let extraction_min fp =
  if est_vide fp then raise FileVide;
  let i = ref 0 in
  swap fp.prio 0 (fp.nb - 1);
  swap fp.positions fp.sommets.(0) fp.sommets.(fp.nb - 1);
  swap fp.sommets 0 (fp.nb - 1);
  descendre fp !i;
  fp.nb <- fp.nb - 1;
  fp.sommets.(fp.nb)

let diminuer_cle fp e p = failwith "to do"

let () =
  let fp = creation 10 in
  ajout fp (1, 2.);
  ajout fp (2, 1.);
  ajout fp (3, 5.);
  ajout fp (4, 1.5);
  Printf.printf "%d\n" (extraction_min fp);
  Printf.printf "%d\n" (extraction_min fp);
  Printf.printf "%d\n" (extraction_min fp);
  Printf.printf "%d\n" (extraction_min fp)
