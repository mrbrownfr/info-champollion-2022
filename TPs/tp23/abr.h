struct noeud
{
    char cle;
    int freq;
    struct noeud *gauche;
    struct noeud *droit;
    struct noeud *parent;
};
typedef struct noeud noeud;

int taille(noeud *n);
noeud *suppression_it(noeud *abr, int value);
void insertion(noeud *abr, char cle, int freq);
noeud *feuille(char cle, int freq);
noeud *recherche(noeud *abr, int cle);