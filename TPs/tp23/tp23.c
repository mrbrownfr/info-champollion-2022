#include <stdlib.h>
#include <stdio.h>
#include "abr.h"
#include "file_priorite.h"

typedef struct arbre_s
{
    char car;
    struct arbre_s *droit;
    struct arbre_s *gauche;
} arbre;

int log2(int nb)
{
    return nb == 0 ? 0 : 1 + log2(nb >> 1);
}

int frequence_car(char *str, char car)
{
    int occ = 0;
    int i = 0;
    while (str[i] != '\0')
    {
        if (str[i] == car)
        {
            occ++;
        }
    }
    return occ;
}

noeud *dictionnaire_frequence(char *str)
{
    noeud *dico = feuille(str[0], frequence_car(str, str[0]));
    int i = 1;
    while (str[i] != '\0')
    {
        insertion(dico, str[i], frequence_car(str, str[i]));
        i++;
    }
    return dico;
}

arbre *construire_arbre(char *str)
{
    noeud *dico = dictionnaire_frequence(str);
    int n = taille(dico);
    file_prio_t *file = creer(n);
    for (int i = 0; i < n; i++)
    {
        arbre *z = malloc(sizeof(arbre));
        z->car = dico->cle;
        dico = suppression_it(dico, dico->cle);
        inserer(file, dico->freq, z);
    }
    for (int i = 0; i < n; i++)
    {
        element_t x = extraire_min(file);
        element_t y = extraire_min(file);
        arbre *z = malloc(sizeof(arbre));
        z->car = '\0';
        z->gauche = x.contenu;
        z->droit = y.contenu;
        inserer(file, x.priorite + y.priorite, z);
    }
    return extraire_min(file).contenu;
}

void remplir_dico(noeud *dico, arbre *huffman, long int path)
{
    if (huffman->car != '\0')
    {
        insertion(dico, huffman->car, path);
    }
    else
    {
        remplir_dico(dico, huffman->droit, (path << 1) + 1);
        remplir_dico(dico, huffman->gauche, path << 1);
    }
}

noeud *dictionnaire_encodage(arbre *huffman)
{
    noeud *dico_int = feuille('\0', (long int)0);
    remplir_dico(dico_int, huffman, 0);
    return dico_int;
}

long int encoder(char *str)
{
    arbre *huffman = construire_arbre(str);
    noeud *dico = dictionnaire_encodage(huffman);
    long int code = 0;
    int i = 0;
    while (str[i] != '\0')
    {
        code = code >> (1 + (int)log2(code));
        code += recherche(dico, str[i])->freq;
        i++;
    }
    return code;
}

int main(int argc, char **argv)
{
    printf("%ld\n", encoder("Hello World !"));
    return 0;
}