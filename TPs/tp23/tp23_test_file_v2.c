/***************************************
 * Test de file à priorité d'entiers.
 *
 * @author M. Durand
 * @date   2023
 * @version 0.1
 ***************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "file_priorite_v2.h"

int main(int argc, char *argv[])
{
  int nb = 30;
  if (argc == 2)
  {
    nb = atoi(argv[1]);
  }

  /* Création d'une file de priorité */
  file_prio_t *fp = creer(nb);

  for (int i = 0; i < nb; i++)
  {
    inserer(fp, i);
  }

  /* Affichage du contenu du tas */
  printf("Contenu du tas \n");
  for (int i = 0; i < fp->taille; i++)
  {
    printf("  Priorite %d\n", fp->tas[i]);
  }

  /* Vérification du bon fonctionnement de l'extraction. Cependant des problèmes
   * ici peuvent signaler des problèmes lors de l'insertion.
   */
  for (int i = 0; i < nb; i++)
  {
    int prio = extraire_min(fp);
    assert(prio == i);
  }

  /* Libération de la file de priorité */
  supprimer(fp);

  return 0;
}
