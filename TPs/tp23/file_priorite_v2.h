/***************************************
 * Interface de file à priorité
 * stockant uniquement des entiers.
 *
 * @author M. Durand
 * @date   2023
 * @version 0.1
 ***************************************/
#include <stdbool.h>

/**
 * Une file à priorité implémentée par un tas pouvant contenir uniquement des
 * entiers (c'est déjà intéressant pour gérer la structure de tas correctement
 * !)
 */
struct file_prio_s
{
  int capacite;
  int taille;
  int *tas;
};

typedef struct file_prio_s file_prio_t;

/**
 * Crée une file à priorité pouvant contenir capacite élements.
 * Entrée : capacite    le nombre d'éléments que pourra contenir la file à
 *                      priorité
 * Sortie : une file à priorité vide.
 */
file_prio_t *creer(int capacite);

/**
 * Détermine si une file à priorité est vide.
 * Entree : file    un pointeur vers une file à priorité.
 * Sortie : true si la file est vide, false sinon.
 */
bool est_vide(file_prio_t *file);

/**
 * Supprime une file à priorité. Libère la mémoire associée à la file de
 * priorité.
 * Entrée : file    un pointeur vers une file à priorité.
 */
void supprimer(file_prio_t *file);

/**
 * Insère un élément, en tenant compte de sa priorité.
 * Entrées : file     un pointeur vers une file à priorité
 *           priorite la priorité de l'élément à insérer
 * Sortie : pas de valeur de retour, la file à priorité est modifiée pour
 *          contenir le nouvel élément.
 */
void inserer(file_prio_t *file, int priorite);

/**
 * Extrait un élément minimum d'une file à priorité.
 * Entrée : file     un pointeur vers une file à priorité
 * Sortie : l'élément dont la valeur de la priorité est minimale dans la file à priorité (élément prioritaire), la file à priorité ne contient plus l'élément.
 */
int extraire_min(file_prio_t *file);

/**
 * Consulte l'élément minimum d'une file à priorité.
 * Entrée : file     un pointeur vers une file à priorité.
 * Sortie : la priorité de l'élément de priorité minimale.
 * L'élément n'est pas extrait.
 */
int minimum(const file_prio_t *file);
