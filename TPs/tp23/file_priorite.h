/***************************************
 * Interface de file à priorité
 * avec contenu.
 *
 * @author M. Durand
 * @date   2023
 * @version 0.1
 ***************************************/
#include <stdbool.h>

/**
 * Type générique pour manipuler des éléments dans une file de priorité.
 *
 */

struct element_s
{
  int priorite;
  void *contenu;
};

typedef struct element_s element_t;

/**
 * Donne la priorité associée à un élément.
 * Entrée : element  un pointeur vers un élément.
 * Sortie : la priorité associée à l'élément.
 */
int priorite(const element_t *element);

/**
 * Renvoie le pointeur vers le contenu de l'élément.
 * Entrée : element  un pointeur vers un élément.
 * Sortie : un pointeur vers le contenu de l'élément.
 */
void *contenu(const element_t *element);

/**
 * Une file à priorité implémentée par un tas.
 */
struct file_prio_s
{
  int capacite;
  int taille;
  element_t *tas;
};

typedef struct file_prio_s file_prio_t;

/**
 * Crée une file à priorité pouvant contenir capacite élements.
 * Entrée : capacite    le nombre d'éléments que pourra contenir la file à
 *                      priorité
 * Sortie : une file à priorité vide.
 */
file_prio_t *creer(int capacite);

/**
 * Détermine si une file à priorité est vide.
 * Entree : file    un pointeur vers une file à priorité.
 * Sortie : true si la file est vide, false sinon.
 */
bool est_vide(file_prio_t *file);

/**
 * Supprime une file à priorité. Libère la mémoire associée à la file de
 * priorité.
 * Entrée : file    un pointeur vers une file à priorité.
 */
void supprimer(file_prio_t *file);

/**
 * Insère un élément, en tenant compte de sa priorité.
 * Entrées : file     un pointeur vers une file à priorité
 *           priorite la priorité de l'élément à insérer
 *           contenu  le pointeur vers le contenu de l'élément à insérer
 * Sortie : pas de valeur de retour, la file à priorité est modifiée pour
 *          contenir le nouvel élément.
 */
void inserer(file_prio_t *file, int priorite, void *contenu);

/**
 * Extrait un élément minimum d'une file à priorité.
 * Entrée : file     un pointeur vers une file à priorité
 * Sortie : l'élément dont la valeur de la priorité est minimale dans la file à priorité (élément prioritaire), la file à priorité ne contient plus l'élément. Pour accéder au contenu et à la priorité, si nécessaire on utilisera priorite() et contenu().
 */
element_t extraire_min(file_prio_t *file);

/**
 * Consulte l'élément minimum d'une file à priorité.
 * Entrée : file     un pointeur vers une file à priorité.
 * Sortie : la valeur de l'élément dont la valeur de la priorité est
 * minimale (prochain élément à être extrait). L'élément n'est pas extrait.
 */
element_t minimum(const file_prio_t *file);
