#include <stdlib.h>
#include <stdbool.h>
#include "abr.h"

noeud *feuille(char cle, int freq)
{
    noeud *feuille = malloc(sizeof(noeud));
    feuille->cle = cle;
    feuille->freq = freq;
    feuille->parent = NULL;
    feuille->droit = NULL;
    feuille->gauche = NULL;
    return feuille;
}

// Question 10
int taille(noeud *n)
{
    return n == NULL ? 0 : 1 + taille(n->gauche) + taille(n->droit);
}

noeud *prt(noeud *n)
{
    return n == NULL ? NULL : n->parent;
}

bool est_feuille(noeud *n)
{
    return n != NULL && n->gauche == NULL && n->droit == NULL;
}

// Question 20
void insertion(noeud *abr, char cle, int freq)
{
    if (abr == NULL)
    {
        abr = feuille(cle, freq);
        return;
    }
    if (abr->cle == cle)
    {
        abr->freq = freq;
    }
    else if (abr->cle > cle)
    {
        if (abr->gauche == NULL)
        {
            abr->gauche = feuille(cle, freq);
            abr->gauche->parent = abr;
        }
        else
        {
            insertion(abr->gauche, cle, freq);
        }
    }
    else
    {
        if (abr->droit == NULL)
        {
            abr->droit = feuille(cle, freq);
            abr->droit->parent = abr;
        }
        else
        {
            insertion(abr->droit, cle, freq);
        }
    }
}

noeud *recherche(noeud *abr, int cle)
{
    if (abr == NULL)
    {
        return NULL;
    }
    if (abr->cle == cle)
    {
        return abr;
    }
    return abr->cle > cle ? recherche(abr->gauche, cle) : recherche(abr->droit, cle);
}

// Question 17
noeud *minimum_it(noeud *abr)
{
    if (abr == NULL)
    {
        return NULL;
    }
    while (abr->gauche != NULL)
    {
        abr = abr->gauche;
    }
    return abr;
}

noeud *suppression_it(noeud *abr, int value)
{
    noeud *z = recherche(abr, value);
    if (z == NULL)
        return abr;
    noeud *par = prt(z);
    if (est_feuille(z))
    {
        if (par != NULL)
        {
            if (z == par->droit)
                par->droit = NULL;
            else
                par->gauche = NULL;
            free(z);
        }
        else
            return NULL;
    }
    else if (z->droit == NULL)
    {
        if (par != NULL)
        {
            if (z == par->droit)
                par->droit = z->gauche;
            else
                par->gauche = z->gauche;
        }
        z->gauche->parent = par;
        free(z);
    }
    else if (z->gauche == NULL)
    {
        if (par != NULL)
        {
            if (z == par->droit)
                par->droit = z->droit;
            else
                par->gauche = z->gauche;
        }
        z->gauche->parent = par;
        free(z);
    }
    else
    {
        noeud *y = minimum_it(z->droit);
        z->cle = y->cle;
        if (y == prt(y)->droit)
            prt(y)->droit = y->droit;
        else
            prt(y)->gauche = y->droit;
        if (y->droit != NULL)
            y->droit->parent = prt(y);
        free(y);
    }
    return abr;
}