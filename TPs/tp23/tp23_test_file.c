/***************************************
 * Test de file à priorité avec contenu.
 *
 * @author M. Durand
 * @date   2023
 * @version 0.2
 ***************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "file_priorite.h"

// Question 1
// Dans un tas-min, l'élément de priorité minimale est la racine
// Dans un tas-min, l'élément de priorité maximale se trouve sur le dernier niveau

int main(int argc, char *argv[])
{
    int nb = 30;
    if (argc == 2)
    {
        nb = atoi(argv[1]);
    }

    /* Création d'un tableau de caractères */
    char *caracteres = (char *)malloc(sizeof(char) * nb);
    if (caracteres == NULL)
    {
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < nb; i++)
    {
        caracteres[i] = (i % 26) + 65;
    }

    /* Création d'une file de priorité */
    file_prio_t *fp = creer(nb);

    /* Insertion de nb éléments avec comme contenu des pointeurs vers les cases
     * correspondantes du tableau de caractères. */
    for (int i = 0; i < nb; i++)
    {
        inserer(fp, i, &caracteres[i]);
    }

    /* Affichage du contenu du tas */
    printf("Contenu du tas \n");
    for (int i = 0; i < fp->taille; i++)
    {
        printf("  Priorité %d, Valeur %c\n", fp->tas[i].priorite, *(char *)(fp->tas[i].contenu));
    }

    /* Vérification du bon fonctionnement de l'extraction. En cas de problème ici,
     * cela cependant provenir d'un problème lors de l'insertion, à vérifier !
     */
    for (int i = 0; i < nb; i++)
    {
        element_t elt = extraire_min(fp);
        assert(priorite(&elt) == i);
    }

    supprimer(fp);

    /* Création d'une file de priorité */
    fp = creer(nb);

    /* Insertion de nb éléments avec comme contenu des pointeurs vers les cases
     * correspondantes du tableau de caractères. */
    for (int i = nb - 1; i >= 0; i--)
    {
        inserer(fp, i, &caracteres[i]);
    }

    /* Affichage du contenu du tas */
    printf("Contenu du tas \n");
    for (int i = 0; i < fp->taille; i++)
    {
        printf("  Priorité %d, Valeur %c\n", fp->tas[i].priorite, *(char *)(fp->tas[i].contenu));
    }

    /* Vérification du bon fonctionnement de l'extraction. En cas de problème ici,
     * cela cependant provenir d'un problème lors de l'insertion, à vérifier !
     */
    for (int i = 0; i < nb; i++)
    {
        element_t elt = extraire_min(fp);
        assert(priorite(&elt) == i);
    }

    free(caracteres);
    supprimer(fp);

    return 0;
}
