#include <stdlib.h>
#include <stdio.h>
#include "file_priorite.h"
#include <stdbool.h>
#include <assert.h>

file_prio_t *creer(int capacite)
{
    file_prio_t *file_prio = (file_prio_t *)malloc(sizeof(file_prio_t));
    file_prio->capacite = capacite;
    file_prio->taille = 0;
    file_prio->tas = (element_t *)malloc(sizeof(element_t) * capacite);
    return file_prio;
}

bool est_vide(file_prio_t *file)
{
    return file->taille == 0;
}

void supprimer(file_prio_t *file)
{
    if (file == NULL)
    {
        return;
    }
    free(file->tas);
    free(file);
}

element_t minimum(const file_prio_t *file)
{
    return file->tas[0];
}

int parent(int indice)
{
    return (indice - 1) / 2;
}

int gauche(int indice)
{
    return indice * 2 + 1;
}

int droit(int indice)
{
    return (indice + 1) * 2;
}

void echanger(element_t *tableau, int i, int j)
{
    element_t tmp = tableau[i];
    tableau[i] = tableau[j];
    tableau[j] = tmp;
}

void inserer(file_prio_t *file, int priorite, void *contenu)
{
    // assert(file->capacite == file->taille);

    element_t elem = {.priorite = priorite, .contenu = contenu};
    file->tas[file->taille] = elem;
    int i = file->taille;
    while (i != 0 && file->tas[parent(i)].priorite > priorite)
    {
        echanger(file->tas, i, parent(i));
        i = parent(i);
    }
    file->taille++;
}

int priorite(const element_t *element)
{
    return element->priorite;
}

void *contenu(const element_t *element)
{

    return element->contenu;
}

element_t extraire_min(file_prio_t *file)
{
    assert(file->taille != 0);

    file->taille--;
    echanger(file->tas, 0, file->taille);
    int i = 0;
    int priorite = file->tas[0].priorite;

    while (i != file->taille - 1)
    {
        // cas où le fils gauche ET le fils droit ont une priorité plus petite
        if (gauche(i) < file->taille && droit(i) < file->taille && file->tas[gauche(i)].priorite < priorite && file->tas[droit(i)].priorite < priorite)
        {
            if (file->tas[gauche(i)].priorite <= file->tas[droit(i)].priorite)
            {
                echanger(file->tas, i, gauche(i));
                i = gauche(i);
            }
            else
            {
                echanger(file->tas, i, droit(i));
                i = droit(i);
            }
            continue;
        }
        // un des fils a une priorité plus petite
        if (gauche(i) < file->taille && file->tas[gauche(i)].priorite < priorite)
        {
            echanger(file->tas, i, gauche(i));
            i = gauche(i);
            continue;
        }
        else if (droit(i) < file->taille && file->tas[droit(i)].priorite < priorite)
        {
            echanger(file->tas, i, droit(i));
            i = droit(i);
            continue;
        }
        // on a fini
        break;
    }

    return file->tas[file->taille];
}