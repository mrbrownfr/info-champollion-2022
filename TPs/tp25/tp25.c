#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

struct image
{
    int width;
    int height;
    int maxc;
    uint16_t **pixels;
};
typedef struct image image;

// Question 1
image *charger_image(char *filename)
{
    FILE *fp = fopen(filename, "r");
    assert(fp != NULL);

    int width, height, max_c;
    fscanf(fp, "P2\n%d %d\n%d\n", &width, &height, &max_c);

    image *img = malloc(sizeof(image));
    img->width = width;
    img->height = height;
    img->maxc = max_c;
    img->pixels = malloc(sizeof(uint16_t *) * height);
    assert(img->pixels != NULL);

    uint16_t pixel;
    for (int i = 0; i < height; i++)
    {
        img->pixels[i] = malloc(sizeof(uint16_t) * width);
        assert(img->pixels[i] != NULL);

        for (int j = 0; j < width; j++)
        {
            fscanf(fp, "%hu", &pixel);
            img->pixels[i][j] = pixel;
        }
    }

    fclose(fp);
    return img;
}

// Question 2
// On obtient le bit de poids fort en calculant l'entier modulo 2

// Question 3
// 80 82 84 78 80 90 22 24

// Question 4
char caractere(uint16_t *tab)
{
    int code = 0;
    for (int i = 0; i < 8; i++)
    {
        code *= 2;
        code += tab[i] % 2;
    }
    return (char)code;
}

// Question 5
int sauvegarder_message(image *img, char *nom_sortie)
{
    if (img->width < 8)
    {
        return 1;
    }
    FILE *fp = fopen(nom_sortie, "w");
    int i = 0;
    char car = ' ';
    while (car != '\0')
    {
        car = caractere(img->pixels[i]);
        fprintf(fp, "%c", car);
        i++;
    }
    fclose(fp);
    return 0;
}

// Question 7
void inserer_caractere(char c, uint16_t *tab)
{
    int code = c;
    for (int i = 7; i >= 0; i--)
    {
        tab[i] = code % 2;
        code /= 2;
    }
}

// Question 8
int cacher(image *img, char *nom_entree)
{
    FILE *fp = fopen(nom_entree, "r");
    if (fp != NULL)
    {
        return 1;
    }

    char car;
    int i = 0;
    while (!feof(fp))
    {
        fscanf(fp, "%c", &car);
        inserer_caractere(car, img->pixels[i]);
        i++;
    }
    inserer_caractere('\0', img->pixels[i]);

    fclose(fp);
    return 0;
}

// Question 9
int sauvegarder_image(image *img, char *nom_fichier)
{
    FILE *fp = fopen(nom_fichier, "w");
    if (fp != NULL)
    {
        return 1;
    }

    fprintf(fp, "P2\n%d %d\n%d\n", img->width, img->height, img->maxc);
    for (int i = 0; i < img->height; i++)
    {
        for (int j = 0; j < img->width; j++)
        {
            fprintf(fp, "%d\n", img->pixels[i][j]);
        }
    }

    fclose(fp);
    return 0;
}

int main(int argc, char **argv)
{
    // image *img = charger_image("image_avec_message.pgm");
    // sauvegarder_message(img, "message.txt");

    image *img = charger_image("champollion_cour_honneur.pgm");
    // cacher(img, "message.txt");
    // sauvegarder_image(img, "message_de_amaury.pgm");
    sauvegarder_message(img, "decode.txt");

    for (int i = 0; i < img->height; i++)
    {
        free(img->pixels[i]);
    }
    free(img->pixels);
    free(img);
    return 0;
}