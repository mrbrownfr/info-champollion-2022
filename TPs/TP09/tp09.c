#include "./tableau.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    assert(1 <= argc); // Parce qu'on est jamais trop prudents

    int taille_tableau;
    if (argc >= 2)
    {
        taille_tableau = atoi(argv[1]);
        assert(taille_tableau >= 0);
    }

    tableau_entier *tableau = creer_tableau(taille_tableau);
    initialiser(tableau, taille_tableau);
    set(tableau, 4, 4);
    afficher_tableau(tableau);
    printf("%d\n", get(tableau, 3));

    printf("%s\n", argv[0]);

    int nouvelle_size;
    if (argc >= 3)
    {
        nouvelle_size = atoi(argv[2]);
        assert(nouvelle_size >= 0);

        modifier_taille(tableau, nouvelle_size);
        afficher_tableau(tableau);
    }

    supprimer_tableau(tableau);

    return 0;
}
