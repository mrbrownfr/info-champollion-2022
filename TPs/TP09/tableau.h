struct tableau_entier_s
{
    int *data;
    int taille;
};
typedef struct tableau_entier_s tableau_entier;

/// @brief Génère un tableau de tableau d'entiers
/// @param taille la taille du tableau à générer
/// @return un tableau généré mais non initialisé
tableau_entier *creer_tableau(int taille);

/// @brief Libère le tableau passé en argument
/// @param pointeur le tableau à libérer
void supprimer_tableau(tableau_entier *pointeur);

/// @brief Initialise un tableau de tableau d'entiers
/// @param tableau le tableau à initialiser
/// @param entier l'entier auquel initialiser le tableau
void initialiser(tableau_entier *tableau, int entier);

/// @brief Permet d'afficher le tableau passé en paramètre
/// @param tableau le tableau à afficher
void afficher_tableau(tableau_entier *tableau);

/// @brief Récupère la valeur contenue dans le tableau à l'index donné
/// @param tableau le tableau dans lequel chercher
/// @param index l'index auquel chercher
/// @return la valeur contenue à cet index
/// @exception si l'indice n'est pas valide, le programme panique
int get(tableau_entier *tableau, int index);

/// @brief Remplace la valeur à l'induce donné du tableau donné par la valeur donné
/// @param t le tableau dans lequel remplacer
/// @param i l'indice auquel remplacer
/// @param valeur la nouvelle valeur
/// @exception si l'indice n'est pas valide, le programme panique
void set(tableau_entier *t, int i, int valeur);

/// @brief Modifie la taille du tableau passé en argument
/// @param tableau le tableau à modifier
/// @param new_size la nouvelle taille du tableau
/// @exception si la taille du tableau est plus petite que l'original, des données seront perdues irremédiablement; de même si la taille du tableau est plus grande qsue l'original, des données ne seront pas initialisées.
void modifier_taille(tableau_entier *tableau, int new_size);
