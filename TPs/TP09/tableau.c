#include "./tableau.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

tableau_entier *creer_tableau(int taille)
{
    errno = 0;
    tableau_entier *tab = malloc(sizeof(tableau_entier));
    if (tab == NULL)
    {
        fprintf(stderr, "Une erreur est survenue lors de l'allocation du tableau :/ \nPLUS DE DETAILS: %s", strerror(errno));
        exit(1);
    }
    tab->taille = taille;
    tab->data = (int *)malloc(sizeof(int) * taille);
    if (tab->data == NULL)
    {
        fprintf(stderr, "Une erreur est survenue lors de l'allocation du tableau :/ \nPLUS DE DETAILS: %s", strerror(errno));
        exit(1);
    }
    return tab;
}

void supprimer_tableau(tableau_entier *pointeur)
{
    free(pointeur->data);
    free(pointeur);
}

void initialiser(tableau_entier *tableau, int entier)
{
    for (int i = 0; i < tableau->taille; i++)
    {
        tableau->data[i] = entier;
    }
}

void afficher_tableau(tableau_entier *tableau)
{
    for (int i = 0; i < tableau->taille; i++)
    {
        printf("%d ", tableau->data[i]);
    };
    printf("\n");
}

int get(tableau_entier *tableau, int index)
{
    assert(0 <= index && index < tableau->taille);
    return tableau->data[index];
}

void set(tableau_entier *t, int i, int valeur)
{
    assert(0 <= i && i < t->taille);
    t->data[i] = valeur;
}

void modifier_taille(tableau_entier *tableau, int new_size)
{
    errno = 0;

    /* ETAPE 1: allouer un tableau temporaire */
    tableau_entier *tab_tmp = malloc(sizeof(tableau_entier));
    if (tab_tmp == NULL)
    {
        fprintf(stderr, "Une erreur est survenue lors de l'allocation du tableau :/ \nPLUS DE DETAILS: %s", strerror(errno));
        exit(1);
    }
    tab_tmp->taille = tableau->taille;
    tab_tmp->data = (int *)malloc(sizeof(int) * tableau->taille);
    if (tab_tmp->data == NULL)
    {
        fprintf(stderr, "Une erreur est survenue lors de l'allocation du tableau :/ \nPLUS DE DETAILS: %s", strerror(errno));
        exit(1);
    }

    /* ETAPE 2: copier toutes les données du tableau de base dans le tableau temporaire */
    for (int i = 0; i < tableau->taille; i++)
    {
        tab_tmp->data[i] = tableau->data[i];
    }

    /* ETAPE 3: libérer l'ancien tableau de données */
    free(tableau->data);

    /* ETAPE 4: tout remettre à jour */
    tableau->data = (int *)malloc(sizeof(int) * new_size); // On alloue de nouveau un tableau à la bonne taille...
    if (tableau->data == NULL)
    {
        fprintf(stderr, "Une erreur est survenue lors de l'allocation du tableau :/ \nPLUS DE DETAILS: %s", strerror(errno));
        exit(1);
    }
    tableau->taille = new_size; // On met la taille à jour...
    int minimum_size;           // La plus petite des 2 tailles de tableau pour éviter d'accéder à des éléments qui n'existent pas
    if (tab_tmp->taille <= tableau->taille)
    {
        minimum_size = tab_tmp->taille;
    }
    else
    {
        minimum_size = tableau->taille;
    }
    for (int i = 0; i < minimum_size; i++)
    {
        tableau->data[i] = tab_tmp->data[i]; // Et on recopie les anciennes données
    }
    supprimer_tableau(tab_tmp); // On enlève enfin le tableau temporaire
}
