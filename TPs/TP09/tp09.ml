(** PARTIE COMPLEXE *)

type complexe = { re : float; im : float }

let re (complex : complexe) = complex.re
let c1 : complexe = { re = 1.; im = 2. };;

assert (re c1 = 1.)

let complex_sum (c1 : complexe) (c2 : complexe) : complexe =
  { re = c1.re +. c2.re; im = c1.im +. c2.im }

let mod_cplx (nb : complexe) : float = sqrt ((nb.re ** 2.) +. (nb.im ** 2.))

(** PARTIE BINAIRE *)

type bit = Un | Zero

let vers_binaire (nb : int) : bit list =
  let rec aux nombre =
    if nombre > 0 then
      if nombre mod 2 = 1 then Un :: aux (nombre / 2)
      else Zero :: aux (nombre / 2)
    else if nombre = 0 then []
    else failwith "Le nombre entré doit être positif !"
  in
  List.rev (aux nb)

let vers_binaire_2 (nb : int) : bit list =
  let rec aux nombre liste =
    if nombre > 0 then
      if nombre mod 2 = 1 then aux (nombre / 2) (Un :: liste)
      else aux (nombre / 2) (Zero :: liste)
    else if nombre = 0 then liste
    else failwith "Le nombre entré doit être positif !"
  in
  aux nb []

let demi_add (b1 : bit) (b2 : bit) : bit * bit =
  match (b1, b2) with
  | Un, Un -> (Un, Zero)
  | Zero, Un | Un, Zero -> (Zero, Un)
  | Zero, Zero -> (Zero, Zero)

let incrementer (bits : bit list) : bit list =
  let retenue, liste =
    List.fold_right
      (fun x y ->
        let retenue, liste = y in
        let retenue2, valeur = demi_add x retenue in
        (retenue2, valeur :: liste))
      bits (Un, [])
  in
  if retenue = Un then retenue :: liste else liste

let vers_entier (nb : bit list) : int =
  let valeur, _ =
    List.fold_right
      (fun x y ->
        let rec puissance x n =
          if n = 0 then 1
          else if n mod 2 = 0 then puissance (x * x) (n / 2)
          else x * puissance (x * x) ((n - 1) / 2)
        in
        let valeur, exposant = y in
        if x = Un then (valeur + puissance 2 exposant, exposant + 1)
        else (valeur, exposant + 1))
      nb (0, 0)
  in
  valeur
