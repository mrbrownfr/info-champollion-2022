(** {2 Arbres homogènes} *)

(* Question 1 *)
type 'a arbre = Noeud of 'a * 'a arbre list

(* Question 2 *)
let rec taille a = match a with
| Noeud (_, []) -> 1
| Noeud (_, fils) -> 1 + List.fold_left (fun acc x -> acc + (taille x)) 0 fils

(* Question 3 *)
let rec nb_feuilles a = match a with
| Noeud (_, []) -> 1
| Noeud (_, fils) -> List.fold_left (fun acc x -> acc + (nb_feuilles x)) 0 fils

(* Question 4 *)
let nb_noeuds a = (taille a) - (nb_feuilles a)

(* Question 5 *)
let rec hauteur a = match a with
| Noeud (_, []) -> 1
| Noeud (_, fils) -> 1 + (List.fold_left (fun acc x -> max acc (hauteur x)) 0 fils)

let a = Noeud (1, [Noeud (2, []); Noeud (3, [Noeud (4, [])])]);;

Printf.printf "%d\n" (taille a);;
Printf.printf "%d\n" (nb_feuilles a);;
Printf.printf "%d\n" (nb_noeuds a);;
Printf.printf "%d\n" (hauteur a);;

(** {2 Arbres hétérogènes} *)

type ('f, 'n) arbre_het =
| Feuille of 'f
| Noeud of 'n * (('f, 'n) foret)
and ('f, 'n) foret =
Arbres of ('f, 'n) arbre_het array ;;


(* Question 6 *)
let rec taille_het (a: ('f, 'n) arbre_het) = match a with
| Feuille _ -> 1
| Noeud (_, (Arbres foret)) -> 1 + (Array.fold_left (fun acc x -> acc + taille_het x) 0 foret);;

(* Question 7 *)
let rec nb_feuilles_het (a: ('f, 'n) arbre_het) = match a with
| Feuille _ -> 1
| Noeud (_, (Arbres foret)) -> Array.fold_left (fun acc x -> acc + nb_feuilles_het x) 0 foret;;

(* Question 8 *)
let rec hauteur_het (a: ('f, 'n) arbre_het) = match a with
| Feuille _ -> 1
| Noeud (_, (Arbres foret)) -> 1 + (Array.fold_left (fun acc x -> max acc (hauteur_het x)) 0 foret);;

let a = Noeud (1, Arbres [|Feuille 'a'; Noeud (3, Arbres [|Feuille 'b'; Feuille 'c'|])|]);;

Printf.printf "%d\n" (taille_het a);;
Printf.printf "%d\n" (nb_feuilles_het a);;
Printf.printf "%d\n" (hauteur_het a);;

(* Question 9 *)
(* représentation préfixée (c ((a) (b ((Z) (Y))) (d))) *)
(* représentation postfixée (((a) ((Z) (Y)) b) (d) c) *)

(* représentation préfixée (5 ((a) (2 ((z) (b))) (c))) *)
(* représentation postfixée (((a) ((z) (b)) 2) (c) 5) *)

(* Question 10 *)
let rec affichage_prefixe (a: (char, int) arbre_het) = match a with
  | Feuille f -> Printf.printf "(%c)" f
  | Noeud (n, (Arbres foret)) -> Printf.printf "(%d" n; Array.iter affichage_prefixe foret; Printf.printf ")";;

affichage_prefixe a;;
print_newline ();;

(* Question 11 *)
let rec affichage_postfixe (a: (char, int) arbre_het) = match a with
| Feuille f -> Printf.printf "(%c)" f
| Noeud (n, (Arbres foret)) -> Printf.printf "("; Array.iter affichage_postfixe foret; Printf.printf "%d)" n;;

affichage_postfixe a;;
print_newline ();;

(* Question 12 *)
let rec parcours_arbre (a: ('f, 'n) arbre_het) (explo_feuille : 'f -> unit) (explo_noeud : 'n -> ('f, 'n) foret -> unit) = match a with
| Feuille f -> explo_feuille f
| Noeud (n, foret) -> explo_noeud n foret

(* Question 13 *)
let prefixe (a: (char, int) arbre_het) = parcours_arbre a (fun f -> Printf.printf "%c " f)
  (fun n (Arbres foret) -> Printf.printf "%d " n; Array.iter (fun x -> parcours_arbre x ));;