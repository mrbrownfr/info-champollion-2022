#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Question 14

// 1 a pour enfant 2 et pas de frère

// 2 a pour enfant 2 et pour frère 5
// 5 n'a pas d'enfant et a pour frère 6

// 2 n'as pas d'enfant et a pour frère 3
// 3 n'as pas d'enfant et a pour frère 4

// Question 15
typedef struct Arbre
{
    int val;
    struct Arbre *enfant;
    struct Arbre *frere;
} arbre;

// Question 16
arbre *feuille(int val)
{
    arbre *f = malloc(sizeof(arbre));
    f->val = val;
    f->enfant = NULL;
    f->frere = NULL;
    return f;
}

// Question 17
void ajouter_enfant(arbre *a, arbre *enfant)
{
    if (a->enfant == NULL)
    {
        a->enfant = enfant;
    }
    else
    {
        enfant->frere = a->enfant;
        a->enfant = enfant;
    }
}

// Question 19
int taille(arbre *a)
{
    if (a == NULL)
    {
        return 0;
    }
    return 1 + taille(a->enfant) + taille(a->frere);
}

// Question 20
int nb_feuilles(arbre *a)
{
    if (a == NULL)
    {
        return 0;
    }
    if (a->enfant == NULL)
    {
        return 1 + nb_feuilles(a->frere);
    }
    return nb_feuilles(a->enfant) + nb_feuilles(a->frere);
}

// Question 21
int nb_noeuds(arbre *a)
{
    return taille(a) - nb_feuilles(a);
}

int max(int a, int b)
{
    if (a > b)
    {
        return a;
    }
    return b;
}

// Question 22
int hauteur(arbre *a)
{
    if (a == NULL)
    {
        return 0;
    }
    return max(1 + hauteur(a->enfant), hauteur(a->frere));
}

// Question 23
int squelette(arbre *a, arbre *b)
{
    if (a == NULL || b == NULL)
    {
        return a == b;
    }
    return squelette(a->enfant, b->enfant) && squelette(a->frere, b->frere);
}

// Question 24
int egalite(arbre *a, arbre *b)
{
    if (a == NULL || b == NULL)
    {
        return a == b;
    }
    return a->val == b->val && egalite(a->enfant, b->enfant) && egalite(a->frere, b->frere);
}

// Question 25
void affichage_prefixe(arbre *a)
{
    if (a == NULL)
    {
        return;
    }
    printf("(%d", a->val);
    if (a->enfant != NULL)
    {
        printf("(");
        affichage_prefixe(a->enfant);
        printf(")");
    }
    else
    {
        affichage_prefixe(a->enfant);
    }

    printf(")");
    affichage_prefixe(a->frere);
}

// Question 26
void libere_memoire(arbre *a)
{
    if (a == NULL)
    {
        return;
    }
    libere_memoire(a->enfant);
    libere_memoire(a->frere);
    free(a);
}

// Question 27
arbre *from_string(char *str)
{
}

int main()
{
    // Question 18
    arbre *a = feuille(1);
    arbre *b = feuille(2);
    ajouter_enfant(b, feuille(2));
    ajouter_enfant(b, feuille(3));
    ajouter_enfant(b, feuille(4));
    ajouter_enfant(a, b);
    ajouter_enfant(a, feuille(5));
    ajouter_enfant(a, feuille(6));

    arbre *c = feuille(2);
    arbre *d = feuille(2);
    ajouter_enfant(d, feuille(3));
    ajouter_enfant(d, feuille(4));
    ajouter_enfant(d, feuille(5));
    ajouter_enfant(c, d);
    ajouter_enfant(c, feuille(6));
    ajouter_enfant(c, feuille(7));

    printf("%d\n", taille(a));
    printf("%d\n", nb_feuilles(a));
    printf("%d\n", nb_noeuds(a));
    printf("%d\n", hauteur(a));
    printf("%d\n", squelette(a, c));
    printf("%d\n", egalite(a, c));
    affichage_prefixe(a);

    libere_memoire(a);
    libere_memoire(b);
    libere_memoire(c);
    libere_memoire(d);

    return 0;
}