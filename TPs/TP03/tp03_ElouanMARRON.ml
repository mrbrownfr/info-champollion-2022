(** Question 5: vérifie si une liste est croissante
    @param liste la liste à vérifier
    @return true si croissante, false sinon *)
let rec est_croissante liste =
  match liste with
  | [] | [ _ ] -> true
  | head :: tail -> head <= List.hd tail && est_croissante tail

(** Question 2: vérifie si 2 listes sont égales
      @param l1 la première liste à compmarer
      @param l2 la 2e liste à comparer
      @return true si les listes sont égales, false sinon *)
let rec sont_egales l1 l2 =
  if l1 = [] && l2 = [] then true
  else if List.hd l1 = List.hd l2 then sont_egales (List.tl l1) (List.tl l2)
  else false

(** Question 3: génère une liste aléatoire à n éléments
    @param n le nombre d'éléments de la liste
    @return une liste aléatoirement générée *)
let gen_liste_aleatoire n =
  let rec aux liste =
    match List.length liste = n with
    | true -> liste
    | false -> aux (Random.int (2 * n) :: liste)
  in
  aux []

(** Question 4: génère une liste croissante à n éléments
    @param n le nombre d'éléments de la liste
    @return une liste aléatoirement générée  *)
let gen_liste_triee n =
  if n < 0 then failwith "n doit être positif !"
  else
    let rec aux x =
      match x = 0 with true -> [] | false -> [ n - (x - 1) ] @ aux (x - 1)
    in
    aux n

(** Question 5: génère une liste décroissante
    @param n le nombre d'éléments de la liste
    @return une liste aléatoirement générée *)
let gen_liste_triee_dec n = List.rev (gen_liste_triee n)

(* * Question 6 *)
type listcollection = {
  l10 : int list;
  l50 : int list;
  l100 : int list;
  l1000 : int list;
  l5000 : int list;
  l10000 : int list;
  l50000 : int list;
  l100000 : int list;
}

let liste_aleatoire : listcollection =
  {
    l10 = gen_liste_aleatoire 10;
    l50 = gen_liste_aleatoire 50;
    l100 = gen_liste_aleatoire 100;
    l1000 = gen_liste_aleatoire 1000;
    l5000 = gen_liste_aleatoire 5000;
    l10000 = gen_liste_aleatoire 10000;
    l50000 = gen_liste_aleatoire 50000;
    l100000 = gen_liste_aleatoire 100000;
  }

let liste_croissante : listcollection =
  {
    l10 = gen_liste_triee 10;
    l50 = gen_liste_triee 50;
    l100 = gen_liste_triee 100;
    l1000 = gen_liste_triee 1000;
    l5000 = gen_liste_triee 5000;
    l10000 = gen_liste_triee 10000;
    l50000 = gen_liste_triee 50000;
    l100000 = gen_liste_triee 100000;
  }

let liste_decroissante : listcollection =
  {
    l10 = gen_liste_triee_dec 10;
    l50 = gen_liste_triee_dec 50;
    l100 = gen_liste_triee_dec 100;
    l1000 = gen_liste_triee_dec 1000;
    l5000 = gen_liste_triee_dec 5000;
    l10000 = gen_liste_triee_dec 10000;
    l50000 = gen_liste_triee_dec 50000;
    l100000 = gen_liste_triee_dec 100000;
  }

(** Question 7: implémentation du tri par insertion
    @param liste la liste à trier
    @return la liste triée *)
let tri_insertion liste =
  let rec insertion elt lst =
    match lst with
    | [] -> [ elt ]
    | head :: tail ->
        if elt <= head then [ elt; head ] @ tail else head :: insertion elt tail
  in
  let rec aux lst sorted =
    if List.length lst <> 0 then
      aux (List.tl lst) (insertion (List.hd lst) sorted)
    else sorted
  in
  aux liste []
(* TODO Finir la fonction tri_insertion *)

(** Question 9: implémentation du tri par sélection
    @param liste la liste à trier
    @return la liste triée *)
let tri_selection liste =
  let min_list lst = List.fold_left min (List.hd lst) (List.tl lst) in
  let rec pop elt lst =
    match lst with
    | [] -> []
    | head :: tail -> if head = elt then tail else head :: pop elt tail
  in
  let rec insertion elt lst =
    match lst with
    | [] -> [ elt ]
    | head :: tail ->
        if elt <= head then [ elt; head ] @ tail else head :: insertion elt tail
  in
  let rec aux l sorted =
    if List.length l <> 0 then
      aux (pop (min_list l) l) (insertion (min_list l) sorted)
    else sorted
  in
  aux liste []

(** Question 11: implémentation du tri par fusion
    @param liste la liste à trier
    @return la liste triée *)
let rec tri_fusion liste =
  if List.length liste <= 1 then liste
  else
    let lng = List.length liste / 2 in
    let split n l =
      let rec firstk k xs =
        match xs with
        | [] -> []
        | x :: xs -> if k = 1 then [ x ] else x :: firstk (k - 1) xs
      in
      let rec lminusfirstk n u =
        match u with
        | [] -> []
        | _ :: q -> if n = 0 then u else lminusfirstk (n - 1) q
      in
      match l with
      | [ x; y ] -> [ [ x ]; [ y ] ]
      | _ -> [ firstk lng l; lminusfirstk lng l ]
    in

    let rec fusion l1 l2 =
      match l1 with
      | [] -> l2
      | _ ->
          if l2 = [] then l1
          else if List.hd l1 <= List.hd l2 then
            List.hd l1 :: fusion (List.tl l1) l2
          else List.hd l2 :: fusion l1 (List.tl l2)
    in
    fusion
      (tri_fusion (List.hd (split lng liste)))
      (tri_fusion (List.hd (List.tl (split lng liste))))

(** Question 13: implémentation du tri rapide
    @param liste la liste à trier
    @return la liste triée *)
let rec tri_rapide_prem liste =
  if List.length liste <= 1 then liste
  else
    let rec pop elt lst =
      match lst with
      | [] -> []
      | head :: tail -> if head = elt then tail else head :: pop elt tail
    in
    let pivot = List.hd liste in
    let rec crible pred u =
      match u with
      | [] -> ([], [])
      | e :: suite ->
          let oui, non = crible pred suite in
          if pred e then (e :: oui, non) else (oui, e :: non)
    in
    let inferieurs, superieurs =
      crible (fun x -> x <= pivot) (pop pivot liste)
    in
    tri_rapide_prem inferieurs @ [ pivot ] @ tri_rapide_prem superieurs

(** Question 15: implémentation du tri rapide
    @param liste la liste à trier
    @return la liste triée *)
let rec tri_rapide_alea liste =
  if List.length liste <= 1 then liste
  else
    let rec pop elt lst =
      match lst with
      | [] -> []
      | head :: tail -> if head = elt then tail else head :: pop elt tail
    in
    let pivot =
      let rec nieme idx liste =
        if idx < 0 then failwith "Index négatif impossible !"
        else
          match idx with
          | 0 -> List.hd liste
          | _ -> nieme (idx - 1) (List.tl liste)
      in
      nieme (Random.int (List.length liste)) liste
    in
    let rec crible pred u =
      match u with
      | [] -> ([], [])
      | e :: suite ->
          let oui, non = crible pred suite in
          if pred e then (e :: oui, non) else (oui, e :: non)
    in
    let inferieurs, superieurs =
      crible (fun x -> x <= pivot) (pop pivot liste)
    in
    tri_rapide_alea inferieurs @ (pivot :: tri_rapide_alea superieurs)

(** Question 17: utilisation de la librairie standard
    @param liste la liste à trier
    @return la liste triée *)
let sort_std liste =
  List.sort (fun x y -> if x < y then -1 else if x = y then 0 else 1) liste

(* let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l10

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n"

   let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l50

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n"

   let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l100

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n"

   let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l1000

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n"

   let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l5000

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n"

   let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l10000

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n"

   let debut = Sys.time ();;

   tri_rapide_alea liste_aleatoire.l50000

   let fin = Sys.time ()
   let temps = fin -. debut;;

   print_float temps;;
   Printf.printf "OK\n" *)

let temps f x =
  let t = Sys.time () in
  let fx = f x in
  print_float (Sys.time () -. t);
  fx;
  Printf.printf "\n"
;;

print_string "tri_insertion:\n";;
temps tri_insertion liste_aleatoire.l10;;
temps tri_insertion liste_aleatoire.l50;;
temps tri_insertion liste_aleatoire.l100;;
temps tri_insertion liste_aleatoire.l1000;;
temps tri_insertion liste_aleatoire.l5000;;
temps tri_insertion liste_aleatoire.l10000;;
temps tri_insertion liste_aleatoire.l50000;;
temps tri_insertion liste_aleatoire.l100000;;
temps tri_insertion liste_croissante.l10;;
temps tri_insertion liste_croissante.l50;;
temps tri_insertion liste_croissante.l100;;
temps tri_insertion liste_croissante.l1000;;
temps tri_insertion liste_croissante.l5000;;
temps tri_insertion liste_croissante.l10000;;
temps tri_insertion liste_croissante.l50000;;
temps tri_insertion liste_croissante.l100000;;
temps tri_insertion liste_decroissante.l10;;
temps tri_insertion liste_decroissante.l50;;
temps tri_insertion liste_decroissante.l100;;
temps tri_insertion liste_decroissante.l1000;;
temps tri_insertion liste_decroissante.l5000;;
temps tri_insertion liste_decroissante.l10000;;
temps tri_insertion liste_decroissante.l50000;;
temps tri_insertion liste_decroissante.l100000;;
print_string "\n";;
print_string "tri_selection:\n";;
temps tri_selection liste_aleatoire.l10;;
temps tri_selection liste_aleatoire.l50;;
temps tri_selection liste_aleatoire.l100;;
temps tri_selection liste_aleatoire.l1000;;
temps tri_selection liste_aleatoire.l5000;;
temps tri_selection liste_aleatoire.l10000;;
temps tri_selection liste_aleatoire.l50000;;
temps tri_selection liste_aleatoire.l100000;;
temps tri_selection liste_croissante.l10;;
temps tri_selection liste_croissante.l50;;
temps tri_selection liste_croissante.l100;;
temps tri_selection liste_croissante.l1000;;
temps tri_selection liste_croissante.l5000;;
temps tri_selection liste_croissante.l10000;;
temps tri_selection liste_croissante.l50000;;
temps tri_selection liste_croissante.l100000;;
temps tri_selection liste_decroissante.l10;;
temps tri_selection liste_decroissante.l50;;
temps tri_selection liste_decroissante.l100;;
temps tri_selection liste_decroissante.l1000;;
temps tri_selection liste_decroissante.l5000;;
temps tri_selection liste_decroissante.l10000;;
temps tri_selection liste_decroissante.l50000;;
temps tri_selection liste_decroissante.l100000;;
print_string "\n";;
print_string "tri_fusion:\n";;
temps tri_fusion liste_aleatoire.l10;;
temps tri_fusion liste_aleatoire.l50;;
temps tri_fusion liste_aleatoire.l100;;
temps tri_fusion liste_aleatoire.l1000;;
temps tri_fusion liste_aleatoire.l5000;;
temps tri_fusion liste_aleatoire.l10000;;
temps tri_fusion liste_aleatoire.l50000;;
temps tri_fusion liste_aleatoire.l100000;;
temps tri_fusion liste_croissante.l10;;
temps tri_fusion liste_croissante.l50;;
temps tri_fusion liste_croissante.l100;;
temps tri_fusion liste_croissante.l1000;;
temps tri_fusion liste_croissante.l5000;;
temps tri_fusion liste_croissante.l10000;;
temps tri_fusion liste_croissante.l50000;;
temps tri_fusion liste_croissante.l100000;;
temps tri_fusion liste_decroissante.l10;;
temps tri_fusion liste_decroissante.l50;;
temps tri_fusion liste_decroissante.l100;;
temps tri_fusion liste_decroissante.l1000;;
temps tri_fusion liste_decroissante.l5000;;
temps tri_fusion liste_decroissante.l10000;;
temps tri_fusion liste_decroissante.l50000;;
temps tri_fusion liste_decroissante.l100000;;
print_string "\n";;
print_string "tri_rapide_alea:\n";;
temps tri_rapide_alea liste_aleatoire.l10;;
temps tri_rapide_alea liste_aleatoire.l50;;
temps tri_rapide_alea liste_aleatoire.l100;;
temps tri_rapide_alea liste_aleatoire.l1000;;
temps tri_rapide_alea liste_aleatoire.l5000;;
temps tri_rapide_alea liste_aleatoire.l10000;;
temps tri_rapide_alea liste_aleatoire.l50000;;
temps tri_rapide_alea liste_aleatoire.l100000;;
temps tri_rapide_alea liste_croissante.l10;;
temps tri_rapide_alea liste_croissante.l50;;
temps tri_rapide_alea liste_croissante.l100;;
temps tri_rapide_alea liste_croissante.l1000;;
temps tri_rapide_alea liste_croissante.l5000;;
temps tri_rapide_alea liste_croissante.l10000;;
temps tri_rapide_alea liste_croissante.l50000;;
temps tri_rapide_alea liste_croissante.l100000;;
temps tri_rapide_alea liste_decroissante.l10;;
temps tri_rapide_alea liste_decroissante.l50;;
temps tri_rapide_alea liste_decroissante.l100;;
temps tri_rapide_alea liste_decroissante.l1000;;
temps tri_rapide_alea liste_decroissante.l5000;;
temps tri_rapide_alea liste_decroissante.l10000;;
temps tri_rapide_alea liste_decroissante.l50000;;
temps tri_rapide_alea liste_decroissante.l100000;;
print_string "\n";;
print_string "sort_std:\n";;
temps sort_std liste_aleatoire.l10;;
temps sort_std liste_aleatoire.l50;;
temps sort_std liste_aleatoire.l100;;
temps sort_std liste_aleatoire.l1000;;
temps sort_std liste_aleatoire.l5000;;
temps sort_std liste_aleatoire.l10000;;
temps sort_std liste_aleatoire.l50000;;
temps sort_std liste_aleatoire.l100000;;
temps sort_std liste_croissante.l10;;
temps sort_std liste_croissante.l50;;
temps sort_std liste_croissante.l100;;
temps sort_std liste_croissante.l1000;;
temps sort_std liste_croissante.l5000;;
temps sort_std liste_croissante.l10000;;
temps sort_std liste_croissante.l50000;;
temps sort_std liste_croissante.l100000;;
temps sort_std liste_decroissante.l10;;
temps sort_std liste_decroissante.l50;;
temps sort_std liste_decroissante.l100;;
temps sort_std liste_decroissante.l1000;;
temps sort_std liste_decroissante.l5000;;
temps sort_std liste_decroissante.l10000;;
temps sort_std liste_decroissante.l50000;;
temps sort_std liste_decroissante.l100000;;
print_string "\n"
