(* Question 2 *)
let nb_occ str =
  let n = String.length str in
  let dico = Hashtbl.create n in
  for i = 0 to n - 1 do
    if Hashtbl.mem dico str.[i] then
      Hashtbl.replace dico str.[i] (Hashtbl.find dico str.[i] + 1)
    else Hashtbl.add dico str.[i] 1
  done;
  dico

type huffman = F of char | N of huffman * huffman

(* Question 3 *)
let foret str = Hashtbl.fold (fun k d acc -> (F k, d) :: acc) (nb_occ str) []

(* Question 4 *)
(* Insérer est de complexité O(n) *)
(* extraire_min est de complexité O(1) *)
(* minimum est de complexité O(1) *)
(* diminuer_clé est de complexité O(n) *)

(* Question 5 *)
let tri (l : (huffman * int) list) = List.sort (fun (_, x) (_, y) -> x - y) l

(* Question 6 *)
let rec insere (k1, d1) l =
  match l with
  | [] -> [ (k1, d1) ]
  | (k2, d2) :: q ->
      if d2 > d1 then (k1, d1) :: (k2, d2) :: q
      else (k2, d2) :: insere (k1, d1) q

(* Question 7 *)
let extraire_min l =
  match l with [] -> failwith "liste vide" | e :: q -> (e, q)

let arbre_codage str =
  let q = ref (foret str |> tri) in
  let n = List.length !q in
  for i = 0 to n - 2 do
    let (k1, d1), nq = extraire_min !q in
    let (k2, d2), nq = extraire_min nq in
    q := nq;
    q := insere (N (k1, k2), d1 + d2) !q
  done;
  fst @@ List.hd !q

(* Question 8 *)
let code_par_lettre str =
  let code = arbre_codage str in
  let rec aux code dic path =
    match code with
    | F v -> Hashtbl.add dic v path
    | N (g, d) ->
        aux g dic (path @ [ 1 ]);
        aux d dic (path @ [ 0 ])
  in
  let dico = String.length str |> Hashtbl.create in
  aux code dico [];
  dico

(* Question 9 *)
let pourcent_compression str =
  float_of_int
    (Hashtbl.fold
       (fun k d acc ->
         acc + (d * List.length (Hashtbl.find (code_par_lettre str) k)))
       (nb_occ str) 0)
  /. (float_of_int @@ (String.length str * 7))

(* Question 10 *)
let codage_huffman str huff =
  let rec aux code dic path =
    match code with
    | F v -> Hashtbl.add dic v path
    | N (g, d) ->
        aux g dic (path @ [ 1 ]);
        aux d dic (path @ [ 0 ])
  in
  let dico = String.length str |> Hashtbl.create in
  aux huff dico [];
  String.fold_left (fun acc c -> Hashtbl.find dico c :: acc) [] str |> List.rev

(* Question 11 *)
let decodage_huffman code huff =
  let rec aux code dic path =
    match code with
    | F v -> Hashtbl.add dic path v
    | N (g, d) ->
        aux g dic (path @ [ 1 ]);
        aux d dic (path @ [ 0 ])
  in
  let dico = List.length code |> Hashtbl.create in
  aux huff dico [];
  List.fold_left
    (fun acc c -> acc ^ Char.escaped @@ Hashtbl.find dico c)
    "" code
;;

List.fold_left (fun acc x -> acc + x) 0 [ 1; 2; 3; 4 ]
