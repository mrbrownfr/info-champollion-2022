#include "tp15_liste_v2.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


int main(void)
{
  dll* l = new_dll();

  node* n = l->sentinel;

  insert_after(n, 0);

  n = n->next;

  for(int i = 1; i < 6; i = i+2)
  {
    insert_after(n, i+1);
    n = n->next;
    insert_before(n, i);
  }

  int check = 0;
  n = l->sentinel->next;
  while (n != NULL && n != l->sentinel)
  {
    // printf("%d \n", n->value);
    assert(n->value == check++);
    n = n->next;
  }

  delete_dll(l);

  printf("Test passed : OK\n");

  return 0;
}
