struct Node
{
  int value;
  struct Node *prev;
  struct Node *next;
};

typedef struct Node node;

struct ListeDC
{
  node *sentinel;
};

/* dll pour Double Linked List */
typedef struct ListeDC dll;

/* Crée un nouveau noeud contenant une valeur donnée en affectant les champs
 * prev et next à NULL.
 * Entrée : x   un entier représentant la valeur de l'élément
 * Sortie : un pointeur vers un nouveau n\oe ud.
 */
node *new_node(int x);

/* Crée une nouvelle liste doublement chaînée, vide.
 * Sortie : un pointeur vers une nouvelle liste.
 */
dll *new_dll();

/* Supprime un noeud d'une liste et libère la mémoire associée au noeud.
 * Entrée : n   un pointeur vers un élément de la liste
 * Sortie : pas de valeur de retour, le noeud a été supprimée de la liste et
 * libéré.
 * Précondition : le noeud passé en paramètre n'est pas le noeud sentinelle.
 */
void delete_node(node *n);

/* Alloue et insère un nouveau noeud en utilisant la valeur donnée avant le noeud passé en argument.
 * Entrée : n   un pointeur vers le noeud devant lequel insérer
 *          x   la valeur du nouvel élément.
 * Sortie : pas de valeur de retour, la liste a été modifiée et le noeud a été
 * créé comme indiqué.
 * Pré-condition : n est un pointeur valide vers un noeud alloué et présent dans
 * la liste.
 */
void insert_before(node *n, int x);

/* Alloue et insère un nouveau noeud en utilisant la valeur donnée après le noeud passé en argument.
 * Entrée : n   un pointeur vers le noeud devant lequel insérer
 *          x   la valeur du nouvel élément.
 * Sortie : pas de valeur de retour, la liste a été modifiée et le noeud a été
 * créé comme indiqué.
 * Pré-condition : n est un pointeur valide vers un noeud alloué et présent dans
 * la liste.
 */
void insert_after(node *n, int x);

/* Supprime une liste et libère la mémoire associée à tous ses noeuds et à elle-même.
 * Entrée : l   un pointeur vers la liste
 * Sortie : pas de valeur de retour, la liste a été supprimée et la mémoire
 *          est libérée
 */
void delete_dll(dll *l);

/* Ajoute un élément en tête de la liste avec la valeur passée en argument.
 * Entrée : l   un pointeur vers la liste
 *          x   la valeur de l'élément à ajouter
 * Sortie : pas de valeur de retour, la liste a été modifiée et le noeud a
 * été créé et inséré comme indiqué
 */
void push_left(dll *d, int x);

/* Ajoute un élément en queue de la liste avec la valeur passée en argument.
 * Entrée : l   un pointeur vers la liste
 *          x   la valeur de l'élément à ajouter
 * Sortie : pas de valeur de retour, la liste a été modifiée et le noeud a
 * été créé et inséré comme indiqué
 */
void push_right(dll *d, int x);

/* Retourne l'élément en tête de la liste et le supprime.
 * Entrée : l   un pointeur vers la liste
 * Sortie : la valeur du noeud en tête de la liste
 * Pré-condition : la liste l n'est pas vide
 */
int pop_left(dll *d);

/* Retourne l'élément en queue de la liste et le supprime.
 * Entrée : l   un pointeur vers la liste
 * Sortie : la valeur du noeud en queue de la liste
 * Pré-condition : la liste l n'est pas vide
 */
int pop_right(dll *d);

/* Converti un tableau en liste doublement chaînée
 * Entrée : t   un pointeur vers le tableau à copier
 *          n   la taille du tableau à copier
 * Sortie : un pointeur vers la liste doublement chaînée nouvellement
 * crée contenant toutes les valeurs du tableau
 */
dll *from_array(int *t, int n);
