struct Node {
  int value;
  struct Node *prev;
  struct Node *next;
};

typedef struct Node node;

struct ListeDC {
  node *start;
  node *end;
};

/* dll pour Double Linked List */
typedef struct ListeDC dll;


/* Crée un nouveau noeud contenant une valeur donnée en affectant les champs
 * prev et next à NULL.
 * Entrée : x   un entier représentant la valeur de l'élément
 * Sortie : un pointeur vers un nouveau n\oe ud.
 */
node *new_node(int x);

/* Crée une nouvelle liste doublement chaînée, vide.
 * Sortie : un pointeur vers une nouvelle liste.
 */
dll *new_dll();


/* Supprime un noeud d'une liste et libère la mémoire associée au noeud.
 * Entrée : l   une liste doublement chaînée
 *          n   un pointeur vers un élément de la liste
 * Sortie : pas de valeur de retour, le noeud a été supprimée de la liste et
 * libéré.
 */
void delete_node(dll *l, node *n);



/* Alloue et insère un nouveau noeud en utilisant la valeur donnée avant le noeud passé en argument.
 * Entrée : l   une liste doublement chaînée
 *          n   un pointeur vers le noeud devant lequel insérer
 *          x   la valeur du nouvel élément.
 * Sortie : pas de valeur de retour, la liste a été modifiée et le noeud a été
 * créé comme indiqué.
 * Pré-condition : n est un pointeur valide vers un noeud alloué et présent dans
 * la liste.
 */
void insert_before(dll *l, node *n, int x);


/* Alloue et insère un nouveau noeud en utilisant la valeur donnée après le noeud passé en argument.
 * Entrée : l   une liste doublement chaînée
 *          n   un pointeur vers le noeud devant lequel insérer
 *          x   la valeur du nouvel élément.
 * Sortie : pas de valeur de retour, la liste a été modifiée et le noeud a été
 * créé comme indiqué.
 * Pré-condition : n est un pointeur valide vers un noeud alloué et présent dans
 * la liste.
 */
void insert_after(dll *l, node *n, int x);

