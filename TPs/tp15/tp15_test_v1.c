#include "tp15_liste_v1.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/* devrait être définie dans le fichier tp15_liste_v1 */
/* ATTENTION : ne pas lire la fonction suivante si vous souhaitez réaliser par vous-même la
 * question facultative correspondante */
/* il vous faudra aussi l'enlever pour que la compilation fonctionne si vous
 * l'avez écrite par vous-même (dans tp15_liste_v1.c) */
void delete_dll(dll *l)
{
  if (l->start != NULL)
  {
    node *n = l->start;
    while (n != NULL)
    {
      node *suivant = n->next;
      delete_node(l, n);
      n = suivant;
    }
  }
  free(l);
}

int main(void)
{
  dll *l = new_dll();

  node *n = new_node(0);

  l->start = n;
  l->end = n;

  for (int i = 1; i < 6; i = i + 2)
  {
    insert_after(l, n, i + 1);
    n = n->next;
    insert_before(l, n, i);
  }

  int check = 0;
  n = l->start;
  while (n != NULL && n != l->end)
  {
    assert(n->value == check++);
    n = n->next;
  }

  delete_dll(l);

  printf("Test passed : OK\n");
  return 0;
}
