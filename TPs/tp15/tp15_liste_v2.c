#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "tp15_liste_v2.h"

node *new_node(int x)
{
  node *n = (node *)malloc(sizeof(node));
  if (n == NULL)
  {
    exit(EXIT_FAILURE);
  }
  n->value = x;
  n->prev = NULL;
  n->next = NULL;
  return n;
}

dll *new_dll()
{
  node *sentinel = new_node(0);
  sentinel->next = sentinel;
  sentinel->prev = sentinel;
  dll *l = (dll *)malloc(sizeof(dll));
  if (l == NULL)
  {
    exit(EXIT_FAILURE);
  }
  l->sentinel = sentinel;
  return l;
}

void delete_node(node *n)
{
  n->prev->next = n->next;
  n->next->prev = n->prev;

  free(n);
}

void insert_before(node *n, int x)
{
  node *new_n = new_node(x);

  new_n->prev = n->prev;
  n->prev->next = new_n;

  new_n->next = n;
  n->prev = new_n;
}

void insert_after(node *n, int x)
{
  node *new_n = new_node(x);

  new_n->next = n->next;
  n->next->prev = new_n;

  new_n->prev = n;
  n->next = new_n;
}

void delete_dll(dll *l)
{
  node *n = l->sentinel->next;
  while (n != l->sentinel)
  {
    n = n->next;
    delete_node(n->prev);
  }
  free(n);
  free(l);
}

void push_left(dll *d, int x)
{
  insert_after(d->sentinel, x);
}

void push_right(dll *d, int x)
{
  insert_before(d->sentinel, x);
}

int pop_left(dll *d)
{
  assert(d->sentinel->next != d->sentinel || d->sentinel->prev != d->sentinel);
  int val = d->sentinel->next->value;
  delete_node(d->sentinel->next);
  return val;
}

int pop_right(dll *d)
{
  assert(d->sentinel->next != d->sentinel || d->sentinel->prev != d->sentinel);
  int val = d->sentinel->prev->value;
  delete_node(d->sentinel->prev);
  return val;
}

dll *from_array(int *t, int n)
{
  dll *d = new_dll();
  for (int i = 0; i < n; i++)
  {
    push_right(d, t[i]);
  }
  return d;
}
