#include <stdlib.h>
#include <assert.h>
#include "tp15_liste_v1.h"

node *new_node(int x)
{
	node *n = (node *)malloc(sizeof(node));
	if (n == NULL)
	{
		exit(EXIT_FAILURE);
	}
	n->value = x;
	n->prev = NULL;
	n->next = NULL;
	return n;
}

dll *new_dll()
{
	dll *l = (dll *)malloc(sizeof(dll));
	if (l == NULL)
	{
		exit(EXIT_FAILURE);
	}
	l->start = NULL;
	l->end = NULL;
	return l;
}

void delete_node(dll *l, node *n)
{
	assert(l->start != NULL || l->end != NULL);
	if (n->prev == NULL)
	{
		l->start = n->next;
		n->next->prev = NULL;
	}
	else if (n->next == NULL)
	{
		l->end = n->prev;
		n->prev->next = NULL;
	}
	else
	{
		n->prev->next = n->next;
		n->next->prev = n->prev;
	}
	free(n);
}

void insert_before(dll *l, node *n, int x)
{
	node *new_n = new_node(x);
	if (n->prev == NULL) // si n est le noeud de début de liste
	{
		l->start = new_n;
		new_n->next = n;
		new_n->prev = NULL;
		n->prev = new_n;
	}
	else
	{
		new_n->prev = n->prev;
		n->prev->next = new_n;

		new_n->next = n;
		n->prev = new_n;
	}
}

void insert_after(dll *l, node *n, int x)
{
	node *new_n = new_node(x);
	if (n->next == NULL) // si n est le noeud de fin de liste
	{
		l->end = new_n;
		new_n->prev = n;
		new_n->next = NULL;
		n->next = new_n;
	}
	else
	{
		new_n->next = n->next;
		n->next->prev = new_n;

		new_n->prev = n;
		n->next = new_n;
	}
}
