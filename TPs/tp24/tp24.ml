type objet = { masse : int; valeur : int }

(* VERSION PROGRAMMATION DYNAMIQUE *)

let sol_prog_dyn objets capacite =
  let dp = Array.init (capacite + 1) (fun _ -> []) in
  let rec aux objets =
    match objets with
    | [] -> ()
    | e :: q ->
        for i = capacite downto 0 do
          let x = dp.(i) in
          if
            i + e.masse <= capacite
            && e.valeur + List.fold_left (fun acc y -> acc + y.valeur) 0 x
               > List.fold_left (fun acc y -> acc + y.valeur) 0 dp.(i + e.masse)
          then dp.(i + e.masse) <- e :: x
        done;
        aux q
  in
  aux objets;
  let i = ref capacite in
  while dp.(!i) = [] do
    i := !i - 1
  done;
  dp.(!i)

(* VERSION GLOUTONE *)

let sol_gloutonne objets capacite =
  let objets = List.sort (fun x y -> x.masse - y.masse) objets in
  let rec aux objets capacite path =
    match objets with
    | [] -> path
    | e :: q ->
        if e.masse > capacite then aux q capacite path
        else max (aux q (capacite - e.masse) (e :: path)) (aux q capacite path)
  in
  aux objets capacite []

(* VERSION MEMOISATION *)

(* let sol_memoisation objets capacite =
   let objets = List.sort (fun x y -> x.masse - y.masse) objets
   let rec aux objets capacite path =
     match objets with
     | [] ->
   in
   aux objets capacite [];
   !max_path *)

(* VERSION EXHAUSTIVE *)

let sol_exhaustive objets capacite =
  let objets = List.sort (fun x y -> x.masse - y.masse) objets
  and max_path = ref [] in
  let rec aux objets capacite path =
    match objets with
    | [] ->
        if
          List.fold_left (fun acc x -> acc + x.valeur) 0 path
          > List.fold_left (fun acc x -> acc + x.valeur) 0 !max_path
        then max_path := path
    | e :: q ->
        if e.masse > capacite then aux q capacite path
        else aux q (capacite - e.masse) (e :: path);
        aux q capacite path
  in
  aux objets capacite [];
  !max_path

let situation1 =
  [
    { masse = 13; valeur = 70 };
    { masse = 12; valeur = 40 };
    { masse = 8; valeur = 30 };
    { masse = 10; valeur = 30 };
  ]

let situation2 =
  [
    { masse = 5; valeur = 60 };
    { masse = 2; valeur = 30 };
    { masse = 2; valeur = 30 };
    { masse = 2; valeur = 30 };
    { masse = 1; valeur = 10 };
  ]

(* Problème II - tribonnaci *)

let rec tribo_rec n =
  if n = 0 then 0
  else if n = 1 || n = 2 then 1
  else tribo_rec (n - 1) + tribo_rec (n - 2) + tribo_rec (n - 3)

let tribo_prog_dyn n =
  if n = 0 then 0
  else if n = 1 || n = 2 then 1
  else
    let n3 = ref 0 and n2 = ref 1 and n1 = ref 1 in
    for i = 3 to n do
      let tmp = !n1 in
      n1 := !n1 + !n2 + !n3;
      n3 := !n2;
      n2 := tmp
    done;
    !n1

(* let tribo n =
   let a0 = 1. /. 3. *. (1. +. Float.cbrt (19. -. 3. *. Float.sqrt 33.) +. Float.cbrt (19. +. 3. *. Float.sqrt 33.) ) and
   a1 =  *)
