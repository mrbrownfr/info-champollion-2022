(* Q3: on devrait obtenir :
   - : int = 11;
   - : int = -9;
   - : int = 7;
   - : int = 3;
   - : int = 1; *)

(* Q4: on obtient "Error: This expression has type float but an expression was expected of type int" *)

(* Q5:
   7.2 /. 2. => - : float = 3.6;
   3. ** 3. => - : float = 27. *)

(* Q6:
   3 < 2 => - : bool = false;
   true => - : bool = true;
   3.0 < 2.0 => - : bool = false;
   3. < 2.0 => - : bool = false (corrigé);
   5. <> 3. => - : bool = true;
   2 / 2 = 5 => - : bool = false (corrigé);
   (3 < 2) || (2 = 2) => - : bool = true;
   3. *. 0.1 = 0.3 => - : bool = false (corrigé);
   not (42 <> 21) => - : bool = false (corrigé);
   (not (3 <= 2)) && (2. = 2.) => - : bool = true (corrigé); *)

(* Q7:
   'a';; => - : char = 'a';
   "bon" ^ "jour" => - : string = "bonjour"; *)

(* Q8:
   let petite = 10 => val petite : int = 10;
   let petite = 1 in petite * 2 => - : int = 2;
   let p = petite * 2 in let q = 3 in p - q => - : int = 17;
   let v = 4. and w = 5. in v < w => - : bool = true; *)

(* Q9: *)
type date = { day : int; month : string; year : int }

let fete_musique : date = { day = 21; month = "June"; year = 2023 }
let jour : int = fete_musique.day
let test = 18

(* Q10: *)
let carre_entier x = x * x
let carre_flottant y = y *. y

(* Q11:
   fst : ('a * 'b) -> 'a = <fun>;
   snd : ('a * 'b) -> 'b = <fun>; *)

(* Q12:
   min : 'a -> 'a -> 'a;
   max : 'a -> 'a -> 'a; *)
let maximum = max 4 11
let maximum = string_of_int maximum;;

print_endline maximum

let minimum = min 4 11
let minimum = string_of_int minimum;;

print_endline minimum

(* Q13: type de la fonction = int -> int -> int
   Il y a d'autres possibilités si l'opérateur /. est utilisé, ce qui permettrait d'utiliser des float *)
let perimetre longueur largeur = longueur * largeur

(* Q14: *)
let est_pythagore a b c = carre_entier a + carre_entier b = carre_entier c

(* Q15:
   somme est de type ('a -> int) -> ('a -> int) -> 'a -> int d'après utop.
   composition est de type ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b d'après utop. *)

(* Q16: *)
let pair_proche x = if x mod 2 = 0 then x else x + 1

(* Q17:
   - : 'a list;
   - : int list;
   - : float list;
   - : string list;
   - : int list;
   - : (int * int) list; *)

(* Q18:
   hd & tl: 'a list -> 'a *)

(* Q19: *)
let deuxieme liste =
  let listewohd = List.tl liste in
  List.hd listewohd

(* Q20: Une erreur est levée car la fonction List.tl ne peut pas renvoyer de liste vide *)

(* Q21 : les 2 renvoient "- : int = 1"      *)

(* Q22:
   - Problème : lever une erreur si la liste est trop courte
   - Solution : créer une exception custom et la lever en cas de problème *)
exception ListeTropCourte of string

let deuxieme2 liste =
  if List.length liste < 2 then
    raise
      (ListeTropCourte
         "La liste donnée est trop courte ! Elle doit comporter 2 éléments au \
          minimum.")
  else deuxieme liste

(* Q23: *)
let carre_liste liste = List.map carre_entier liste

(* Q24: on obtient "Error: This pattern matches values of type 'a list but a pattern was expected which matches values of type int ". Cela nous indique que tous les cas doivent être vérifiés dans l'arbre, et sinon mettre une condition générale (_ -> ...) *)

(* Q25: *)
let un_seul1 liste = if List.length liste = 1 then true else false
let un_seul2 liste = match List.length liste with 1 -> true | _ -> false

(* Q26: *)
let est_voyelle caractere =
  match caractere with
  | 'a' | 'e' | 'i' | 'o' | 'u' | 'y' | 'A' | 'E' | 'I' | 'O' | 'U' | 'Y' ->
      true
  | _ -> false
