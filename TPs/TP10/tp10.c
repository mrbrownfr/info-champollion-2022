///////////////////////////////////////////////////////
//! REPRESENTATION DES NOMBRES DYADIQUES EN MACHINE !//
///////////////////////////////////////////////////////

/*
Tout entier est un nombre dyadique. En effet, en posant b = 0, on obtient une fraction de la forme a/1, où a est un entier relatif.
*/

/*
Tout rationnel n'est pas un nombre dyadique: prenons comme exemple 1/3. Il correspond à la définition d'un rationnel, mais 3 étant premier, il ne peut pas s'écrire sous la forme d'une puissance de 2.
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <nl_types.h>

#define MISSED_ALLOC 69

/// @brief Représentation structurelle d'un nombre dyadique
struct Dyadique
{
    int a;
    int b;
};
typedef struct Dyadique dyadique;

/// @brief Affiche un nombre dyadique
/// @param nombre le nombre à afficher (sous sa représentation dyadrique)
void print_dyadique(dyadique *nombre)
{
    if (nombre->b == 1)
    {
        printf("%d\n", nombre->a);
    }
    else
    {
        printf("%d/2^{%d}\n", nombre->a, nombre->b);
    };
}

/// @brief Transforme un nombre dyadique en sa représentation normalisée
/// @param nombre le nombre à transformer (sous sa représentation dyadrique)
/// @return une structure dyadique contenant le nombre normalisé
dyadique normalise(dyadique *nombre)
{
    dyadique result = {
        .a = nombre->a,
        .b = nombre->b,
    };
    while (result.a % 2 == 0 && result.b > 0)
    {
        result.a /= 2;
        result.b--;
    };
    return result;
}

/// @brief Compare 2 nombres dyadriques
/// @param nb1 le premier nombre à comparer
/// @param nb2 le 2e nombre à comparer
/// @return 0 si ils ne sont pas égaux, 1 sinon
__uint8_t egal(dyadique *nb1, dyadique *nb2)
{

    dyadique normal_nb1 = normalise(nb1);
    dyadique normal_nb2 = normalise(nb2);
    return ((normal_nb1.a == normal_nb2.a) && (normal_nb1.b == normal_nb2.b));
}

/// @brief Convertit un entier dans sa représentation dyadique
/// @param nb l'entier à convertir
/// @return une structure contenant la représentation dyadique de l'entier
dyadique of_int(int nb)
{
    dyadique dyad = {
        .a = nb,
        .b = 0,
    };
    return dyad;
}

/// @brief Calcule l'opposé d'un nombre dyadique
/// @param nb le nombre à opposer
/// @return le nombre opposé sous sa forme dyadique
dyadique opp(dyadique *nb)
{
    dyadique dyad = {
        .a = nb->a,
        .b = nb->b,
    };
    dyad.a *= -1;
    return dyad;
}

/// @brief Elève 2 à la puissance donné
/// @param pow la puissance à laquelle élever 2
/// @return 2 à la puissance donnée
__uint_least8_t pow_2(int pow)
{
    if (pow >= 1)
    {
        return 2 * pow_2(pow - 1);
    }
    return 1;
}

/// @brief Ajoute 2 nombres suivant leur représentation dyadique
/// @param nb1 le premier nombre à additionner
/// @param nb2 le second nombre à additionner
/// @return une structure dyadique contenant la somme des 2 nombres
dyadique add(dyadique *nb1, dyadique *nb2)
{
    errno = 0;
    dyadique result;
    dyadique norm_nb1 = normalise(nb1);
    dyadique norm_nb2 = normalise(nb2);
    result.a = pow_2(norm_nb2.b) * norm_nb1.a + pow_2(norm_nb1.b) * norm_nb2.a;
    result.b = norm_nb1.b + norm_nb2.b;
    return result;
}

/// @brief Calcule l'inverse de toute puissance de 2
/// @param n la puissance de 2 à calculer
/// @return une structure dyadique contenant l'inverse de la puissance de 2
dyadique inv_pow2(int n)
{
    dyadique result = {
        .a = 1,
        .b = n,
    };
    return result;
}

/*
Le dénominateur du produit de 2 nombres dyadiques sera toujours une puissance de 2, et le numérateur un produit de relatifs.
*/

/// @brief Multiplie 2 nombres dyadiques
/// @param nb1 le premier nombre à multiplier
/// @param nb2 le second nombre à multiplier
/// @return une structure dyadique contenant le produit des 2 nombres
dyadique mul(dyadique *nb1, dyadique *nb2)
{
    dyadique result = {
        .a = nb1->a * nb1->b,
        .b = nb1->b + nb2->b,
    };
    return result;
}

/// @brief Convertit un nombre dyadique en flottant
/// @param d le nombre dyadique à convertir
/// @return la forme flottante du nombre dyadique
float to_float(dyadique *d)
{
    return (d->a / pow_2(d->b));
}

/////////////////////////////////////
//! ECRITURE SCIENTIFIQUE BINAIRE !//
/////////////////////////////////////

/// @brief Représentation structurelle d'un nombre sous forme scientifique dans la base 2
struct Scientifique
{
    int s;      // vaut 1 si le nombre représenté est négatif
    int e;      // exposant de l'écriture scientifique
    char m[25]; // mantisse : 24 caractères binaires disponibles (représentation sur 23 dans la forme standard, mais attendez la suite)
};
typedef struct Scientifique scientifique;

/// @brief Affiche un nombre scientifique
/// @param sc le nombre à afficher
void print_scientifique(scientifique *sc)
{
    if (sc->s)
    {
        printf("-");
    };
    printf("1.%sx2^{%d}\n", sc->m, sc->e);
}

/// @brief Initialise un nombre scientifique avec des valeurs par défaut
/// @param sc l'adresse du nombre à initialiser
void initialise_scientifique(scientifique *sc)
{
    sc->s = 0;
    sc->e = 0;
    sc->m[24] = '\0';
    for (int i = 0; i < 24; i++)
    {
        sc->m[i] = 0;
    };
}

/// @brief Convertit un entier en une chaine de caractère contenant la représentation binaire de ce nombre
/// @param chaine la chaîne de caractères à remplir
/// @param valeur le nombre à convertir
/// @param p le nombre de bits autorisés pour la représentation binaire du nombre
/// @return le nombre de bits nécéssaire pour stocker le nombre
/// @exception Il est à votre charge de vérifier que le nombre passé en argument rentre dans le nombre de bits passé en argument, et dans 24 bits.
int int_vers_chaine(char *chaine, int valeur, int p)
{
    char *buffer = malloc(sizeof(char) * p);
    if (buffer == NULL)
    {
        fprintf(stderr, "Une erreur a eu lieu dans l'allocation du buffer dans la fonction = int_vers_chaine = :/ \n");
        exit(MISSED_ALLOC);
    };
    int index = 0;
    while (valeur >= 0)
    {
        buffer[index] = valeur % 2;
        valeur /= 2;
        index++;
    };
    for (int i = 0; index - i >= 0; i++)
    {
        chaine[i] = buffer[index - i];
    };
    free(buffer);
    return index;
}

/// @brief Convertit un nombre dyadique en notation scientifique
/// @param d le nombre dyadique à convertir
/// @return une structure contenant la représentation scientifique du nombre dyadique
scientifique of_dyadique(dyadique *d)
{
    scientifique result = {
        .s = (d->a < 0),
        .e = (d->b * -1),
    };

    result.e += int_vers_chaine(result.m, valeur, 24);

    return result;
}

////////////////////////////////////////////////////////////////
//! VERS UNE REPRESENTATION DES NOMBRES FLOTTANTS EN MACHINE !//
////////////////////////////////////////////////////////////////

/// @brief Affiche un nombre flottant sous forme scientifique
/// @param sc le nombre à afficher
void print_flottant(scientifique *sc)
{
    if (sc->s)
    {
        printf("1");
    };
    char chaine[9];
    chaine[8] = '\0';
    int_vers_chaine(chaine, (sc->e + pow_2(7) - 1), 8); //* Vérifier la véracité du biais
    printf("%s", chaine);
    for (int i = 1; i < 24; i++)
    {
        printf("%c", sc->m[i]);
    };
}

typedef __uint32_t flottant;

/// @brief Calcule le logarithme base 2 du nombre passé en argument
/// @param n le nombre auquel appliquer le logarithme
/// @return le logarithme du nombre passé en argument (partie entière)
__uint8_t log_2(int n)
{
    int compteur = 0;
    while (n > 1)
    {
        n >> 1;
        compteur++;
    };
    return compteur;
}

flottant flottant_of_dyadique(dyadique *d)
{
}

int main()
{
    // dyadique nb_dyad = {.a = }

    return 0;
}
