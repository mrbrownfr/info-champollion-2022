for i = 1 to 10 do
  print_int i; print_newline ()
done;;

let pyramide n =
  for i = 0 to (n-1) do
    for j = 1 to (n-i) do
      print_char ' '
    done;
    for j = 1 to (2*i + 1) do
      print_char '*'
    done;
    print_newline ();
  done;;

type personne = { nom : string ; mutable age : int };;

let personne1 = {nom = "Adrien"; age = 16};;

let vieillir p n = p.age <- (p.age + n);;

type variable = { mutable valeur : int };;

let x = { valeur = 10};;
x. valeur <- x. valeur + 21;;
x. valeur ;;

let p = ref 31;;
(* val p : int ref = {contents = 31} *)
p := !p + 1;;

let tableau = [| 1; 5; 10; 20|];;

let t = Array.make 20 "bonjour";;

let mon_tableau n =
  let t = Array.make n 0 in
    for i = 0 to (n-1) do
      t.(i) <- i * 2;
    done; t;;

let test x y =
try let q = x / y in print_int q; print_newline ()
with Division_by_zero -> print_endline " Division par zéro !";;

exception ListeVide ;;
exception Erreur of string ;;

let f u = match u with
| [] -> raise ListeVide ;
| e::q -> e;;

let n = 100;;
let premier = Array.make (n+1) true;;

let crible premier =
  for i = 2 to n do
    if premier.(i-1) = true then begin
      let j = ref (2*i) in
      while !j < n do
        premier.(!j-1) <- false;
        j := !j + i
      done
    end;
  done;;

let afficher_premiers premier =
  for i = 1 to n do
    Printf.printf "%d : %b" i premier.(i-1);
    print_newline ()
  done;;

let echange t i j =
  let temp = t.(i) in t.(i) <- t.(j); t.(j) <- temp;;


  
let melange t =
  for i = 1 to (Array.length t) - 1 do
    let j = Random.int i in echange t i j
  done;;