open Bd

(* Question 1 *)
(*
   descripteurs : [|"E"; "A"; "G"|]
   donnees : [|[|"triangle"; "cercle vert"; "2"|]; [|"rectangle"; "cercle rouge"; "3"|]|]
*)

(* Question 2 *)
(* lecture_fichier est de type string -> char -> relation *)

(* Question 3 *)
let table_vegetaux = lecture_fichier "vegetaux.csv" ','

(* Question 6 *)
let () = ecriture_fichier table_vegetaux "vegetaux_bak.csv" ","

(* Question 7 *)
let proj_nom_veg = projection table_vegetaux "nomVeg"

(* Question 8 *)
let oignon = selection table_vegetaux "nomVeg" "Oignon"

(* Question 9 *)
let () =
  let table_vegetaux = lecture_fichier "vegetaux.csv" ',' in
  let table_legumes = selection table_vegetaux "type" "légume" in
  let nom_legumes = projection table_legumes "nomVeg" in
  ecriture_fichier nom_legumes "nom_legumes.csv" ","

(* Question 10 *)
(* SELECT nomVeg FROM vegetaux WHERE type="légume" *)
(* Le résultat est bien celui attendu *)

(* Question 12 *)
(* La complexité est O(n^2) *)

(* Question 13 *)
let table_vegetaux = lecture_fichier "vegetaux.csv" ','
let table_marchands = lecture_fichier "marchands.csv" ','
let prod_cart = produit_cartesien table_vegetaux table_marchands

(* Question 15 *)
(* let () =
   let veg = lecture_fichier "vegetaux.csv" ','
   and marchands = lecture_fichier "marchands.csv" ','
   and prix = lecture_fichier "prix.csv" ',' in
   let t = jointure marchands prix "idMar" "idMar" in
   let t = jointure veg t "idVeg" "idVeg" in
   let t = selection t "nomVeg" "Pomme" in
   let sel table champ valeur =
     let rec find a x n = if a.(n) = x then n else find a x (n + 1) in
     try
       let i = find table.descripteurs champ 0 in
       let donnees = ref [] in
       for j = 0 to Array.length table.donnees - 1 do
         if float_of_string table.donnees.(j).(i) <= valeur then
           donnees := !donnees @ [ table.donnees.(j) ]
       done;
       let rel =
         { descripteurs = table.descripteurs; donnees = Array.of_list !donnees }
       in
       Printf.printf "\n%a\n" pp_relation rel;
       rel
     with _ -> raise ChampAbsent
   in
   let t = sel t "prix" 3. in
   projection t "nomBoutique" |> supprimer_doublons |> ignore *)

let () =
  Printf.printf "\n\nTest jointure\n\n";
  let marchands = lecture_fichier "marchands.csv" ','
  and prix = lecture_fichier "prix.csv" ',' in
  jointure marchands prix "idMar" "idMar" |> ignore

(* Question 17 *)
(* Lorsqu'une opération d'agrégation est effectuée sur les résultats d'une réquête,
   toutes les données concernées sont parcourues pour effectuer l'opération *)

(* Question 18 *)
(* L'algorithme prend en entrée une table, un champ et un opérateur d'agrégation.
    *)
