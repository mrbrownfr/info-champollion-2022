(*********************************************)
(* Squelette : Marie Durand                  *)
(* Version : 1.1                             *)
(* Date : Juin 2023                          *)
(*********************************************)

type relation = { descripteurs : string array; donnees : string array array }

let pp_string_array outchan arr =
  let n = Array.length arr in
  (* Printf.fprintf outchan "[|"; *)
  for i = 0 to n - 2 do
    Printf.fprintf outchan "%s, " arr.(i)
  done;
  if n <> 0 then Printf.fprintf outchan "%s" arr.(n - 1)
(* Printf.fprintf outchan "|]" *)

let pp_relation outchan rel =
  let n_donnees = Array.length rel.donnees in
  Printf.fprintf outchan "%a\n" pp_string_array rel.descripteurs;
  for i = 0 to n_donnees - 1 do
    Printf.fprintf outchan "%a\n" pp_string_array rel.donnees.(i)
  done

(* Une base de donnéers relationnelle stockée dans des fichiers *)

(* Détermine le nombre de lignes d'un fichier. 
 * Entrée : nom_fichier   un nom (ou un chemin) de fichier supposé accessible
 * Sortie : un entier indiquant le nombre de lignes du fichier. *)
let nb_lignes nom_fichier =
  let fc = open_in nom_fichier in
  let nb = ref 0 in
  try
    while true do
      let _ = input_line fc in
      incr nb
    done;
    !nb (* nécessaire pour que les expressions soient de même type *)
  with End_of_file ->
    close_in fc;
    Printf.printf "nombre de lignes : %d\n" !nb;
    !nb

exception ChampAbsent

(*******************************************************************)
(* Lecture d'un fichier                                            *)
(*******************************************************************)

(* Fonction auxiliaire qui effectue le parcours une liste d'éléments pour les
   copier dans un
   tableau. *)
let rec parcours tab u i =
  match u with
  | [] -> ()
  | chaine :: q ->
      tab.(i) <- chaine;
      parcours tab q (i + 1)

(* Renvoie un tableau contenant les descripteurs dans l'ordre de leur apparition
    dans la chaîne de caractères passée en paramètre.
    Entrée : ligne       une chaîne de caractères
             separateur  le caractère utilisé comme séparateur entre les valeurs
   Sortie : un tableau contenant les descripteurs et le nombre de valeurs du
   tableau.
*)
let lecture_descripteurs ligne separateur =
  let liste_desc = String.split_on_char separateur ligne in
  let nb = List.length liste_desc in
  let tab = Array.make nb "" in
  parcours tab liste_desc 0;
  (tab, nb)

(* Renvoie un enregistrement sous la forme d'un tableau contenant chaque valeur
   lue sur la ligne passée en paramètre.
   Entrée : ligne      une chaîne de caractères
            nb         le nombre de valeurs attendues
            separateur le caractère utilisé comme séparateur entre les valeurs
   Sortie : un tableau contenant les valeurs rencontrées sur la ligne, dans
   l'ordre.
*)
let lecture_enregistrement ligne nb separateur =
  let enr = Array.make nb "" in
  let liste_valeurs = String.split_on_char separateur ligne in
  let rec parcours u i =
    match u with
    | [] -> ()
    | v :: q ->
        enr.(i) <- v;
        parcours q (i + 1)
  in
  parcours liste_valeurs 0;
  enr

(* Lit un fichier et renvoie la table contenant les données associées.
   Entrées : nom_fichier  un nom de fichier supposé accessible
             separateur le caractère qui sert de délimiteur dans le fichier
             (exemples : ',' ou ';'...)
   Sortie : une table dans laquelle on peut retrouver toutes les données du
   fichier. *)
let lecture_fichier nom_fichier separateur =
  let n = nb_lignes nom_fichier - 1 in
  let fc = open_in nom_fichier in
  let desc, nb = lecture_descripteurs (input_line fc) separateur in
  (* lecture des données *)
  let data = Array.make n [||] in
  for i = 0 to n - 1 do
    let ligne = input_line fc in
    data.(i) <- lecture_enregistrement ligne nb separateur
  done;
  close_in fc;
  let rel = { descripteurs = desc; donnees = data } in
  Printf.printf "%a\n" pp_relation rel;
  rel

(*******************************************************************)
(* Lecture d'un fichier                                            *)
(*******************************************************************)

(* Écrire le contenu d'une table dans un fichier.
   Entrée : table         un objet de type relation
            nom_fichier   le nom du fichier dans lequel écrire
            separateur    une chaîne de caractères représentant le séparateur à
                          utiliser dans le fichier.
   Sortie : pas de valeur de retour, le fichier contient les données de la
   table. *)
let ecriture_fichier table nom_fichier separateur =
  let fc = open_out nom_fichier in
  (* type string array -> string *)
  let arr_to_string arr =
    let str = Array.fold_left (fun acc x -> acc ^ separateur ^ x) "" arr in
    String.sub str 1 (String.length str - 1)
  in
  output_string fc (arr_to_string table.descripteurs ^ "\n");
  Array.iter (fun x -> output_string fc (arr_to_string x ^ "\n")) table.donnees

(*******************************************************************)
(* Projection                                                      *)
(*******************************************************************)

(* Renvoie une table contenant pour chaque enregistrement de la table initiale
   seulement les informations correspondant à la colonne indiquée par le champ.
   Entrée : table   les données d'une table
            champ   le nom de l'attribut
   Sortie : une table contenant les informations correspondant à la colonne du
   champ. Lève l'exception ChampAbsent si le champ n'est pas trouvé parmi les
   descripteurs de la table. *)
let projection table champ =
  let desc = [| champ |] in
  let rec find a x n = if a.(n) = x then n else find a x (n + 1) in
  try
    let i = find table.descripteurs champ 0 in
    let donnees =
      Array.init (Array.length table.donnees) (fun j ->
          [| table.donnees.(j).(i) |])
    in
    let projection = { descripteurs = desc; donnees } in
    Printf.printf "%a" pp_relation projection;
    projection
  with _ -> raise ChampAbsent

(*******************************************************************)
(* Sélection                                                       *)
(*******************************************************************)

(* Renvoie une table contenant seulement les enregistrements pour lesquels une
   condition est respectée (valeur du champ donné égale à la valeur cible).
   Entrée : table   les données d'une table
            champ   le nom de l'attribut considéré
            valeur  la valeur à conserver
   Sortie : une table contenant les informations souhaitée. Lève l'exception
   ChampAbsent si le champ n'est pas trouvé parmi les descripteurs de la table.
*)
let selection table champ valeur =
  let rec find a x n = if a.(n) = x then n else find a x (n + 1) in
  try
    let i = find table.descripteurs champ 0 in
    let donnees = ref [] in
    for j = 0 to Array.length table.donnees - 1 do
      if table.donnees.(j).(i) = valeur then
        donnees := !donnees @ [ table.donnees.(j) ]
    done;
    let rel =
      { descripteurs = table.descripteurs; donnees = Array.of_list !donnees }
    in
    Printf.printf "\n%a\n" pp_relation rel;
    rel
  with _ -> raise ChampAbsent

let supprimer_doublons table =
  let rec supp_doublons l =
    match l with
    | [] -> []
    | e :: q -> if List.mem e q then supp_doublons q else e :: supp_doublons q
  in
  let donnees = Array.to_list table.donnees |> supp_doublons |> Array.of_list in
  let rel = { descripteurs = table.descripteurs; donnees } in
  Printf.printf "%a\n" pp_relation rel;
  rel

(*******************************************************************)
(* Produit cartésien                                               *)
(*******************************************************************)
let produit_cartesien table1 table2 =
  let desc = Array.append table1.descripteurs table2.descripteurs in
  let donnees =
    Array.map
      (fun x -> Array.map (fun y -> Array.append x y) table2.donnees)
      table1.donnees
    |> Array.to_list |> Array.concat
  in
  let rel = { descripteurs = desc; donnees } in
  Printf.printf "%a\n" pp_relation rel;
  rel

(*******************************************************************)
(* Jointure                                                        *)
(*******************************************************************)
let jointure table1 table2 champ1 champ2 =
  let produit = produit_cartesien table1 table2 in
  let rec find a x n = if a.(n) = x then n else find a x (n + 1) in
  try
    let i1 = find table1.descripteurs champ1 0 in
    let i2 =
      find table2.descripteurs champ2 0 + Array.length table1.descripteurs
    in
    let donnees = ref [] in
    for j = 0 to Array.length produit.donnees - 1 do
      if produit.donnees.(j).(i1) = produit.donnees.(j).(i2) then
        donnees := !donnees @ [ produit.donnees.(j) ]
    done;
    let rel =
      { descripteurs = produit.descripteurs; donnees = Array.of_list !donnees }
    in
    Printf.printf "\n%a\n" pp_relation rel;
    rel
  with _ -> raise ChampAbsent
