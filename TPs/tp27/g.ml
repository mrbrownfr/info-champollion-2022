type vertex = int
type edge = vertex * vertex
type graph = vertex list array

let create_graph n : graph = Array.init n (fun _ -> [])
let nb_vertices (g : graph) = Array.length g
let has_edge (g : graph) ((i, j) : edge) = List.mem j g.(i)

let add_edge (g : graph) ((i, j) : edge) =
  let rec insert x l =
    match l with
    | [] -> [ x ]
    | e :: q -> if e >= x then x :: e :: q else e :: insert x q
  in
  g.(i) <- insert j g.(i)

let remove_edge (g : graph) ((i, j) : edge) =
  let rec remove x l =
    match l with [] -> [] | e :: q -> if e = x then q else e :: remove x q
  in
  g.(i) <- remove j g.(i)

let neighbours (g : graph) (v : vertex) = g.(v)

let all_egdes (g : graph) : edge list =
  let rec aux i =
    if i = nb_vertices g then []
    else List.map (fun j -> (i, j)) (neighbours g i) @ aux (i + 1)
  in
  aux 0

let outdegree (g : graph) (v : vertex) : int = List.length g.(v)

let indegree (g : graph) (v : vertex) : int =
  Array.fold_left (fun acc l -> if List.mem v l then acc + 1 else acc) 0 g

let reverse (g : graph) : graph =
  let rev = create_graph (nb_vertices g) in
  List.iter (fun (i, j) -> add_edge rev (j, i)) (all_egdes g);
  rev

let to_non_oriented (g : graph) =
  List.iter
    (fun (i, j) -> if not @@ has_edge g (j, i) then add_edge g (j, i))
    (all_egdes g)

let g1 : graph =
  [|
    [ 1; 2 ];
    [ 2; 3; 4 ];
    [];
    [ 0; 5 ];
    [ 1; 2 ];
    [ 10 ];
    [ 1; 9 ];
    [ 8 ];
    [ 6 ];
    [ 7; 10 ];
    [ 11 ];
    [ 5 ];
  |]

let g2 : graph =
  [|
    [ 1; 4 ];
    [ 0; 2; 4; 7 ];
    [ 1; 5 ];
    [ 6; 8 ];
    [ 0; 1 ];
    [ 2; 7; 8 ];
    [ 3; 8 ];
    [ 1; 5; 8 ];
    [ 3; 5; 6; 7 ];
    [];
  |]

(* exercice 1 *)
let dfs pre post g x0 =
  let visite = Array.make (nb_vertices g) false in
  let rec visiter s =
    if not visite.(s) then (
      visite.(s) <- true;
      pre s;
      List.iter visiter (neighbours g s);
      post s)
  in
  visiter x0

(* exercice 3 *)
let pre1 x = Printf.printf "pre %d\n" x
let post1 x = Printf.printf "post %d\n" x

(*
dfs pre1 post1 g1 0
   pre 0
   pre 1
   pre 2
   post 2
   pre 3
   pre 5
   pre 10
   pre 7
   pre 8
   post 8
   post 7
   post 10
   post 5
   post 3
   pre 4
   post 4
   post 1
   post 0
*)

(*
dfs pre1 post1 g2 5
  pre 5
  pre 2
  pre 1
  pre 0
  pre 4
  post 4
  post 0
  pre 7
  pre 8
  pre 3
  pre 6
  post 6
  post 3
  post 8
  post 7
  post 1
  post 2
  post 5
*)

(* question 2 *)
(* l'intervalle [pre.(s),post.(s)] représente la parcours des successeurs de s *)

(* exercice 4 *)
let dfs_pre_post pre_f post_f g x0 =
  let n = nb_vertices g in
  let visite = Array.make n false
  and pre = Array.make n (-1)
  and post = Array.make n (-1)
  and horloge = ref 0 in
  let rec visiter s =
    if not visite.(s) then (
      visite.(s) <- true;
      pre_f s;
      pre.(s) <- !horloge;
      incr horloge;
      List.iter visiter (neighbours g s);
      post_f s;
      post.(s) <- !horloge;
      incr horloge)
  in
  visiter x0;
  (pre, post)

(* Le type est (vertex -> unit) -> (vertex -> unit) -> graph -> vertex -> vertex array * vertex array *)

(* exercice 5 *)
let composantes g =
  let n = nb_vertices g in
  let visite = Array.make n false and comp = Array.make n 0 and id = ref 0 in
  Array.iter
    (fun s ->
      if not visite.(s) then (
        dfs (fun v -> visite.(v) <- true) (fun v -> comp.(v) <- !id) g s;
        incr id))
    (Array.init n (fun i -> i));
  comp

(* question 3 *)
let est_connexe g =
  let comp = composantes g in
  Array.for_all (( = ) comp.(0)) comp

(* exercice 6 *)
let dfs_ite g =
  let n = nb_vertices g in
  let visite = Array.make n false
  and stack = Stack.create ()
  and parcours = ref [] in
  let visiter x =
    Stack.push x stack;
    while not @@ Stack.is_empty stack do
      let s = Stack.pop stack in
      if not visite.(s) then (
        visite.(s) <- true;
        parcours := s :: !parcours;
        List.iter
          (fun v -> if not visite.(v) then Stack.push v stack)
          (neighbours g s))
    done
  in
  Array.iter visiter (Array.init n (fun i -> i));
  !parcours

(* exercice 7 *)
let bfs_ite g =
  let n = nb_vertices g in
  let visite = Array.make n false
  and q = Queue.create ()
  and parcours = ref [] in

  let visiter x =
    Queue.add x q;
    while not @@ Queue.is_empty q do
      let s = Queue.take q in
      if not visite.(s) then (
        visite.(s) <- true;
        List.iter (fun v -> Queue.add v q) (neighbours g s);
        parcours := s :: !parcours)
    done
  in
  Array.iter visiter (Array.init n (fun i -> i));
  !parcours
