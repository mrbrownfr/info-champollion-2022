/** Squelette.
 *  Version 1 : N. PECHEUX
 **/
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define N 164  // Nombre de péages
#define MAX 23 // Longueur maximale d'un nom de péage (+1)

// Lecture dans `noms` des noms des péages
void read_names(char noms[N][MAX])
{
    FILE *f = fopen("noms.txt", "r");
    if (f == NULL)
    {
        fprintf(stderr, "Cannot open file noms.txt\n");
        exit(1);
    }
    for (int i = 0; i < N; i++)
    {
        fscanf(f, "%[^\n]\n", noms[i]);
    }
    fclose(f);
}

char *ville_de_id(char noms[N][MAX], int id)
{
    return noms[id];
}

int id_de_ville(char noms[N][MAX], char *nom)
{
    for (int i = 0; i < N; i++)
    {
        if (strcmp(noms[i], nom) == 0)
        {
            return i;
        }
    }
    return -1;
}

// Initialise une matrice d'adjacence avec des `0` sur la diagonale et
// des -1 (pour plus l'infini) partout.
void init(double mat[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            mat[i][j] = ((i == j) ? (double)0 : (double)-1);
        }
    }
}

// Lecture dans `dist` et dans `prix` des matrices d'adjacence (qui
// doivent avoir été initialisées au préalable.
void read_graph(double dist[N][N], double prix[N][N])
{
    FILE *f = fopen("graph.txt", "r");
    if (f == NULL)
    {
        fprintf(stderr, "Cannot open file graph.txt\n");
        exit(1);
    }
    while (true)
    {
        int x, y;
        double p, d;
        int c = fscanf(f, "%d,%d,%lf,%lf", &x, &y, &p, &d);
        if (c != 4)
        {
            break;
        }
        dist[x][y] = p;
        prix[x][y] = d;
    }
    fclose(f);
}

float max_prix(double prix[N][N])
{
    double m = 0;
    int i_m, j_m;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (prix[i][j] > m)
            {
                m = prix[i][j];
                i_m = i;
                j_m = j;
            }
        }
    }
    printf("%d, %d\n", i_m, j_m);
    return m;
}

double addition(double a, double b)
{
    if (a == -1 || b == -1)
    {
        return -1;
    }
    return a + b;
}

double comparer(double a, double b)
{
    if (a == -1)
    {
        return b;
    }
    if (b == -1)
    {
        return a;
    }
    return (a < b) ? a : b;
}

void floyd_warshall(double best[N][N], double prix[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            best[i][j] = prix[i][j];
        }
    }

    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                best[i][j] = comparer(best[i][j], addition(best[i][k], best[k][j]));
            }
        }
    }
}

void floyd_warshall_pred(double best[N][N], int pred[N][N], double prix[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            best[i][j] = prix[i][j];
            pred[i][j] = i;
        }
    }

    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                double a = best[i][j];
                double b = addition(best[i][k], best[k][j]);
                if (comparer(a, b) == b)
                {
                    if (j != k)
                    {
                        pred[i][j] = k;
                    }
                    best[i][j] = b;
                }
            }
        }
    }
}

void print_chemin(int i, int j, int pred[N][N])
{
    while (pred[i][j] != i)
    {
        printf("%d, ", j);
        j = pred[i][j];
    }
    printf("%d, ", j);
}

int main(void)
{

    // Question 1
    // MAX vaut 23 plutôt que 22 car il y a aussi le caractère de fin de séquence

    // Question 2
    // La gare de péage d'identifiant 37 est FLEURY-EN-BIERE
    // VILLEFRANCHE-LIMAS est la ville d'identifiant 90

    // Question 4
    // Le tarif du péage 37 au péage 90 est de 36.5€
    // La péage le plus cher coûte 99€ et est entre le péage 48 et 141

    // Question 5
    // Si on sort au 3 pour retourner à AUXERRE NORD, le trajet coûte 36,4€ soit 10 centimes de moins.
    // C'est donc plus rentable

    // Question 8
    // La complexité spatiale est de O(n^2), la complexité temporelle est de O(n^3)

    // Question 9
    // L'économie réalisée est de 4€60

    char noms[N][MAX] = {{'\0'}};
    read_names(noms);

    printf("%s\n", ville_de_id(noms, 37));
    printf("%d\n", id_de_ville(noms, "VILLEFRANCHE-LIMAS"));

    double dist[N][N];
    init(dist);
    double prix[N][N];
    init(prix);
    read_graph(dist, prix);

    printf("%f\n", max_prix(prix));

    double best[N][N];
    int pred[N][N];
    floyd_warshall_pred(best, pred, prix);
    printf("%f\n", best[37][90]);
    print_chemin(37, 90, pred);
    printf("\n%d\n", pred[37][91]);
}
